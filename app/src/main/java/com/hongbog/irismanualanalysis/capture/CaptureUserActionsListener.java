package com.hongbog.irismanualanalysis.capture;

import android.view.View;
import android.widget.SeekBar;

public interface CaptureUserActionsListener {
    void clickIrisMap(View v);
    void onSeekBarProgressChanged(SeekBar seekBar, int progress, boolean fromUser);
    void clickCompleteIrisMap(View v);
}
