package com.hongbog.irismanualanalysis.capture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.databinding.ActivityCaptureBinding;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.ImageUtil;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

public class CaptureActivity extends AppCompatActivity implements CaptureNavigator {
    public static final String IRIS_IMAGE = "irisImage";
    public static final int CAPTURE_RESULT_OK = RESULT_FIRST_USER + 1;
    private ImageView imageView;
    private CanvasView canvasView;
    private TextView rotationText;
    private ConstraintLayout rootLayout, menuLayout;
    private CaptureViewModel mViewModel;
    private SeekBar seekBar;
    private ActivityCaptureBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDataBinding();
    }

    private void setupDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_capture);

        mViewModel = obtainViewModel(this);

        final CaptureUserActionsListener listener = getCaptureActionsListener();

        binding.setListener(listener);
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(this);

        final Image irisImage = getIntent().getParcelableExtra(UserInfoActivity.IRIS_IMAGE);

        mViewModel.start(irisImage);

        setupView(irisImage.getSide());
    }

    @NonNull
    public static CaptureViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(CaptureViewModel.class);
    }

    private void setupView(String imageSide) {
        imageView = binding.imageView;
        canvasView = binding.canvasView;
        canvasView.getImageDto().setSide(imageSide);

        rotationText = binding.rotationText;
        rootLayout = binding.rootLayout;
        seekBar = binding.seekBar;
        menuLayout = binding.menuLayout;

        ViewTreeObserver viewTreeObserver = canvasView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int rootWidth = rootLayout.getWidth();
                int rootHeight = rootLayout.getHeight();

                Image imageDto = canvasView.getImageDto();
                imageDto.setWidth(rootHeight * 4 / 3);
                imageDto.setLength(rootHeight);
                imageDto.setCenterX(imageDto.getWidth() / 2);
                imageDto.setCenterY(imageDto.getLength() / 2);

                FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
                frameParams.width = Math.round(imageDto.getWidth());
                frameParams.height = Math.round(imageDto.getLength());
                imageView.setLayoutParams(frameParams);
                canvasView.setLayoutParams(frameParams);

                LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) menuLayout.getLayoutParams();
                linearParams.width = rootWidth - Math.round(imageDto.getWidth());
                linearParams.height = Math.round(imageDto.getLength());
                menuLayout.setLayoutParams(linearParams);

                ConstraintLayout.LayoutParams constParams = new ConstraintLayout.LayoutParams(
                        rootWidth - Math.round(imageDto.getWidth()) - 30, ViewGroup.LayoutParams.WRAP_CONTENT
                );

                constParams.startToStart = R.id.seekbarLayout;
                constParams.endToEnd = R.id.seekbarLayout;
                constParams.bottomToBottom = R.id.seekbarLayout;
                seekBar.setLayoutParams(constParams);

                canvasView.setDraw(true);
                canvasView.setInitialize(CaptureActivity.this);

                canvasView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                canvasView.invalidate();
            }
        });
    }

    private CaptureUserActionsListener getCaptureActionsListener() {
        return new CaptureUserActionsListener() {
            @Override
            public void clickIrisMap(View v) {
                RadioButton radioButton = (RadioButton) v;

                switch (radioButton.getId()) {
                    case R.id.pupilBtn:
                        canvasView.setState(0);
                        break;
                    case R.id.autoNerveBtn:
                        canvasView.setState(1);
                        break;
                    case R.id.irisBtn:
                        canvasView.setState(2);
                        break;
                }
            }

            @Override
            public void onSeekBarProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rotationText.setText(String.valueOf(progress - 90));
                Image imageDto = canvasView.getImageDto();
                imageDto.setRotationAngle(progress - 90);
                canvasView.invalidate();
            }

            @Override
            public void clickCompleteIrisMap(View v) {
                canvasView.invalidate();
                canvasView.buildDrawingCache();

                Image imageDto = canvasView.getImageDto();

                Bitmap irisBitmap = Bitmap.createScaledBitmap(
                        ((BitmapDrawable) imageView.getDrawable()).getBitmap(),
                        (int) imageDto.getWidth(), (int) imageDto.getLength(), true);
                Bitmap irisMapBitmap = Bitmap.createScaledBitmap(
                        ImageUtil.loadBitmapFromView(canvasView),
                        (int) imageDto.getWidth(), imageDto.getLength(), true);
//                Bitmap irisMapBitmap = ImageUtil.loadBitmapFromView(canvasView);

                final String side = canvasView.getImageDto().getSide();

                if (TextUtils.equals(side, UserInfoActivity.IRIS_LEFT)) {
                    Image.leftIrisBitmap = irisBitmap;
                    Image.leftIrisMapBitmap = irisMapBitmap;
                } else {
                    Image.rightIrisBitmap = irisBitmap;
                    Image.rightIrisMapBitmap = irisMapBitmap;
                }

                mViewModel.setIrisImage(imageDto);

                openPatientInfo();
            }
        };
    }

    @Override
    public void openPatientInfo() {
        Intent intent = new Intent(CaptureActivity.this, UserInfoActivity.class);
        intent.putExtra(IRIS_IMAGE, mViewModel.getIrisImage());
        setResult(CAPTURE_RESULT_OK, intent);
        finish();
    }
}

