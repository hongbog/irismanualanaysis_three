package com.hongbog.irismanualanalysis.capture;

import android.app.Application;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.ImageUtil;

import java.io.IOException;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


public class CaptureViewModel extends AndroidViewModel {

    private Image mIrisImage;
    private MutableLiveData<Bitmap> mIrisImageBitmap = new MutableLiveData<>();

    public CaptureViewModel(@NonNull Application application) {
        super(application);
    }

    public Image getIrisImage() {
        return mIrisImage;
    }

    public void setIrisImage(Image irisImage) {
        mIrisImage = irisImage;
    }

    public LiveData<Bitmap> getIrisImageBitmap() {
        return mIrisImageBitmap;
    }

    public void start(@Nullable Image irisImage) {
        if (irisImage == null) return;
        mIrisImage = irisImage;

        if (TextUtils.equals(irisImage.getSide(), UserInfoActivity.IRIS_LEFT)) {
            mIrisImageBitmap.setValue(Image.leftIrisBitmap);
        } else {
            mIrisImageBitmap.setValue(Image.rightIrisBitmap);
        }
    }
}
