package com.hongbog.irismanualanalysis.capture;

import android.graphics.Color;

import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis.Organ;

import java.util.HashMap;
import java.util.Map;

public class ImageConstant {
    // 중앙선 각도 설정
    int[] centerLinePoint = {270, 90, 0, 180};

    private Map<String, Integer> lineNum = new HashMap<String, Integer>();
    private Map<String, int[]> linePoint = new HashMap<String, int[]>();
    private Map<String, int[]> pancreasLinePoint = new HashMap<String, int[]>();
    private Map<String, Integer> arcNum = new HashMap<String, Integer>();
    private Map<String, int[]> arcPoint = new HashMap<String, int[]>();
    private Map<String, int[]> pancreasArcPoint = new HashMap<String, int[]>();
    private Map<String, String[]> regionName = new HashMap<String, String[]>();
    private Map<String, int[]> regionColor = new HashMap<>();

    private ImageConstant() {
        // 내부 선 각도 설정
//        int[] rightLinePoint = {45, 156, 177, 225, 243, 273, 297, 327};
        int[] rightLinePoint = {327, 45, 156, 177, 225, 243, 273, 297};  // 뇌, 신장, 간, 폐
        int[] leftLinePoint = {33, 63, 87, 183, 201, 312};  // 뇌, 폐, 신장
        int[] rightPancreasLinePoint = {150, 225};  // 췌장
        int[] leftPancreasLinePoint = {105, 240};  // 췌장
        lineNum.put("right", rightLinePoint.length);
        lineNum.put("left", leftLinePoint.length);
        linePoint.put("right", rightLinePoint);
        linePoint.put("left", leftLinePoint);
        pancreasLinePoint.put("right", rightPancreasLinePoint);
        pancreasLinePoint.put("left", leftPancreasLinePoint);

        // 호 시작 각도 및 크기 설정
        int[] rightArcPoint = {237, 78, 66, 21, 135, 18, 183, 24};
        int[] leftArcPoint = {222, 81, 333, 24, 93, 18};
        int[] rightPancreasArcPoint = {60, 75};  // 췌장
        int[] leftPancreasArcPoint = {15, 135};  // 췌장
        arcNum.put("right", rightArcPoint.length / 2);
        arcNum.put("left", leftArcPoint.length / 2);
        arcPoint.put("right", rightArcPoint);
        arcPoint.put("left", leftArcPoint);
        pancreasArcPoint.put("right", rightPancreasArcPoint);
        pancreasArcPoint.put("left", leftPancreasArcPoint);

        // 부위 별 신체 장기 이름
        String[] rightRegionName = {Organ.BRAIN_ORGAN, Organ.KIDENY_ORGAN, Organ.LIVER_ORGAN,
                Organ.LUNGS_ORGAN, Organ.PANCREAS_ORGAN};
        String[] leftRegionName = {Organ.BRAIN_ORGAN, Organ.LUNGS_ORGAN, Organ.KIDENY_ORGAN,
                Organ.PANCREAS_ORGAN};

        regionName.put("right", rightRegionName);
        regionName.put("left", leftRegionName);

        // 부위별 색상 => 오른쪽: 뇌, 신장, 간, 폐, 췌장 // 왼쪽: 뇌, 폐, 신장, 췌장
        int [] rightRegionColor = {
                Color.argb(150, 0, 0, 255),
                Color.argb(150, 255, 255, 0),
                Color.argb(150, 0, 128, 0),
                Color.argb(150, 238, 138, 238),
                Color.argb(150, 255, 255, 240)};
        int [] leftRegionColor = {
                Color.argb(150, 0, 0, 255),
                Color.argb(150, 238, 138, 238),
                Color.argb(150, 255, 255, 0),
                Color.argb(150, 255, 255, 240)};
        regionColor.put("right", rightRegionColor);
        regionColor.put("left", leftRegionColor);
    }

    private static class SingleToneHolder {
        static final ImageConstant instance = new ImageConstant();
    }

    public static ImageConstant getInstance() {
        return SingleToneHolder.instance;
    }

    public int[] getCenterLinePoint() {
        return centerLinePoint;
    }

    public void setCenterLinePoint(int[] centerLinePoint) {
        this.centerLinePoint = centerLinePoint;
    }

    public Map<String, Integer> getLineNum() {
        return lineNum;
    }

    public void setLineNum(Map<String, Integer> lineNum) {
        this.lineNum = lineNum;
    }

    public Map<String, int[]> getLinePoint() {
        return linePoint;
    }

    public void setLinePoint(Map<String, int[]> linePoint) {
        this.linePoint = linePoint;
    }

    public Map<String, int[]> getPancreasLinePoint() {
        return pancreasLinePoint;
    }

    public void setPancreasLinePoint(Map<String, int[]> pancreasLinePoint) {
        this.pancreasLinePoint = pancreasLinePoint;
    }

    public Map<String, Integer> getArcNum() {
        return arcNum;
    }

    public void setArcNum(Map<String, Integer> arcNum) {
        this.arcNum = arcNum;
    }

    public Map<String, int[]> getArcPoint() {
        return arcPoint;
    }

    public void setArcPoint(Map<String, int[]> arcPoint) {
        this.arcPoint = arcPoint;
    }

    public Map<String, int[]> getPancreasArcPoint() {
        return pancreasArcPoint;
    }

    public void setPancreasArcPoint(Map<String, int[]> pancreasArcPoint) {
        this.pancreasArcPoint = pancreasArcPoint;
    }

    public Map<String, String[]> getRegionName() {
        return regionName;
    }

    public void setRegionName(Map<String, String[]> regionName) {
        this.regionName = regionName;
    }

    public Map<String, int[]> getRegionColor() {
        return regionColor;
    }

    public void setRegionColor(Map<String, int[]> regionColor) {
        this.regionColor = regionColor;
    }
}
