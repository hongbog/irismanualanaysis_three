package com.hongbog.irismanualanalysis.capture;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.hongbog.irismanualanalysis.data.entity.Image;

public class CanvasView extends View{
    private ImageConstant imageConstant;
    private ScaleGestureDetector scaleGestureDetector;
    private Paint pupilPaint, autoNervePaint, irisPaint;
    private Paint[] insideLinePaint, centerLinePaint, pancreasLinePaint;
    private Paint[] autoNerveArcPaint, irisArcPaint, pancreasInnerArcPaint, pancreasOuterArcPaint;
    private RectF[] autoNerveRect, irisRect, pancreasInnerRect, pancreasOuterRect;

    private Image imageDto = new Image();
    private float oriX, oriY;  // oriX: 움직이기 전 x 좌표, oriY: 움직이기 전 y 좌표
    private int state = 0;  // 선택 영역 상태(동공, 자율신경환, 홍채)
    private long lastPinchZoomTime;
    private boolean isDraw = false;

    public CanvasView(Context context) {
        super(context);
    }

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }

    public void setInitialize(Context context) {
        imageConstant = ImageConstant.getInstance();

        // pupilR: 동공 원 반지름, autoNerveR: 자율신경환 원 반지름, irisR: 홍채 원 반지름, rotationAngle: 회전 각도, position: 왼쪽/오른쪽
        imageDto.setPupilRadius(50);
        imageDto.setAutonomicNerveWaveRadius(70);
        imageDto.setIrisRadious(100);
        imageDto.setRotationAngle(0);

        pupilPaint = new Paint();
        pupilPaint.setAntiAlias(true);
        pupilPaint.setStrokeWidth(5);
        pupilPaint.setColor(Color.rgb(255,204,0));
        pupilPaint.setStyle(Paint.Style.STROKE);

        autoNervePaint = new Paint();
        autoNervePaint.setAntiAlias(true);
        autoNervePaint.setStrokeWidth(5);
        autoNervePaint.setColor(Color.rgb(0,50,255));
        autoNervePaint.setStyle(Paint.Style.STROKE);

        irisPaint = new Paint();
        irisPaint.setAntiAlias(true);
        irisPaint.setStrokeWidth(5);
        irisPaint.setColor(Color.rgb(76, 187, 23));
        irisPaint.setStyle(Paint.Style.STROKE);

        centerLinePaint = new Paint[2];
        for (int i = 0; i < centerLinePaint.length; i++) {
            centerLinePaint[i] = new Paint();
            centerLinePaint[i].setAntiAlias(true);
            centerLinePaint[i].setStrokeWidth(5);
            centerLinePaint[i].setColor(Color.BLACK);
            centerLinePaint[i].setStyle(Paint.Style.STROKE);
        }

        insideLinePaint = new Paint[imageConstant.getLineNum().get(imageDto.getSide())];
        for (int i = 0; i < imageConstant.getLineNum().get(imageDto.getSide()); i++) {
            insideLinePaint[i] = new Paint();
            insideLinePaint[i].setAntiAlias(true);
            insideLinePaint[i].setStrokeWidth(5);
            insideLinePaint[i].setColor(Color.rgb(255, 24, 0));
            insideLinePaint[i].setStyle(Paint.Style.STROKE);
        }

        pancreasLinePaint = new Paint[2];
        for (int i = 0; i < pancreasLinePaint.length; i++) {
            pancreasLinePaint[i] = new Paint();
            pancreasLinePaint[i].setAntiAlias(true);
            pancreasLinePaint[i].setStrokeWidth(5);
            pancreasLinePaint[i].setColor(Color.rgb(255, 24, 0));
            pancreasLinePaint[i].setStyle(Paint.Style.STROKE);
        }

        autoNerveArcPaint = new Paint[imageConstant.getArcNum().get(imageDto.getSide())];
        autoNerveRect = new RectF[imageConstant.getArcNum().get(imageDto.getSide())];
        for (int i = 0; i < imageConstant.getArcNum().get(imageDto.getSide()); i++) {
            autoNerveRect[i] = new RectF();
            autoNerveArcPaint[i] = new Paint();
            autoNerveArcPaint[i].setAntiAlias(true);
            autoNerveArcPaint[i].setStrokeWidth(5);
            autoNerveArcPaint[i].setColor(Color.rgb(255, 24, 0));
            autoNerveArcPaint[i].setStyle(Paint.Style.STROKE);
        }

        irisArcPaint = new Paint[imageConstant.getArcNum().get(imageDto.getSide())];
        irisRect = new RectF[imageConstant.getArcNum().get(imageDto.getSide())];
        for (int i = 0; i < imageConstant.getArcNum().get(imageDto.getSide()); i++) {
            irisRect[i] = new RectF();
            irisArcPaint[i] = new Paint();
            irisArcPaint[i].setAntiAlias(true);
            irisArcPaint[i].setStrokeWidth(5);
            irisArcPaint[i].setColor(Color.rgb(255, 24, 0));
            irisArcPaint[i].setStyle(Paint.Style.STROKE);
        }

        pancreasInnerArcPaint = new Paint[1];
        pancreasInnerRect = new RectF[1];
        pancreasOuterArcPaint = new Paint[1];
        pancreasOuterRect = new RectF[1];
        for (int i = 0; i < pancreasInnerArcPaint.length; i++) {
            pancreasInnerRect[i] = new RectF();
            pancreasInnerArcPaint[i] = new Paint();
            pancreasInnerArcPaint[i].setAntiAlias(true);
            pancreasInnerArcPaint[i].setStrokeWidth(5);
            pancreasInnerArcPaint[i].setColor(Color.rgb(255, 24, 0));
            pancreasInnerArcPaint[i].setStyle(Paint.Style.STROKE);

            pancreasOuterRect[i] = new RectF();
            pancreasOuterArcPaint[i] = new Paint();
            pancreasOuterArcPaint[i].setAntiAlias(true);
            pancreasOuterArcPaint[i].setStrokeWidth(5);
            pancreasOuterArcPaint[i].setColor(Color.rgb(255, 24, 0));
            pancreasOuterArcPaint[i].setStyle(Paint.Style.STROKE);
        }

        scaleGestureDetector = new ScaleGestureDetector(context, new OnPinchListener(context));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (isDraw) {
            // 동공, 자율신경환, 홍채 외각 영역 그리기
            canvas.drawCircle(imageDto.getCenterX(), imageDto.getCenterY(), imageDto.getPupilRadius(), pupilPaint);
            canvas.drawCircle(imageDto.getCenterX(), imageDto.getCenterY(), imageDto.getAutonomicNerveWaveRadius(), autoNervePaint);
            canvas.drawCircle(imageDto.getCenterX(), imageDto.getCenterY(), imageDto.getIrisRadious(), irisPaint);
            // 췌장 외 영역 그리기
            insideLineDraw(canvas);
            arcDraw(canvas);
            centerLineDraw(canvas);
            // 췌장 영역 그리기
            pancreasLineDraw(canvas);
            pancreasArcDraw(canvas);
        }
    }

    private void insideLineDraw(Canvas canvas) {
        int lineNum = imageConstant.getLineNum().get(imageDto.getSide());
        for (int i = 0; i < lineNum; i++) {
            int linePoint = imageConstant.getLinePoint().get(imageDto.getSide())[i];
            float startX =  imageDto.getCenterX() + imageDto.getAutonomicNerveWaveRadius() * (float) Math.sin((linePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float startY = imageDto.getCenterY() - imageDto.getAutonomicNerveWaveRadius() * (float) Math.cos((linePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float endX =  imageDto.getCenterX() + imageDto.getIrisRadious() * (float) Math.sin((linePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float endY = imageDto.getCenterY() - imageDto.getIrisRadious() * (float) Math.cos((linePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);

            canvas.drawLine(startX, startY, endX, endY, insideLinePaint[i]);
        }
    }

    private void centerLineDraw(Canvas canvas) {
        for (int i = 0; i < centerLinePaint.length; i++) {
            int startCenterLinePoint = imageConstant.getCenterLinePoint()[i*2];
            int endCenterLinePoint = imageConstant.getCenterLinePoint()[i*2 + 1];
            float startX =  imageDto.getCenterX() + imageDto.getIrisRadious() * (float) Math.sin((startCenterLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float startY = imageDto.getCenterY() - imageDto.getIrisRadious() * (float) Math.cos((startCenterLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float endX =  imageDto.getCenterX() + imageDto.getIrisRadious() * (float) Math.sin((endCenterLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float endY = imageDto.getCenterY() - imageDto.getIrisRadious() * (float) Math.cos((endCenterLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);

            canvas.drawLine(startX, startY, endX, endY, centerLinePaint[i]);
        }
    }

    private void pancreasLineDraw(Canvas canvas) {
        for (int i = 0; i < pancreasLinePaint.length; i++) {
            int pancreasLinePoint = imageConstant.getPancreasLinePoint().get(imageDto.getSide())[i];
            float startX =  imageDto.getCenterX() + imageDto.getAutonomicNerveWaveRadius() * (float) Math.sin((pancreasLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float startY = imageDto.getCenterY() - imageDto.getAutonomicNerveWaveRadius() * (float) Math.cos((pancreasLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float endX =  imageDto.getCenterX() + (imageDto.getAutonomicNerveWaveRadius() + (imageDto.getIrisRadious() - imageDto.getAutonomicNerveWaveRadius()) / 7 ) * (float) Math.sin((pancreasLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);
            float endY = imageDto.getCenterY() - (imageDto.getAutonomicNerveWaveRadius() + (imageDto.getIrisRadious() - imageDto.getAutonomicNerveWaveRadius()) / 7 ) * (float) Math.cos((pancreasLinePoint + imageDto.getRotationAngle()) * Math.PI / 180.0);

            canvas.drawLine(startX, startY, endX, endY, pancreasLinePaint[i]);
        }
    }

    private void arcDraw(Canvas canvas) {
        float innerCircleR = imageDto.getAutonomicNerveWaveRadius();
        float outerCircleR = imageDto.getIrisRadious();
        int arcNum = imageConstant.getArcNum().get(imageDto.getSide());
        for (int i = 0; i < arcNum; i++) {
            int startAngle =imageConstant.getArcPoint().get(imageDto.getSide())[i*2];
            int sweepAngle = imageConstant.getArcPoint().get(imageDto.getSide())[(i*2)+1];
            autoNerveRect[i].set(imageDto.getCenterX() - innerCircleR, imageDto.getCenterY() - innerCircleR, imageDto.getCenterX() + innerCircleR, imageDto.getCenterY() + innerCircleR);
            canvas.drawArc(autoNerveRect[i], startAngle + imageDto.getRotationAngle(), sweepAngle, false, autoNerveArcPaint[i]);
            irisRect[i].set(imageDto.getCenterX() - outerCircleR, imageDto.getCenterY() - outerCircleR, imageDto.getCenterX() + outerCircleR, imageDto.getCenterY() + outerCircleR);
            canvas.drawArc(irisRect[i], startAngle + imageDto.getRotationAngle(), sweepAngle, false, irisArcPaint[i]);
        }
    }

    private void pancreasArcDraw(Canvas canvas) {
        float innerCirCleR = imageDto.getAutonomicNerveWaveRadius();
        float outerCirCleR = (imageDto.getAutonomicNerveWaveRadius() + (imageDto.getIrisRadious() - imageDto.getAutonomicNerveWaveRadius()) / 7 );

        for (int i = 0; i < pancreasOuterArcPaint.length; i++) {
            int startAngle = imageConstant.getPancreasArcPoint().get(imageDto.getSide())[i*2];
            int sweepAngle = imageConstant.getPancreasArcPoint().get(imageDto.getSide())[(i*2)+1];
            pancreasInnerRect[i].set(imageDto.getCenterX() - innerCirCleR, imageDto.getCenterY() - innerCirCleR, imageDto.getCenterX() + innerCirCleR, imageDto.getCenterY() + innerCirCleR);
            canvas.drawArc(pancreasInnerRect[i], startAngle + imageDto.getRotationAngle(), sweepAngle, false, pancreasInnerArcPaint[i]);
            pancreasOuterRect[i].set(imageDto.getCenterX() - outerCirCleR, imageDto.getCenterY() - outerCirCleR, imageDto.getCenterX() + outerCirCleR, imageDto.getCenterY() + outerCirCleR);
            canvas.drawArc(pancreasOuterRect[i], startAngle + imageDto.getRotationAngle(), sweepAngle, false, pancreasOuterArcPaint[i]);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction()&MotionEvent.ACTION_MASK;
        int pointerCount = event.getPointerCount();
        long currTouchTime = System.currentTimeMillis();

        if (pointerCount == 1) {
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    oriX = event.getX();
                    oriY = event.getY();
                case MotionEvent.ACTION_MOVE:
                    if (currTouchTime - lastPinchZoomTime > 200) {
                        float x = event.getX();
                        float y = event.getY();

                        imageDto.setCenterX(imageDto.getCenterX() + (x - oriX));
                        imageDto.setCenterY(imageDto.getCenterY() + (y - oriY));

//                        if (imageDto.getCenterX() - imageDto.getIrisRadious() < 0) {
//                            imageDto.setCenterX(imageDto.getIrisRadious());
//                        } else if (imageDto.getCenterX() + imageDto.getIrisRadious() > imageDto.getWidth()) {
//                            imageDto.setCenterX(imageDto.getWidth() - imageDto.getIrisRadious());
//                        }
//
//                        if (imageDto.getCenterY() - imageDto.getIrisRadious() < 0) {
//                            imageDto.setCenterY(imageDto.getIrisRadious());
//                        } else if (imageDto.getCenterY() + imageDto.getIrisRadious() > imageDto.getLength()) {
//                            imageDto.setCenterY(imageDto.getLength() - imageDto.getIrisRadious());
//                        }

                        if (imageDto.getCenterX() < 0) {
                            imageDto.setCenterX(imageDto.getIrisRadious());
                        } else if (imageDto.getCenterX() > imageDto.getWidth()) {
                            imageDto.setCenterX(imageDto.getWidth() - imageDto.getIrisRadious());
                        }

                        if (imageDto.getCenterY() < 0) {
                            imageDto.setCenterY(imageDto.getIrisRadious());
                        } else if (imageDto.getCenterY() > imageDto.getLength()) {
                            imageDto.setCenterY(imageDto.getLength() - imageDto.getIrisRadious());
                        }

                        oriX = x;
                        oriY = y;
                    }
                    break;
            }
        } else {
            scaleGestureDetector.onTouchEvent(event);
        }

        invalidate();

        return true;
    }

    public class OnPinchListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private final static String TAG_PINCH_LISTENER = "PINCH_LISTENER";
        private Context context;

        public OnPinchListener(Context context) {
            this.context = context;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return super.onScaleBegin(detector);
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            lastPinchZoomTime = System.currentTimeMillis();
            super.onScaleEnd(detector);
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (detector != null) {
                float scaleFactor = detector.getScaleFactor();

                if (state == 0) {
                    imageDto.setPupilRadius(imageDto.getPupilRadius() * scaleFactor);
                } else if (state == 1) {
                    imageDto.setAutonomicNerveWaveRadius(imageDto.getAutonomicNerveWaveRadius() * scaleFactor);
                } else if (state == 2) {
                    imageDto.setIrisRadious(imageDto.getIrisRadious() * scaleFactor);
                }

                if (imageDto.getPupilRadius() + 50 > imageDto.getLength() / 2) {
                    imageDto.setPupilRadius(imageDto.getLength() / 2 - 50);
                }

                if (imageDto.getAutonomicNerveWaveRadius() + 30 > imageDto.getLength() / 2) {
                    imageDto.setAutonomicNerveWaveRadius(imageDto.getLength() / 2 - 30);
                }

                if (imageDto.getIrisRadious() + 10 > imageDto.getLength() / 2) {
                    imageDto.setIrisRadious(imageDto.getLength() / 2 - 10);
                }

                if (imageDto.getPupilRadius() + 20 > imageDto.getAutonomicNerveWaveRadius()) {
                    imageDto.setAutonomicNerveWaveRadius(imageDto.getPupilRadius() + 20);
                }

                if (imageDto.getAutonomicNerveWaveRadius() + 20 > imageDto.getIrisRadious()) {
                    imageDto.setIrisRadious(imageDto.getAutonomicNerveWaveRadius() + 20);
                }

                invalidate();
            }
            return true;
        }
    }

    public Image getImageDto() {
        return imageDto;
    }

    public void setImageDto(Image imageDto) {
        this.imageDto = imageDto;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
