package com.hongbog.irismanualanalysis.capture;

public interface CaptureNavigator {
    void openPatientInfo();
}
