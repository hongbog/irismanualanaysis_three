package com.hongbog.irismanualanalysis.result;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.analysis.AnalysisActivity;
import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.databinding.ActivityResultBinding;
import com.hongbog.irismanualanalysis.util.DateUtil;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.Encryption;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;
import com.hongbog.irismanualanalysis.consent.ConsentActivity;
import com.orhanobut.logger.Logger;

import java.io.File;
import java.util.List;
import java.util.Locale;
import javax.crypto.Cipher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

/**
 * 분석 결과를 뿌려주는 화면
 *
 * @author taein
 * @date 2019-03-07 오후 6:03
 **/
public class ResultActivity extends AppCompatActivity {
    private ResultViewModel mViewModel;
    private ActivityResultBinding binding;
    public static final String IRIS_LEFT = "left";
    public static final String MANAGER_ID = "MANAGER_ID";
    private static final String HONGBOG = "hongbog";
    private final File mGalleryFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), HONGBOG);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mViewModel = ResultActivity.obtainViewModel(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_result);
        final ResultActionListener listener = getResultActionsListener();
        binding.setListener(listener);
        mViewModel = obtainViewModel(this);
        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
        binding.setIsCurrentLeftBitmap(true);

        try {
            mViewModel.start((UserHistory)getIntent().getExtras().getParcelable(AnalysisActivity.USER_HISTORY), getFilesDir());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupObserve();

        if (DisplayUtil.isVerticalDisplay(this) >= 920) {
            binding.resultScrollView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mViewModel.setCountScrollY(binding.resultScrollView.getScrollY());
                    return false;
                }
            });
            mViewModel.getCountScrollY().observe(this, scrollY -> {
                if (scrollY <= binding.resultLeftLayout.getHeight() - 30) {
                    binding.setIsCurrentLeftBitmap(true);
                } else {
                    binding.setIsCurrentLeftBitmap(false);
                }
            });
        }
        try {
            onManagerLoaded();
            // 앞에서 선택한 홍채 정보 및 이미지를 DB에 넣기
            mViewModel.insertAnalysisToDB();
            // 스크립트 결과 프린트
            printScriptResult();
            printOrganScriptResult();
        } catch (Exception e) {
            Logger.e(e, e.getMessage());
        }
        // 결과 디스크에 떨구기
        mViewModel.saveScriptToDisk();
    }
    /**
     * 이미지, json 을 저장 후 LiveData 의 구독자에 의해 호출됨
     * 저장을 실패 한 경우 Toast 메세지를 띄운다.
     * 저장을 성공 한 경우 미디어 스캔을 시도 한다.
     * @author taein
     */
    private void setupObserve() {
        mViewModel.getScanGallery().observe(this, scanFolderForGallery -> {
            if (scanFolderForGallery == null) {
                Toast.makeText(this, R.string.not_write_png, Toast.LENGTH_SHORT).show();
            } else {
                scanMediaFolder(scanFolderForGallery);
            }
        });
    }
    public void onManagerLoaded() {
        final String currentDate = DateUtil.getCurrentDate(DateUtil.DatePattern.NoWhiteSpace);
        File ftpPath = new File(getFilesDir(), File.separator + currentDate);
        File galleryPath = new File(mGalleryFolder, File.separator + currentDate);
        mViewModel.saveIrisInfoTo(ftpPath);
        mViewModel.saveIrisImageTo(galleryPath);
    }

    /**
     * 스트레스링, 콜레스테롤링 스크립트 출력.
     * 암호화된 스크립트를 복호화해서 해당 textView에 뿌려줌
     * @throws Exception
     * @Author jslee
     */
    private void printScriptResult() throws Exception {
        int userHistoryId = mViewModel.getUserHistoryId2();

        String languageCode = Locale.getDefault().getLanguage();
        List<AnalysisScriptResult> scriptResultList = mViewModel.getAnalysisScript(userHistoryId, languageCode);
        if (scriptResultList != null) {
            for (int i = 0; i < scriptResultList.size(); i++) {
                String code = scriptResultList.get(i).category;
                AnalysisScriptResult scriptResult = scriptResultList.get(i);
                if (scriptResultList.get(i).side.equals(IRIS_LEFT)) {
                    if (code.equals(Category.cholesterolRing)) {
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setLeftCholesterolRingViewBol(true);
                            TextView lcrn = binding.leftCholesterolRingNormal.findViewById(R.id.result_normal);
                            lcrn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setLeftCholesterolRingViewBol(false);
                            TextView lcra_symptom = binding.leftCholesterolRingAbnormal.findViewById(R.id.result_symptom);
                            TextView lcra_precaution = binding.leftCholesterolRingAbnormal.findViewById(R.id.result_precaution);
                            TextView lcra_recommend = binding.leftCholesterolRingAbnormal.findViewById(R.id.result_recommend);
                            TextView lcra_avoid = binding.leftCholesterolRingAbnormal.findViewById(R.id.result_avoid);
                            lcra_symptom.setText(decrypt(scriptResult.getSymptom()));
                            lcra_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            lcra_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            lcra_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    } else {
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setLeftStressRingViewBol(true);
                            TextView lsrn = binding.leftStressRingNormal.findViewById(R.id.result_normal);
                            lsrn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setLeftStressRingViewBol(false);
                            TextView lsra_symptom = binding.leftStressRingAbnormal.findViewById(R.id.result_symptom);
                            TextView lsra_precaution = binding.leftStressRingAbnormal.findViewById(R.id.result_precaution);
                            TextView lsra_recommend = binding.leftStressRingAbnormal.findViewById(R.id.result_recommend);
                            TextView lsra_avoid = binding.leftStressRingAbnormal.findViewById(R.id.result_avoid);
                            lsra_symptom.setText(decrypt(scriptResult.getSymptom()));
                            lsra_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            lsra_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            lsra_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }
                } else {
                    if (code.equals(Category.cholesterolRing)) {
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightCholesterolRingViewBol(true);
                            TextView rcrn = binding.rightCholesterolRingNormal.findViewById(R.id.result_normal);
                            rcrn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightCholesterolRingViewBol(false);
                            TextView rcra_symptom = binding.rightCholesterolRingAbnormal.findViewById(R.id.result_symptom);
                            TextView rcra_precaution = binding.rightCholesterolRingAbnormal.findViewById(R.id.result_precaution);
                            TextView rcra_recommend = binding.rightCholesterolRingAbnormal.findViewById(R.id.result_recommend);
                            TextView rcra_avoid = binding.rightCholesterolRingAbnormal.findViewById(R.id.result_avoid);
                            rcra_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rcra_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rcra_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rcra_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    } else {
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightStressRingViewBol(true);
                            TextView rsrn = binding.rightStressRingNormal.findViewById(R.id.result_normal);
                            rsrn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightStressRingViewBol(false);
                            TextView rsra_symptom = binding.rightStressRingAbnormal.findViewById(R.id.result_symptom);
                            TextView rsra_precaution = binding.rightStressRingAbnormal.findViewById(R.id.result_precaution);
                            TextView rsra_recommend = binding.rightStressRingAbnormal.findViewById(R.id.result_recommend);
                            TextView rsra_avoid = binding.rightStressRingAbnormal.findViewById(R.id.result_avoid);
                            rsra_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rsra_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rsra_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rsra_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }
                }
            }
        }
    }
    /**
     * 장기(뇌, 신장, 폐, 간, 췌장) 스크립트 출력.
     * 암호화된 스크립트를 복호화해서 해당 textView에 뿌려줌
     * @throws Exception
     * @Author jslee
     */
    private void printOrganScriptResult() throws Exception {
        int userHistoryId = mViewModel.getUserHistoryId2();

        String languageCode = Locale.getDefault().getLanguage();
        List<OrganScriptResult> organScriptResultList = mViewModel.getOrganScript(userHistoryId, languageCode);
        if(organScriptResultList != null){
            for(int i = 0; i< organScriptResultList.size(); i++){
                String code = organScriptResultList.get(i).category;
                OrganScriptResult scriptResult = organScriptResultList.get(i);
                if(organScriptResultList.get(i).side.equals(IRIS_LEFT)){
                    if(code.equals(Category.brain)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setLeftBrainViewBol(true);
                            TextView lbn = binding.leftBrainNormal.findViewById(R.id.result_normal);
                            lbn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setLeftBrainViewBol(false);
                            TextView lba_symptom = binding.leftBrainAbnormal.findViewById(R.id.result_symptom);
                            TextView lba_precaution = binding.leftBrainAbnormal.findViewById(R.id.result_precaution);
                            TextView lba_recommend = binding.leftBrainAbnormal.findViewById(R.id.result_recommend);
                            TextView lba_avoid = binding.leftBrainAbnormal.findViewById(R.id.result_avoid);
                            lba_symptom.setText(decrypt(scriptResult.getSymptom()));
                            lba_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            lba_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            lba_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }else if(code.equals(Category.kidney)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setLeftKidneyViewBol(true);
                            TextView lkn = binding.leftKidneyNormal.findViewById(R.id.result_normal);
                            lkn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setLeftKidneyViewBol(false);
                            TextView lka_symptom = binding.leftKidneyAbnormal.findViewById(R.id.result_symptom);
                            TextView lka_precaution = binding.leftKidneyAbnormal.findViewById(R.id.result_precaution);
                            TextView lka_recommend = binding.leftKidneyAbnormal.findViewById(R.id.result_recommend);
                            TextView lka_avoid = binding.leftKidneyAbnormal.findViewById(R.id.result_avoid);
                            lka_symptom.setText(decrypt(scriptResult.getSymptom()));
                            lka_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            lka_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            lka_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }else if(code.equals(Category.lung)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setLeftLungViewBol(true);
                            TextView lln = binding.leftLungNormal.findViewById(R.id.result_normal);
                            lln.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setLeftLungViewBol(false);
                            TextView lla_symptom = binding.leftLungAbnormal.findViewById(R.id.result_symptom);
                            TextView lla_precaution = binding.leftLungAbnormal.findViewById(R.id.result_precaution);
                            TextView lla_recommend = binding.leftLungAbnormal.findViewById(R.id.result_recommend);
                            TextView lla_avoid = binding.leftLungAbnormal.findViewById(R.id.result_avoid);
                            lla_symptom.setText(decrypt(scriptResult.getSymptom()));
                            lla_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            lla_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            lla_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }

                    }else if(code.equals(Category.pancreas)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setLeftPancreasViewBol(true);
                            TextView lpn = binding.leftPancreasNormal.findViewById(R.id.result_normal);
                            lpn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setLeftPancreasViewBol(false);
                            TextView lpa_symptom = binding.leftPancreasAbnormal.findViewById(R.id.result_symptom);
                            TextView lpa_precaution = binding.leftPancreasAbnormal.findViewById(R.id.result_precaution);
                            TextView lpa_recommend = binding.leftPancreasAbnormal.findViewById(R.id.result_recommend);
                            TextView lpa_avoid = binding.leftPancreasAbnormal.findViewById(R.id.result_avoid);
                            lpa_symptom.setText(decrypt(scriptResult.getSymptom()));
                            lpa_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            lpa_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            lpa_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }
                    else {
                    }
                }
                else{
                    if(code.equals(Category.brain)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightBrainViewBol(true);
                            TextView rbn = binding.rightBrainNormal.findViewById(R.id.result_normal);
                            rbn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightBrainViewBol(false);
                            TextView rba_symptom = binding.rightBrainAbnormal.findViewById(R.id.result_symptom);
                            TextView rba_precaution = binding.rightBrainAbnormal.findViewById(R.id.result_precaution);
                            TextView rba_recommend = binding.rightBrainAbnormal.findViewById(R.id.result_recommend);
                            TextView rba_avoid = binding.rightBrainAbnormal.findViewById(R.id.result_avoid);
                            rba_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rba_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rba_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rba_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }else if(code.equals(Category.kidney)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightKidneyViewBol(true);
                            TextView rkn = binding.rightKidneyNormal.findViewById(R.id.result_normal);
                            rkn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightKidneyViewBol(false);
                            TextView rka_symptom = binding.rightKidneyAbnormal.findViewById(R.id.result_symptom);
                            TextView rka_precaution = binding.rightKidneyAbnormal.findViewById(R.id.result_precaution);
                            TextView rka_recommend = binding.rightKidneyAbnormal.findViewById(R.id.result_recommend);
                            TextView rka_avoid = binding.rightKidneyAbnormal.findViewById(R.id.result_avoid);
                            rka_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rka_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rka_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rka_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }else if(code.equals(Category.lung)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightLungViewBol(true);
                            TextView rln = binding.rightLungNormal.findViewById(R.id.result_normal);
                            rln.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightLungViewBol(false);
                            TextView rla_symptom = binding.rightLungAbnormal.findViewById(R.id.result_symptom);
                            TextView rla_precaution = binding.rightLungAbnormal.findViewById(R.id.result_precaution);
                            TextView rla_recommend = binding.rightLungAbnormal.findViewById(R.id.result_recommend);
                            TextView rla_avoid = binding.rightLungAbnormal.findViewById(R.id.result_avoid);
                            rla_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rla_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rla_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rla_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }else if(code.equals(Category.liver)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightLiverViewBol(true);
                            TextView rln = binding.rightLiverNormal.findViewById(R.id.result_normal);
                            rln.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightLiverViewBol(false);
                            TextView rla_symptom = binding.rightLiverAbnormal.findViewById(R.id.result_symptom);
                            TextView rla_precaution = binding.rightLiverAbnormal.findViewById(R.id.result_precaution);
                            TextView rla_recommend = binding.rightLiverAbnormal.findViewById(R.id.result_recommend);
                            TextView rla_avoid = binding.rightLiverAbnormal.findViewById(R.id.result_avoid);
                            rla_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rla_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rla_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rla_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }else if(code.equals(Category.pancreas)){
                        if (decrypt(scriptResult.getPrecaution()).trim().equals(getResources().getString(R.string.content_normal))) {
                            binding.setRightPancreasViewBol(true);
                            TextView rpn = binding.rightPancreasNormal.findViewById(R.id.result_normal);
                            rpn.setText(decrypt(scriptResult.getPrecaution()));
                        } else {
                            binding.setRightPancreasViewBol(false);
                            TextView rpa_symptom = binding.rightPancreasAbnormal.findViewById(R.id.result_symptom);
                            TextView rpa_precaution = binding.rightPancreasAbnormal.findViewById(R.id.result_precaution);
                            TextView rpa_recommend = binding.rightPancreasAbnormal.findViewById(R.id.result_recommend);
                            TextView rpa_avoid = binding.rightPancreasAbnormal.findViewById(R.id.result_avoid);
                            rpa_symptom.setText(decrypt(scriptResult.getSymptom()));
                            rpa_precaution.setText(decrypt(scriptResult.getPrecaution()));
                            rpa_recommend.setText(decrypt(scriptResult.getHealthy_food()));
                            rpa_avoid.setText(decrypt(scriptResult.getUnhealthy_food()));
                        }
                    }
                    else{

                    }
                }
            }
        }
    }

    /**
     * 스크립트를 복호화해서 스트링으로 출력
     * @param string
     * @return
     * @throws Exception
     */
    private String decrypt(String string) throws Exception {
        Encryption encryption = Encryption.getInstance();
        string = encryption.encryptOrDecrypt(string,  Cipher.DECRYPT_MODE);
        return string;
    }

    /**
     * 해당 경로를 미디어 스캔 한다.
     * @author taein
     * @param scanFolderForGallery
     */
    private void scanMediaFolder(File scanFolderForGallery) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(scanFolderForGallery);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    public static ResultViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencises into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(ResultViewModel.class);
    }

    @Override
    public void onBackPressed() {
        Image.removeStaticVariable();

        Intent intent = new Intent(this, ConsentActivity.class);
        intent.putExtra(MANAGER_ID, mViewModel.getUserHistory().getManagerId());
        startActivity(intent);
        finish();
    }
    /**
     * 건강보조식품 링크로 연결되는 버튼(onClickRow1-3)
     * 이메일 보내기로 연결되는 버튼(onClickRow4)
     * @return
     * @Author jslee
     */
    private ResultActionListener getResultActionsListener() {
        return new ResultActionListener() {
            @Override
            public void onClickRow1(View view) {
                Uri uri = Uri.parse("http://www.cjonmart.net/index.do");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
            @Override
            public void onClickRow2(View view) {
                Uri uri = Uri.parse("https://www.vitalbeautie.com/kr/ko.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
            @Override
            public void onClickRow3(View view) {
                Uri uri = Uri.parse("https://www.templestay.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
            @Override
            public void onClickRow4(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        sendEmail();
                    }
                }).start();
            }
        };
    }
    /**
     * 이메일 보내기
     * @Author jslee
     *saveResultToDisk
     */
    private void sendEmail(){
        //String uri=getScreenShot();
        SendEmail sendEmail = new SendEmail();

        String refinedScript = mViewModel.makeScriptResult();
        String userEmail = mViewModel.getUserId();


        Intent intent =  sendEmail.sendResultScriptByEmail(refinedScript, userEmail);
        startActivity(intent);
    }
}

