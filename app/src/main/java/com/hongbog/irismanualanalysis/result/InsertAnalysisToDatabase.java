package com.hongbog.irismanualanalysis.result;

import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

public class InsertAnalysisToDatabase {
    public static final String IRIS_LEFT = "left";
    public static final String IRIS_RIGHT = "right";

    private ManualIrisAnalysisRepository mRepository;
    private UserHistory mUserHistory;
    private int mUserHistoryId;

    public InsertAnalysisToDatabase(ManualIrisAnalysisRepository mRepository, UserHistory mUserHistory, int mUserHistoryId) {
        this.mRepository = mRepository;
        this.mUserHistory = mUserHistory;
        this.mUserHistoryId = mUserHistoryId;
    }

    public void insertAllImage() {
        List<Image> images = getIrisImageList();
        mRepository.insertAllImage(images);
    }

    /**
     * 홍채 이미지 정보 저장
     * 왼쪽, 오른쪽 정보를 list로 만들어 list 자체를 insert
     * @return
     * @Author jslee
     */
    private List<Image> getIrisImageList(){
        List<Image> imageList = new ArrayList<>();
        Image leftImage = mUserHistory.getLeftImage();
        leftImage.setUserHistoryId(mUserHistoryId);
        Logger.i("leftImage.getIrisBitmapPath() : '%s'", leftImage.getIrisBitmapPath());
        Logger.i("leftImage.getIrisMapBitmapPath() : '%s'", leftImage.getIrisMapBitmapPath());
        imageList.add(leftImage);

        Image rightImage = mUserHistory.getRightImage();
        rightImage.setUserHistoryId(mUserHistoryId);
        Logger.i("rightImage.getIrisBitmapPath() : '%s'", rightImage.getIrisBitmapPath());
        Logger.i("rightImage.getIrisMapBitmapPath() : '%s'", rightImage.getIrisMapBitmapPath());
        imageList.add(rightImage);
        return imageList;
    }

    public void insertAllIrisAnalysis() {
        List<IrisAnalysis> irisAnalysis = getIrisAnalysisList();
        mRepository.insertAllIrisAnalysis(irisAnalysis);
    }


    /**
     * 홍채 분석 정보(앞에서 선택한 정보) 저장
     * 정보를 list로 만들어 list 자체를 insert
     * @return
     * @Author jslee
     */
    private List<IrisAnalysis> getIrisAnalysisList(){
        List<IrisAnalysis> irisAnalysisList = new ArrayList<>();
        String stressRingCode = mUserHistory.getLeftImage().getBrainOrganAnalysis().getStressRingCode();
        String cholesterolRingCode = mUserHistory.getLeftImage().getBrainOrganAnalysis().getCholesterolRingCode();

        for(int i = 0; i<Category.left.length; i++){
            irisAnalysisList.add(generateIrisAnalysis(IRIS_LEFT, stressRingCode, cholesterolRingCode, Category.left[i]));
        }

        stressRingCode = mUserHistory.getRightImage().getBrainOrganAnalysis().getStressRingCode();
        cholesterolRingCode = mUserHistory.getRightImage().getBrainOrganAnalysis().getCholesterolRingCode();
        for(int i=0; i<Category.right.length;i++){
            irisAnalysisList.add(generateIrisAnalysis(IRIS_RIGHT, stressRingCode, cholesterolRingCode, Category.right[i]));
        }

        Logger.i("irisAnalysisList : '%s'", irisAnalysisList);
        return irisAnalysisList;
    }
    private IrisAnalysis generateIrisAnalysis(String side, String stressRingCode, String cholesterolRingCode, String organCode) {
        IrisAnalysis irisAnalysis = new IrisAnalysis(mUserHistoryId, side);
        irisAnalysis.setStressRingCode (stressRingCode);
        irisAnalysis.setCholesterolRingCode (cholesterolRingCode);
        irisAnalysis.setOrganCode(organCode);
        return irisAnalysis;
    }

    public void insertAllOrganAnalysis() {
        List<OrganAnalysis> organAnalyses = getIrisOrganAnalysisList();
        mRepository.insertAllOrganAnalysis(organAnalyses);
    }

    /**
     * 장기 분석 정보(앞에서 선택한 정보) 저장
     * 정보를 list로 만들어 list 자체를 insert
     * @return
     * @Author jslee
     */
    private List<OrganAnalysis> getIrisOrganAnalysisList(){
        List<OrganAnalysis> organAnalysisList = new ArrayList<>();

        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis()));

        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis()));
        organAnalysisList.add(generateOrganAnalysis(mUserHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis()));

        Logger.i("organAnalysisList : '%s'", organAnalysisList);
        return organAnalysisList;
    }

    private OrganAnalysis generateOrganAnalysis(OrganAnalysis organAnalysis) {
        OrganAnalysis newOrganAnalysis = new OrganAnalysis(mUserHistoryId, organAnalysis.getSide(), organAnalysis.getOrganCode(), organAnalysis.getNormal(),
                organAnalysis.getLacuna(), organAnalysis.getSpoke(), organAnalysis.getPigmentSpot(), organAnalysis.getDefect());
        return newOrganAnalysis;
    }


}
