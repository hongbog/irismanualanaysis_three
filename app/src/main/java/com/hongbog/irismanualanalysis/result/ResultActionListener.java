package com.hongbog.irismanualanalysis.result;

import android.view.View;

/**
 *
 */
public interface ResultActionListener {
    void onClickRow1(View view);
    void onClickRow2(View view);
    void onClickRow3(View view);
    void onClickRow4(View view);

}

