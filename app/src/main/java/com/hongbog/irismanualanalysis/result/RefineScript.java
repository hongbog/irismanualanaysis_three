package com.hongbog.irismanualanalysis.result;

import android.app.Application;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import com.hongbog.irismanualanalysis.data.entity.Script;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.hongbog.irismanualanalysis.util.Encryption;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import javax.crypto.Cipher;

import static com.hongbog.irismanualanalysis.login.LoginActivity.TAG;

public class RefineScript {
    private ManualIrisAnalysisRepository mRepository;
    private String languageCode = Locale.getDefault().getLanguage();

    public RefineScript(Application app) {
        this.mRepository = new ManualIrisAnalysisRepository(app);
    }

    /**
     * Result 스크립트 파일에 떨구기
     * @param userHistory
     */
    public void saveResultToDisk(UserHistory userHistory){
        String rightImage =  userHistory.getRightImage().getIrisBitmapPath();
        String leftImage = userHistory.getLeftImage().getIrisBitmapPath();
        String userEmail = userHistory.getUserId();
        String resultString = makeScriptResult(userHistory);
        resultString = makeResultString(rightImage, leftImage, resultString, userEmail);
        String fileName = "R_"+ userHistory.getAnalysisDate() + "_" + userEmail;
        String folderName = userHistory.getManagerId();
        saveTextFile(resultString, fileName, folderName);

    }

    /**
     *  Result 스크립트 이메일로 보내기
     * @param userHistory
     * @return
     */
    public Intent sendResultScriptByEmail(UserHistory userHistory){
        String resultString = makeScriptResult(userHistory);
        String subject = selectTitle();
        String userEmail = userHistory.getUserId();
        String[] address = {userEmail};
        //String[] address = {"jslee_314@naver.com"};

        return sendMail(address , subject , resultString);
    }

    /**
     *
     * @param userHistory
     * @param jsonFileString
     */
    public void saveJSONToDisk(UserHistory userHistory, String jsonFileString){
        String userEmail = userHistory.getUserId();
        String fileName = userHistory.getAnalysisDate() + "_" + userEmail;
        String folderName = userHistory.getManagerId();
        saveTextFile(jsonFileString, fileName, folderName);

    }

    /**
     * ScriptResult, OrganScriptResult 결과 string 으로 출력
     * @param userHistory
     * @return
     */
    public String makeScriptResult(UserHistory userHistory){

        String string ="";
        try {
            String leftC =  userHistory.getLeftImage().getKidneyOrganAnalysis().getCholesterolRingCode();
            String leftS = userHistory.getLeftImage().getKidneyOrganAnalysis().getStressRingCode();
            String rightC =  userHistory.getRightImage().getKidneyOrganAnalysis().getCholesterolRingCode();
            String rightS = userHistory.getRightImage().getKidneyOrganAnalysis().getStressRingCode();

            string = "\n\nLEFT EYE ANALYSIS\n";
            string = string +  "\n\n[ Cholesterol Analysis ]\n";
            Script scriptResult =mRepository.getScriptByID(leftC, languageCode);
            if(leftC.equals("CT1")){
                string = string + decryptNormal(scriptResult);
            }else {
                string = string + decrypt(scriptResult);
            }
            string = string +  "\n\n[ StressRing Analysis ]\n";
            scriptResult =mRepository.getScriptByID(leftS, languageCode);
            if(leftS.equals("ST1") ){
                string = string + decryptNormal(scriptResult);
            }else {
                string = string + decrypt(scriptResult);
            }

            string = string +  "\n\n\n\n[ Brain Analysis ]\n";
            if(userHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("BRNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else {
                if( userHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string = string +  "\n\n[ Kidney Analysis ]\n";
            if( userHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("KDNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else {
                if( userHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string = string +  "\n\n[ Pancreas Analysis ]\n";
            if( userHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("PCNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else {
                if( userHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string = string +  "\n\n[ Lung Analysis ]\n";
            if( userHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("LGNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else {
                if( userHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(  userHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(  userHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string =  string +"\n\nRIGHT EYE ANALYSIS\n";

            string = string +  "\n\n[ Cholesterol Analysis ]\n";
            scriptResult =mRepository.getScriptByID(rightC, languageCode);
            if(rightC.equals("CT1")){
                string = string + decryptNormal(scriptResult);
            }else {
                string = string + decrypt(scriptResult);
            }

            string = string +  "\n\n[ StressRing Analysis ]\n";
            scriptResult =mRepository.getScriptByID(rightS, languageCode);
            if(rightS.equals("ST1")){
                string = string + decryptNormal(scriptResult);
            }else {
                string = string + decrypt(scriptResult);
            }

            Log.d("mystring", string);

            string = string +  "\n\n[ Brain Analysis ]\n";
            if( userHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("BRDF", languageCode);
                string = string + decryptNormal(scriptResult);
            }else {
                if( userHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(  userHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("BRSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string = string +  "\n\n[ Kidney Analysis ]\n";
            if( userHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("KDNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else{
                if( userHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(  userHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("KDSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string = string +  "\n\n[ Liver Analysis ]\n";
            //if( scriptResult.getPrecaution().equals("You are in great condition.")) {
            if( userHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")) {

                scriptResult = mRepository.getScriptByID("LVNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else{
                if( userHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LVDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LVLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LVPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LVSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }
            string = string +  "\n\n[ Pancreas Analysis ]\n";
            if( userHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")){
                scriptResult =mRepository.getScriptByID("PCNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else {
                if( userHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("PCSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }

            string = string +  "\n\n[ Lung Analysis ]\n";
            if( userHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis().getNormal().equals("Y")) {
                scriptResult = mRepository.getScriptByID("LGNM", languageCode);
                string = string + decryptNormal(scriptResult);
            }else{
                if( userHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis().getDefect().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGDF", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(userHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis().getLacuna().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGLA", languageCode);
                    string = string + decrypt(scriptResult);
                }else if(  userHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis().getPigmentSpot().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGPS", languageCode);
                    string = string + decrypt(scriptResult);
                }else if( userHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis().getSpoke().equals("Y")){
                    scriptResult =mRepository.getScriptByID("LGSP", languageCode);
                    string = string + decrypt(scriptResult);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //string = string +attachString;
        return string;
    }
    /**
     *
     * @param scriptResult
     * @return
     * @throws Exception
     */
    private String decryptNormal(Script scriptResult) throws Exception {
        Encryption encryption = Encryption.getInstance();
        String string =encryption.encryptOrDecrypt(scriptResult.getPrecaution(),  Cipher.DECRYPT_MODE)+ "\n";
        return string;
    }
    /**
     *
     * @param scriptResult
     * @return
     * @throws Exception
     */
    private String decrypt(Script scriptResult) throws Exception {
        Encryption encryption = Encryption.getInstance();
        String string = "======== precaution ======== \n"+encryption.encryptOrDecrypt(scriptResult.getPrecaution(),  Cipher.DECRYPT_MODE)+ "\n"+
                "======== symptom ======== \n"+ encryption.encryptOrDecrypt(scriptResult.getSymptom(),  Cipher.DECRYPT_MODE)+ "\n"+
                "======== healthy_food ======== \n"+encryption.encryptOrDecrypt(scriptResult.getHealthyFood(),  Cipher.DECRYPT_MODE)+ "\n"+
                "======== unhealthy_food ======== \n"+encryption.encryptOrDecrypt(scriptResult.getUnhealthyFood(),  Cipher.DECRYPT_MODE)+ "\n";
        return string;
    }
    /**
     *
     * @param rightImage
     * @param leftImage
     * @param resultString
     * @param userEmail
     * @return
     */
    private String makeResultString(String rightImage, String leftImage, String resultString, String userEmail) {
        return userEmail+ "\n" + rightImage + "\n" + leftImage + "\n" + resultString;
    }
    /**
     * contents String 변수의 내용을 스마트폰 내부 저장소에 txt파일로 만들어서 떨군다
     * @param contents
     * @param filename : R(result) + 현재시간 + user  의 이메일주소로 파일이름만든다
     * @param folderName : ManagerId로 폴더 만들어서 그 안에 파일들을 떨군다
     */
    private void saveTextFile(String contents, String filename, String folderName){
        File saveFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+folderName);
        if(!saveFile.exists()){ // 폴더 없을 경우
            saveFile.mkdir(); // 폴더 생성
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(saveFile+"/" + filename +".txt", true));
            buf.append(contents); // 파일 쓰기
            buf.newLine(); // 개행
            buf.close();

        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

    }
    /**
     *  언어별 이메일의 제목 선택
     * @return
     */
    private String selectTitle(){
        String subject = "";

        if(languageCode =="ko"){
            subject = "홍채 분석 결과(HONGBOG)";
        }else if(languageCode =="zh"){
            subject = "健康分析结果(HONGBOG)";
        }else{
            subject = "Analysis result from HONGBOG ";
        }
        return subject;

    }
    /**
     *
     * @param sendMailAddress
     * @param subject
     * @param content
     * @return
     */
    private Intent sendMail(String[] sendMailAddress , String subject , String content){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plaine/text");
        intent.putExtra(Intent.EXTRA_EMAIL, sendMailAddress);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        return intent;

    }

}
