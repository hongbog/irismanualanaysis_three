package com.hongbog.irismanualanalysis.result;

import android.app.Application;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.google.android.gms.common.util.DataUtils;
import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.dataSource.ManualIrisAnalysisDataSource;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.hongbog.irismanualanalysis.util.DateUtil;
import com.hongbog.irismanualanalysis.util.Encryption;
import com.hongbog.irismanualanalysis.util.FileUtil;
import com.orhanobut.logger.Logger;

import org.apache.commons.net.ftp.FTP;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class ResultViewModel extends AndroidViewModel
        implements ManualIrisAnalysisDataSource.IrisImageSaveToGalleryCallback,
        ManualIrisAnalysisDataSource.IrisInfoSaveToFtpCallback,
        ManualIrisAnalysisDataSource.GetManagerCallback{
    private ManualIrisAnalysisRepository mRepository;
    private String mUserId;
    private UserHistory mUserHistory;
    private int mUserHistoryId;
    private MutableLiveData<Boolean> mEnableLoading = new MutableLiveData<>();
    private MutableLiveData<File> mScanGallery = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mLeftIrisBitmap = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mRightIrisBitmap = new MutableLiveData<>();
    private MutableLiveData<Integer> mCountScrollY = new MutableLiveData<>();

    private static final String HONGBOG = "hongbog";
    private final File mGalleryFolder =
            new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), HONGBOG);
    private File mFtpFolder;
    private final Bitmap.CompressFormat BITMAP_FORMAT = Bitmap.CompressFormat.PNG;

    public LiveData<Bitmap> getLeftIrisBitmap() {
        return mLeftIrisBitmap;
    }

    public LiveData<Bitmap> getRightIrisBitmap() {
        return mRightIrisBitmap;
    }

    public LiveData<File> getScanGallery() {
        return mScanGallery;
    }

    public LiveData<Boolean> getEnableLoading() {
        return mEnableLoading;
    }

    public LiveData<Integer> getCountScrollY() { return mCountScrollY; }

    public ResultViewModel(@NonNull Application application) {
        super(application);
        mRepository = ManualIrisAnalysisRepository.getInstance(application);
    }





    public void start(@NonNull UserHistory userHistory, File ftpPath) throws Exception {
        mUserHistory = userHistory;
        mFtpFolder = ftpPath;
        mUserHistoryId = getUserHistoryId();
        enableLoading();
        setUpIrisImageView();

//        getManagerBy(userHistory.getManagerId());
    }

    /**
     * 앞에서 넘겨받은 user에 담겨있는 ManagerId, UserID 와 현재 analysisData를 생성하여
     * userHistory insert 하고 Auto로 만들어진userHistoryID 받아오기.
     * 그리고 user의 LatelyAnalysisData를 현재 analysisData로 update
     * (가져온 userHistoryID를 기반으로 image, 분석정보등을 저장할것임)
     * @Author jslee
     */
    public int getUserHistoryId() throws Exception {

        // 1. update UserDate ByID
        String analysisDate  = DateUtil.getCurrentdateFormet();
        mUserHistory.setAnalysisDate(analysisDate);

        String managerId = mUserHistory.getManagerId();

        mUserId = mUserHistory.getUserId();
        Log.d("jslee", "updateUserDateByID  : " + analysisDate +" / "+ managerId +" / "+ mUserId);

        String decryptUserId = decryptUserId(mUserId);
        Log.d("jslee", "updateUserDateByID  : " + analysisDate +" / "+ managerId +" / "+ decryptUserId);
        mRepository.updateUserDateByID(analysisDate, managerId, decryptUserId);

        //mUserHistory.setUserId(userId);
        //mUserHistory.setAnalysisDate(analysisDate);

        // 2. insert
        mUserHistory.setUserId(decryptUserId);

        // 2. insert UserHistory
        Log.d("jslee", "insertUserHistory  : " +" / "+ mUserHistory.getManagerId() +" / "+ mUserHistory.getUserId()  +" / "+ mUserHistory.getAnalysisDate());
        mRepository.insertUserHistory(mUserHistory);

        // 3. getUserHistory Id
        Log.d("jslee", "insertUserHistory  "  + managerId +" / "+ decryptUserId +" / "+ analysisDate);
        mUserHistoryId = mRepository.getUserHistoryId(managerId, decryptUserId, analysisDate);
        Log.d("jslee","mUserHistoryId : " + mUserHistoryId);



        setUserHistoryId(mUserHistoryId);
        return mUserHistoryId;
    }
    public int getUserHistoryId2(){
        return mUserHistoryId;
    }
    public String getUserId(){
        return mUserId;
    }
    /**
     * UserID 복호화 한다.
     * @param userId
     * @return
     * @throws Exception
     */
    public String decryptUserId(String userId) throws Exception {
        Encryption encryption = Encryption.getInstance();
        return encryption.encryptOrDecrypt(userId,  Cipher.DECRYPT_MODE);
    }



    public void insertAnalysisToDB(){
        InsertAnalysisToDatabase insertToDB = new InsertAnalysisToDatabase(mRepository, mUserHistory, mUserHistoryId);
        insertToDB.insertAllImage();
        insertToDB.insertAllIrisAnalysis();
        insertToDB.insertAllOrganAnalysis();
    }

    /**
     * 동공/자율신경환/스트레스링/콜레스테롤링 스크립트 출력 SQL
     * @param userHistoryId
     * @return
     * @Author jslee
     */
    public List<AnalysisScriptResult> getAnalysisScript(int userHistoryId, String languageCode) {
        return mRepository.getAnalysisScript(userHistoryId, languageCode);
    }

    /**
     *  장기 패턴 스크립트 출력 SQL
     * @param userHistoryId
     * @return
     * @Author jslee
     */
    public List<OrganScriptResult> getOrganScript(int userHistoryId, String languageCode) {
        return mRepository.getOrganScript(userHistoryId, languageCode);
    }

    public UserHistory getUserHistory() {
        return mUserHistory;
    }



    public void setCountScrollY(int y) {
        mCountScrollY.setValue(y);
    }

    /**
     * 로딩바를 enable 한다.
     * 라이브 데이터를 숨겨 한 단계 추상화 시킨다.
     * @author taein
     */
    private void enableLoading() {
        mEnableLoading.setValue(true);
    }


    /**
     * 홍채 ImageView 에 사용자 홍채 이미지를 설정 한다.
     * @author taein
     */
    private void setUpIrisImageView() {
        mLeftIrisBitmap.setValue(Image.leftIrisBitmap);
        mRightIrisBitmap.setValue(Image.rightIrisBitmap);
    }
    /**
     * Manager 를 읽어 온 후 Agency 와 현재 날짜로 ftp 경로를 생성 한다.
     * ftp 경로에 홍채 정보를 저장 한다.
     * 갤러리 경로에 홍채 정보를 저장 한다.
     * @author taein
     * @param manager
     */
    @Override
    public void onManagerLoaded(Manager manager) {
        final String agencyName = manager.getManagerAgency();
        final String currentDate = DateUtil.getCurrentDate(DateUtil.DatePattern.NoWhiteSpace);
        File ftpPath = new File(mFtpFolder, agencyName + File.separator + currentDate);
        File galleryPath = new File(mGalleryFolder, agencyName + File.separator + currentDate);

        saveIrisInfoTo(ftpPath);
        saveIrisImageTo(galleryPath);
    }

    /**
     * DCIM/hongbog/agencyName/currentDate 경로에
     * 홍채 좌/우 이미지를 저장 한다.
     * @author taein
     * @param galleryPath
     */
    public void saveIrisImageTo(File galleryPath) {
        mRepository.saveIrisImageTo(galleryPath,this);
    }

    /**
     * 해당 디렉토리에 홍채 정보를 저장 한다.
     * @author taein
     * @param ftpPath
     */
    public void saveIrisInfoTo(File ftpPath) {
        final String leftIrisImageFileName = FileUtil.getUniqueFileName();
        final String rightIrisImageFileName = FileUtil.getUniqueFileName();
        final String leftIrisMapImageFileName = FileUtil.getUniqueFileName();
        final String rightIrisMapImageFileName = FileUtil.getUniqueFileName();

        // ftp 저장 (왼쪽 홍채 이미지, 오른쪽 홍채 이미지)
        final File leftIrisImageForFtp = new FileUtil.FileWithDotForExtension(ftpPath,
                leftIrisImageFileName, BITMAP_FORMAT.toString());
        final File rightIrisImageForFtp = new FileUtil.FileWithDotForExtension(ftpPath,
                rightIrisImageFileName, BITMAP_FORMAT.toString());

        // ftp 저장 (왼쪽 홍채맵, 오른쪽 홍채맵)
        final File leftIrisMapImageForFtp = new FileUtil.FileWithDotForExtension(ftpPath,
                leftIrisMapImageFileName, BITMAP_FORMAT.toString());
        final File rightIrisMapImageForFtp = new FileUtil.FileWithDotForExtension(ftpPath,
                rightIrisMapImageFileName, BITMAP_FORMAT.toString());

        mUserHistory.getLeftImage().setIrisBitmapPath(leftIrisImageForFtp.getAbsolutePath());
        mUserHistory.getRightImage().setIrisBitmapPath(rightIrisImageForFtp.getAbsolutePath());

        mUserHistory.getLeftImage().setIrisMapBitmapPath(leftIrisMapImageForFtp.getAbsolutePath());
        mUserHistory.getRightImage().setIrisMapBitmapPath(rightIrisMapImageForFtp.getAbsolutePath());

        Logger.i("saveIrisInfoToFtpPath");
        mRepository.saveIrisInfoToFtpPath(mUserHistory, ftpPath, this);
    }

    /**
     * Login 화면에서 Manager 가 로그인 된 상태이기 때문에
     * 해당 Manager 가 존재 하지 않을 수 없다.
     * 따라서 Exception 로그만 찍어 준다.
     * @author taein
     */
    @Override
    public void onManagerNotAvailable() {
        Logger.e(new Exception(), "Not Available Manager");
    }

    /**
     * @author taein
     */
    @Override
    public void succeedToSaveIrisInfo() {
        Logger.i("succeed to Save Iris Info at Ftp Path");
    }

    /**
     * @author taein
     */
    @Override
    public void failToSaveIrisInfo() {
        Logger.e(new Exception(), "fail to Save Iris Info at Ftp Path");
    }

    /**
     * 로딩바를 disable 한다.
     * 라이브 데이터를 숨겨 한 단계 추상화 시킨다.
     * @author taein
     */
    private void disableLoading() {
        mEnableLoading.setValue(false);
    }

    /**
     * 홍채 정보 저장을 성공 한 콜백 메서드
     * mScanGallery 에 미디어 스캔 경로를 할당 하고 로딩바를 disable 한다.
     * @author taein
     * @param mediaScanPath
     */
    @Override
    public void succeedToSaveIrisImageAndScanTo(File mediaScanPath) {
        mScanGallery.setValue(mediaScanPath);
        disableLoading();
        Logger.i("succeed to save Iris Image To Gallery");
    }

    /**
     * Exception 로그를 찍고
     * 로딩바를 중지 한다.
     * @author taein
     */
    @Override
    public void failToSaveIrisImage() {
        disableLoading();
        Logger.e(new Exception(), "fail to Save Iris Image");
    }

    /**
     * @author taein
     */
    public void setUserHistoryId(int userHistoryId) {
        mUserHistory.setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getBrainOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getBrainOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getKidneyOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getKidneyOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getLungsOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getLungsOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getPancreasOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getLeftImage().getPancreasOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);

        mUserHistory.getRightImage().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getBrainOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getBrainOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getKidneyOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getKidneyOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getLiverOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getLiverOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getLungsOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getLungsOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getPancreasOrganAnalysis().setUserHistoryId(userHistoryId);
        mUserHistory.getRightImage().getPancreasOrganAnalysis().getOrganAnalysis().setUserHistoryId(userHistoryId);
    }

    /**
     * 이메일이나 디스크에 저장하기위해서
     * 결과 스크립트를 String 형태로 정제하는 함수
     * @return
     * @author jslee
     */
    public String makeScriptResult(){
        RefineScript refineScript = new RefineScript(getApplication());
        String refinedScript = refineScript.makeScriptResult(mUserHistory);
        return refinedScript;

    }

    /**
     * 정제된 결과 스트링을 txt 형태로 결과스크립트에 저장
     * @author jslee
     */
    public void saveScriptToDisk(){
        String refinedScript =  makeScriptResult();
        SaveToDisk saveToDisk = new SaveToDisk();
        saveToDisk.saveResultToDisk(mUserHistory, refinedScript);
    }

}
