package com.hongbog.irismanualanalysis.result;

/**
 * 장기코드, 패턴코드의 상수를 관리하는 인터페이스
 * @author jslee
 */
public interface Category {
    String[] left = {"BR", "KD", "LG", "PC"};
    String[] right = {"BR", "KD", "LV", "LG", "PC"};

    String cholesterolRing = "CT";
    String stressRing = "ST";

    String brain = "BR";
    String kidney = "KD";
    String lung = "LG";
    String liver = "LV";
    String pancreas = "PC";

}
