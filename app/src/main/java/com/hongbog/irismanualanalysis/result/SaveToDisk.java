package com.hongbog.irismanualanalysis.result;

import android.os.Environment;
import android.util.Log;

import com.hongbog.irismanualanalysis.data.entity.UserHistory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import static com.hongbog.irismanualanalysis.login.LoginActivity.TAG;

public class SaveToDisk {
    /**
     * Result 스크립트 파일에 떨구기
     * @param userHistory
     */
    public void saveResultToDisk(UserHistory userHistory, String resultString){
        String rightImage =  userHistory.getRightImage().getIrisBitmapPath();
        String leftImage = userHistory.getLeftImage().getIrisBitmapPath();
        String userEmail = userHistory.getUserId();
        resultString = makeResultString(rightImage, leftImage, resultString, userEmail);

        String fileName = "R_"+ userHistory.getAnalysisDate() + "_" + userEmail;
        String folderName = userHistory.getManagerId();
        saveTextFile(resultString, fileName, folderName);

    }

    /**
     *
     * @param rightImage
     * @param leftImage
     * @param resultString
     * @param userEmail
     * @return
     */
    private String makeResultString(String rightImage, String leftImage, String resultString, String userEmail) {
        return userEmail+ "\n" + rightImage + "\n" + leftImage + "\n" + resultString;
    }

    /**
     * contents String 변수의 내용을 스마트폰 내부 저장소에 txt파일로 만들어서 떨군다
     * @param contents
     * @param filename : R(result) + 현재시간 + user  의 이메일주소로 파일이름만든다
     * @param folderName : ManagerId로 폴더 만들어서 그 안에 파일들을 떨군다
     */
    private void saveTextFile(String contents, String filename, String folderName){
        File saveFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+folderName);
        if(!saveFile.exists()){ // 폴더 없을 경우
            saveFile.mkdir(); // 폴더 생성
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(saveFile+"/" + filename +".txt", true));
            buf.append(contents); // 파일 쓰기
            buf.newLine(); // 개행
            buf.close();

        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

    }

    /**
     *
     * @param userHistory
     * @param jsonFileString
     */
    public void saveJSONToDisk(UserHistory userHistory, String jsonFileString){
        String userEmail = userHistory.getUserId();
        String fileName = userHistory.getAnalysisDate() + "_" + userEmail;
        String folderName = userHistory.getManagerId();
        saveTextFile(jsonFileString, fileName, folderName);

    }


}
