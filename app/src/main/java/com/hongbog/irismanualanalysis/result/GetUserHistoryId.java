package com.hongbog.irismanualanalysis.result;

import android.util.Log;

import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.hongbog.irismanualanalysis.util.Encryption;

import javax.crypto.Cipher;

public class GetUserHistoryId {

    private ManualIrisAnalysisRepository mRepository;
    private UserHistory mUserHistory;

    public GetUserHistoryId(ManualIrisAnalysisRepository mRepository, UserHistory mUserHistory) {
        this.mRepository = mRepository;
        this.mUserHistory = mUserHistory;
    }

    /**
     * 앞에서 넘겨받은 user에 담겨있는 ManagerId, UserID 와 현재 analysisData를 생성하여
     * userHistory insert 하고 Auto로 만들어진userHistoryID 받아오기.
     * 그리고 user의 LatelyAnalysisData를 현재 analysisData로 update
     * (가져온 userHistoryID를 기반으로 image, 분석정보등을 저장할것임)
     * @Author jslee
     */
    public int getId(String analysisDate) throws Exception {


        // 1. update UserDate ByID


        String managerId = mUserHistory.getManagerId();
        String mUserId = mUserHistory.getUserId();
        Log.d("jslee", "updateUserDateByID  : " + analysisDate +" / "+ managerId +" / "+ mUserId);
        mRepository.updateUserDateByID(analysisDate, managerId, mUserId);



        // 2. insert UserHistory
        Log.d("jslee", "insertUserHistory  : " +" / "+ mUserHistory.getManagerId() +" / "+ mUserHistory.getUserId()  +" / "+ mUserHistory.getAnalysisDate());
        mRepository.insertUserHistory(mUserHistory);


        String encryptUserId = encryptUserId(mUserId);


        // 3. getUserHistory Id
        Log.d("jslee", "getUserHistoryId : "  + managerId +" / "+ encryptUserId +" / "+ analysisDate);
        int userHistoryId = mRepository.getUserHistoryId(managerId, encryptUserId, analysisDate);
        Log.d("jslee","mUserHistoryId : " + userHistoryId);


        mUserHistory.setUserId(encryptUserId);

        return userHistoryId;
    }

    private String encryptUserId(String userId) throws Exception {
        Encryption encryption = Encryption.getInstance();
        return encryption.encryptOrDecrypt(userId,  Cipher.ENCRYPT_MODE);
    }



}
