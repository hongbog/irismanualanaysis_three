package com.hongbog.irismanualanalysis.result;

import android.content.Intent;
import java.util.Locale;



public class SendEmail {
    private String languageCode = Locale.getDefault().getLanguage();

    /**
     *  Result 스크립트 이메일로 보내기
     * @param resultString
     * @return
     */
    public Intent sendResultScriptByEmail(String resultString, String userEmail){
        String subject = selectTitle();
        String[] address = {userEmail};
        //String[] address = {"jslee_314@naver.com"};

        return sendMail(address , subject , resultString);
    }

    /**
     *  언어별 이메일의 제목 선택
     * @return
     */
    private String selectTitle(){
        String subject = "";

        if(languageCode =="ko"){
            subject = "홍채 분석 결과(HONGBOG)";
        }else if(languageCode =="zh"){
            subject = "健康分析结果(HONGBOG)";
        }else{
            subject = "Analysis result from HONGBOG ";
        }
        return subject;

    }
    /**
     *
     * @param sendMailAddress
     * @param subject
     * @param content
     * @return
     */
    private Intent sendMail(String[] sendMailAddress , String subject , String content){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plaine/text");
        intent.putExtra(Intent.EXTRA_EMAIL, sendMailAddress);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        return intent;

    }

}
