package com.hongbog.irismanualanalysis.camera;

import com.serenegiant.usb.USBMonitor;

public interface CameraUserActionListener extends USBMonitor.OnDeviceConnectListener {
    void capture();
}
