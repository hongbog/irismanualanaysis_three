package com.hongbog.irismanualanalysis.camera;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.capture.CaptureActivity;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.databinding.ActivityCameraBinding;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;
import com.orhanobut.logger.Logger;
import com.serenegiant.common.BaseActivity;
import com.serenegiant.opencv.ImageProcessor;
import com.serenegiant.usb.CameraDialog;
import com.serenegiant.usb.DeviceFilter;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.UVCCamera;
import com.serenegiant.usbcameracommon.UVCCameraHandlerMultiSurface;
import com.serenegiant.widget.UVCCameraTextureView;
import java.nio.ByteBuffer;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import lombok.RequiredArgsConstructor;


public class CameraActivity extends BaseActivity
        implements CameraDialog.CameraDialogParent {

    /**
     * set true if you want to record movie using MediaSurfaceEncoder
     * (writing frame data into Surface camera from MediaCodec
     * by almost same way as USBCameratest2)
     * set false if you want to record movie using MediaVideoEncoder
     */
    private static final boolean USE_SURFACE_ENCODER = false;
    /**
     * preview resolution(width)
     * if your camera does not support specific resolution and mode,
     * {@link UVCCamera#setPreviewSize(int, int, int)} throw exception
     */
    private static final int PREVIEW_WIDTH = 640;
    /**
     * preview resolution(height)
     * if your camera does not support specific resolution and mode,
     * {@link UVCCamera#setPreviewSize(int, int, int)} throw exception
     */
    private static final int PREVIEW_HEIGHT = 480;
    /**
     * preview mode
     * if your camera does not support specific resolution and mode,
     * {@link UVCCamera#setPreviewSize(int, int, int)} throw exception
     * 0:YUYV, other:MJPEG
     */
    private static final int PREVIEW_MODE = 1;
    /**
     * for accessing USB
     */
    private USBMonitor mUSBMonitor;
    /**
     * Handler to execute camera related methods sequentially on private thread
     */
//    private UVCCameraHandler mCameraHandler;
    private UVCCameraHandlerMultiSurface mCameraHandler;
    /**
     * for camera preview display
     */
//    private CameraViewInterface mUVCCameraView;
    private UVCCameraTextureView mUVCCameraView;
    /**
     * for capture button
     */
    private ImageView mCaptureButton;
    /**
     * for image frame process
     */
    protected ImageProcessor mImageProcessor;
    /**
     * for image frame process callback
     */
    private MyImageProcessorCallback imageCallback;
    public static final int REQUEST_CAPTURE = 1;
    private CameraUserActionListener cameraUserActionsListener;
    private static final Object mSync = new Object();
    private static final int IRIS_MANUAL_ANALY_CAMERA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DisplayUtil.isVerticalDisplay(this) <= 720)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        ActivityCameraBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_camera);

        cameraUserActionsListener = getCameraUserActionsListener();

        binding.setListener(cameraUserActionsListener);

        setupView(cameraUserActionsListener);
    }

    @NonNull
    public static CameraViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(CameraViewModel.class);
    }

    private void setupView(CameraUserActionListener listener) {
//        final View view = findViewById(R.id.camera_view);
//        mUVCCameraView = (CameraViewInterface)view;
        mUVCCameraView = findViewById(R.id.camera_view);
        mUVCCameraView.setAspectRatio(PREVIEW_WIDTH / (float) PREVIEW_HEIGHT);

        mCaptureButton = findViewById(R.id.capture_button);

        mUSBMonitor = new USBMonitor(this, listener);
//        mCameraHandler = UVCCameraHandler.createHandler(this, mUVCCameraView,
//                USE_SURFACE_ENCODER ? 0 : 1, PREVIEW_WIDTH, PREVIEW_HEIGHT, PREVIEW_MODE);

        /*mCameraHandler = UVCCameraHandlerMultiSurface.createHandler(this, mUVCCameraView,
                USE_SURFACE_ENCODER ? 0 : 1, PREVIEW_WIDTH, PREVIEW_HEIGHT, UVCCamera.PIXEL_FORMAT_YUV420SP);*/

        mCameraHandler = UVCCameraHandlerMultiSurface.createHandler(this, mUVCCameraView,
                USE_SURFACE_ENCODER ? 0 : 1, PREVIEW_WIDTH, PREVIEW_HEIGHT, PREVIEW_MODE);

    }

    private CameraUserActionListener getCameraUserActionsListener() {
        return new CameraUserActionListener() {
            @Override
            public void onAttach(final UsbDevice device) {
                if (equalCamera(device)) {
//                    startCameraDialog();
                    startCamera(device);
                }
            }

            private boolean equalCamera(final UsbDevice device) {
                final List<DeviceFilter> filter = DeviceFilter.getDeviceFilters(CameraActivity.this,
                        com.hongbog.irismanualanalysis.R.xml.device_filter);

                return device.getVendorId() == filter.get(IRIS_MANUAL_ANALY_CAMERA).mVendorId &&
                        device.getProductId() == filter.get(IRIS_MANUAL_ANALY_CAMERA).mProductId;
            }

            @Override
            public void onConnect(final UsbDevice device, final USBMonitor.UsbControlBlock ctrlBlock, final boolean createNew) {
                mCameraHandler.open(ctrlBlock);
                startPreview();
            }

            @Override
            public void onDisconnect(final UsbDevice device, final USBMonitor.UsbControlBlock ctrlBlock) {
                synchronized (mSync) {
                    if (mCameraHandler != null) {
                        queueEvent(() -> stopPreview(), 0);
                    }
                }
            }

            @Override
            public void onDettach(final UsbDevice device) {
            }

            @Override
            public void onCancel(final UsbDevice device) {
            }

            @Override
            public void capture() {
                if (mCameraHandler.isOpened()) {
                    if (checkPermissionWriteExternalStorage()) {
                        final Image irisImage = getIntent().getParcelableExtra(UserInfoActivity.IRIS_IMAGE);
                        new Thread(new CaptureBitmap(irisImage)).start();
                    }
                }
            }
        };
    }

    @RequiredArgsConstructor
    private class CaptureBitmap implements Runnable {

        @NonNull
        private Image irisImage;

        @Override
        public void run() {
            @Nullable final Bitmap captureBitmap = Bitmap.createBitmap(mUVCCameraView.captureStillImage());
            if (captureBitmap == null) return;

            if (TextUtils.equals(irisImage.getSide(), UserInfoActivity.IRIS_LEFT)) {
                Image.leftIrisBitmap = captureBitmap;
            } else {
                Image.rightIrisBitmap = captureBitmap;
            }

            openCaptureActivity();
        }

        private void openCaptureActivity() {
            Intent intent = new Intent(CameraActivity.this, CaptureActivity.class);
            intent.putExtra(UserInfoActivity.IRIS_IMAGE, irisImage);
            runOnUiThread(() -> startActivityForResult(intent, REQUEST_CAPTURE));
        }
    }

    /**
     * Capture 화면에서 수정이 완료 되었 다면 PatientInfo 화면으로 보낸다.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @author taein
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAPTURE) {
            if (resultCode == CaptureActivity.CAPTURE_RESULT_OK) {
                setResult(CaptureActivity.CAPTURE_RESULT_OK, data);
                finish();
            }
        }
    }

    private void startPreview() {
        mUVCCameraView.resetFps();
        mCameraHandler.startPreview();
        runOnUiThread(() -> {
            try {
                final SurfaceTexture st = mUVCCameraView.getSurfaceTexture();
                if (st != null) {
                    final Surface surface = new Surface(st);
                    mPreviewSurfaceId = surface.hashCode();
                    mCameraHandler.addSurface(mPreviewSurfaceId, surface, false);
                }
                mCaptureButton.setVisibility(View.VISIBLE);
                startImageProcessor(PREVIEW_WIDTH, PREVIEW_HEIGHT);
            } catch (final Exception e) {
                Logger.e(e, "raise Exception");
            }
        });
    }

    private void stopPreview() {
        stopImageProcessor();

        if (mPreviewSurfaceId != 0) {
            mCameraHandler.removeSurface(mPreviewSurfaceId);
            mPreviewSurfaceId = 0;
        }

        mCameraHandler.close();
    }

    private int mPreviewSurfaceId;
    private int mImageProcessorSurfaceId;

    protected void startImageProcessor(final int processing_width, final int processing_height) {
        if (mImageProcessor == null) {
            imageCallback = new MyImageProcessorCallback(processing_width, processing_height);
            mImageProcessor = new ImageProcessor(PREVIEW_WIDTH, PREVIEW_HEIGHT    // src size
                    , imageCallback);    // processing size
            mImageProcessor.start(processing_width, processing_height);    // processing size
            final Surface surface = mImageProcessor.getSurface();
            mImageProcessorSurfaceId = surface != null ? surface.hashCode() : 0;
            if (mImageProcessorSurfaceId != 0) {
                mCameraHandler.addSurface(mImageProcessorSurfaceId, surface, false);
            }
        }
    }

    private void startCameraDialog() {
        if (!mCameraHandler.isOpened()) {
            CameraDialog.showDialog(CameraActivity.this);
        } else {
            mCameraHandler.close();
            Toast.makeText(this, R.string.connect_camera_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private void startCamera(UsbDevice device) {
        if (!mCameraHandler.isOpened()) {
            mUSBMonitor.requestPermission(device);
        } else {
            mCameraHandler.close();
            Toast.makeText(this, R.string.connect_camera_msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        mUSBMonitor.register();
//        if (mUVCCameraView != null)
//            mUVCCameraView.onResume();

        final int usbCount = mUSBMonitor.getDeviceCount();
        if (usbCount == 0)
            Toast.makeText(this, R.string.connect_camera_msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        stopPreview();
//        if (mUVCCameraView != null)
//        mUVCCameraView.onPause();
        mUSBMonitor.unregister();

        super.onStop();
    }

    protected void stopImageProcessor() {
        if (mImageProcessorSurfaceId != 0) {
            mCameraHandler.removeSurface(mImageProcessorSurfaceId);
            mImageProcessorSurfaceId = 0;
        }
        if (mImageProcessor != null) {
            mImageProcessor.release();
            mImageProcessor = null;
        }
    }

    @Override
    public void onDestroy() {
        synchronized (mSync) {
            if (mCameraHandler != null) {
                mCameraHandler.release();
                mCameraHandler = null;
            }
        }

        closeUsbMonitor();

        if (mUVCCameraView != null) {
            mUVCCameraView.onPause();
        }

        mUVCCameraView = null;

        super.onDestroy();
    }

    private void closeUsbMonitor() {
        if (mUSBMonitor != null) {
            mUSBMonitor.destroy();
            mUSBMonitor = null;
        }
    }

    @Override
    public USBMonitor getUSBMonitor() {
        return mUSBMonitor;
    }

    @Override
    public void onDialogResult(boolean canceled) {
    }

    protected class MyImageProcessorCallback implements ImageProcessor.ImageProcessorCallback {
        private final int width, height;
        private final Matrix matrix = new Matrix();
        private Bitmap mFrame;

        public Bitmap getFrame() {
            return mFrame;
        }

        protected MyImageProcessorCallback(
                final int processing_width, final int processing_height) {
            width = processing_width;
            height = processing_height;
        }

        @Override
        public void onFrame(final ByteBuffer frame) {
            if (frame == null) return;
            if (mFrame == null) {
                mFrame = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                final float scaleX = width;
                final float scaleY = height;
                matrix.reset();
                matrix.postScale(scaleX, scaleY);
            }

            frame.clear();
            mFrame.copyPixelsFromBuffer(frame);
        }

        @Override
        public void onResult(final int type, final float[] result) {
        }
    }

    private class ExceptionHandler implements Thread.UncaughtExceptionHandler {

        private static final String USB_MONITOR = "USBMonitor";
        private final Context mContext;

        public ExceptionHandler(Context context) {
            mContext = context;
        }

        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
            /**
             * CameraUserActionsListener::onAttach 가 호출되고 USB 가 제거 될 경우
             * 해당 예외를 처리 한다.
             * @author taein
             */
            if (TextUtils.equals(USB_MONITOR, thread.getName())) {
                if (throwable instanceof NullPointerException) {
                    if (mContext instanceof CameraActivity) {
                        runOnUiThread(() -> {
                            Toast.makeText(mContext, R.string.removed_usb, Toast.LENGTH_LONG).show();
                            closeUsbMonitor();
                            mUSBMonitor = new USBMonitor(CameraActivity.this, cameraUserActionsListener);
                            mUSBMonitor.register();
                        });
                    }
                }
            }
        }
    }
}