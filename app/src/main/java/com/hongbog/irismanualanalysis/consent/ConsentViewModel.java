package com.hongbog.irismanualanalysis.consent;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * IrisManualAnalysis
 * Method: ConsentViewModel
 * Created by oprime on 2019-04-26
 * Description:
 **/
public class ConsentViewModel extends AndroidViewModel {

    private static final String TAG = ConsentViewModel.class.getSimpleName();

    public ConsentViewModel(Application app) {
        super(app);
    }

    // TODO: Version 2.0일 시, 쓰이는 것들.
/*    private final DataRepository mRepository;

    public UserInfoViewModel(DataRepository repository) {
        mRepository = repository;
    }*/

    private MutableLiveData<Bitmap> mAllInfoBitmap = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mPrivateInfoBitmap = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mIdentificationInfoBitmap = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mHealthyInfoBitmap = new MutableLiveData<>();
    private MutableLiveData<Boolean> mChangedConsentText = new MutableLiveData<>();
    private Bitmap checkedImageView;
    private Bitmap unCheckedImageView;
    private boolean mAllInfoChecked = false;
    private boolean mPrivateInfoChecked = false;
    private boolean mIdentificationInfoChecked = false;
    private boolean mHealthyInfoChecked = false;

    public LiveData<Bitmap> getAllInfoBitmap() {
        return mAllInfoBitmap;
    }

    public LiveData<Bitmap> getPriavteInfoBitmap(){
        return mPrivateInfoBitmap;
    }

    public LiveData<Bitmap> getIdentificationInfoBitmap() { return mIdentificationInfoBitmap; }

    public LiveData<Bitmap> getHealthyInfoBitmap() { return mHealthyInfoBitmap; }

    public LiveData<Boolean> getChangedConsentText() { return mChangedConsentText; }

    /**
     * #Method: setAllInfoBitmap
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:44
     * #Description: 모두 동의 버튼을 누를 시, 다른 동의 버튼이 함께 활성화된다.
     **/
    public void setAllInfoBitmap() {
        mAllInfoChecked = !mAllInfoChecked;

        if (mAllInfoChecked) {
            mAllInfoBitmap.setValue(checkedImageView);
            mPrivateInfoChecked = true;
            mIdentificationInfoChecked = true;
            mHealthyInfoChecked = true;
        } else {
            mAllInfoBitmap.setValue(unCheckedImageView);
            mPrivateInfoChecked = false;
            mIdentificationInfoChecked = false;
            mHealthyInfoChecked = false;
        }

        setPrivateInfoBitmap(mPrivateInfoChecked);
        setIdentificationInfoBitmap(mIdentificationInfoChecked);
        setHealthyInfoBitmap(mHealthyInfoChecked);
        checkKeepGoingText();
    }

    /**
     * #Method: setPrivateInfoBitmap
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:45
     * #Description: 개인정보 동의 버튼에 관한 메소드.
     **/
    public void setPrivateInfoBitmap(boolean checked) {
        mPrivateInfoChecked = checked;
        if (checked) {
            mPrivateInfoBitmap.setValue(checkedImageView);
        } else {
            mPrivateInfoBitmap.setValue(unCheckedImageView);
        }
        checkKeepGoingText();
    }

    /**
     * #Method: setIdentificationInfoBitmap
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:45
     * #Description: 고유 식별정보 동의 버튼에 관한 메소드.
     **/
    public void setIdentificationInfoBitmap(boolean checked) {
        mIdentificationInfoChecked = checked;
        if (checked) {
            mIdentificationInfoBitmap.setValue(checkedImageView);
        } else {
            mIdentificationInfoBitmap.setValue(unCheckedImageView);
        }
        checkKeepGoingText();
    }

    /**
     * #Method: setHealthyInfoBitmap
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:46
     * #Description: 건강정보 동의 버튼에 관한 메소드.
     **/
    public void setHealthyInfoBitmap(boolean checked) {
        mHealthyInfoChecked = checked;
        if (checked) {
            mHealthyInfoBitmap.setValue(checkedImageView);
        } else {
            mHealthyInfoBitmap.setValue(unCheckedImageView);
        }
        checkKeepGoingText();
    }

    /**
     * #Method: checkKeepGoingText
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:46
     * #Description: 필수 동의 버튼이 활성화되어야만 다음 진행 버튼과 텍스트 색상이 바뀌는 메소드.
     **/
    public void checkKeepGoingText() {
        if (mPrivateInfoChecked && mIdentificationInfoChecked && mHealthyInfoChecked) {
            mChangedConsentText.setValue(true);
        } else {
            mChangedConsentText.setValue(false);
        }
    }

    /**
     * #Method: start
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:47
     * #Description: 처음 프로그램 실행 시 설정되는 메소드.
     **/
    public void start(Drawable drawable1, Drawable drawable2, Boolean enabled) {
        checkedImageView = ((BitmapDrawable)drawable1).getBitmap();
        unCheckedImageView = ((BitmapDrawable)drawable2).getBitmap();
        mChangedConsentText.setValue(enabled);
    }
}
