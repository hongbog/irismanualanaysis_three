package com.hongbog.irismanualanalysis.consent;

import android.view.View;

/**
 * IrisManualAnalysis
 * Method: ConsentActionsListener
 * Created by oprime on 2019-04-26
 * Description:
 **/
public interface ConsentActionsListener {
    void onCheckedReadInfo(View view);
    void onExpandedButtonClicked(View view);
    void onKeepGoingButtonClicked();
}
