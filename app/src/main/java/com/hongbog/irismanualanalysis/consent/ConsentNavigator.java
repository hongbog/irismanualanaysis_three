package com.hongbog.irismanualanalysis.consent;

/**
 * IrisManualAnalysis
 * Method: ConsentNavigator
 * Created by oprime on 2019-04-26
 * Description:
 **/
public interface ConsentNavigator {
    void openAnalysisPage();
}
