package com.hongbog.irismanualanalysis.consent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.analysis.AnalysisActivity;
import com.hongbog.irismanualanalysis.databinding.ActivityConsentBinding;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;

public class ConsentActivity extends AppCompatActivity implements ConsentNavigator {

    public static final String TAG = ConsentActivity.class.getSimpleName();
    private ConsentViewModel mViewModel;
    private ActivityConsentBinding binding;

    private boolean isPrivateChecked = false;
    private boolean isIdentificationChecked = false;
    private boolean isHealthyChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_consent);
        mViewModel = obtainViewModel(this);
        final ConsentActionsListener listener = getConsentActionsListener();
        binding.setListener(listener);
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(this);

        mViewModel.start(getResources().getDrawable(
                R.drawable.check_btn),
                getResources().getDrawable(R.drawable.uncheck_btn),
                false);

        /* Created by 조원태 2019-04-29 오후 7:51
         * #Description: LiveData에서 필수 동의 버튼이 활성화 시, 다음 진행 버튼의 클릭과 색상을
         *               변화해주는 ViewModel의 옵저버.
         */
        mViewModel.getChangedConsentText().observe(this, Checked -> {
            if (Checked) {
                binding.keepGoingBtn.setTextColor(getResources().getColor(R.color.colorTitle));
            } else {
                binding.keepGoingBtn.setTextColor(getResources().getColor(R.color.colorDisable));
            }
        });
    }

    @NonNull
    public static ConsentViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(ConsentViewModel.class);
    }

    /**
     * #Method: getConsentActionsListener
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:48
     * #Description: onCheckREadInfo -> 체크 버튼 이미지에 관한 리스너.
     *               onExpandedButtonClicked -> 약관 동의서를 펼쳐서 볼 수 있게 하는 리스너.
     *               onKeepGoingButtonClicked -> 다음 진행 버튼을 클릭 시 실행되는 리스너.
     **/
    private ConsentActionsListener getConsentActionsListener() {
        return new ConsentActionsListener() {
            @Override
            public void onCheckedReadInfo(View view) {
                switch (view.getId()) {
                    case R.id.private_info_check:
                    case R.id.private_info_txt:
                        isPrivateChecked = !isPrivateChecked;
                        mViewModel.setPrivateInfoBitmap(isPrivateChecked);
                        break;
                    case R.id.identification_info_check:
                    case R.id.identification_info_txt:
                        isIdentificationChecked = !isIdentificationChecked;
                        mViewModel.setIdentificationInfoBitmap(isIdentificationChecked);
                        break;
                    case R.id.healthy_info_check:
                    case R.id.healthy_info_txt:
                        isHealthyChecked = !isHealthyChecked;
                        mViewModel.setHealthyInfoBitmap(isHealthyChecked);
                        break;
                    case R.id.all_info_check:
                    case R.id.all_info_txt:
                    case R.id.all_info_sub_txt:
                        mViewModel.setAllInfoBitmap();
                        break;
                }
            }

            @Override
            public void onExpandedButtonClicked(View view) {
                switch (view.getId()) {
                    case R.id.private_info_expand_btn:
                    case R.id.private_info_expand_btn_layout:
                        binding.identificationInfoExpandLayout.collapse();
                        binding.healthyInfoExpandLayout.collapse();
                        binding.privateInfoExpandLayout.toggle();
                        break;
                    case R.id.identification_info_expand_btn:
                    case R.id.identification_info_expand_btn_layout:
                        binding.privateInfoExpandLayout.collapse();
                        binding.healthyInfoExpandLayout.collapse();
                        binding.identificationInfoExpandLayout.toggle();
                        break;
                    case R.id.healthy_info_expand_btn:
                    case R.id.healthy_info_expand_btn_layout:
                        binding.privateInfoExpandLayout.collapse();
                        binding.identificationInfoExpandLayout.collapse();
                        binding.healthyInfoExpandLayout.toggle();
                        break;
                }
            }

            @Override
            public void onKeepGoingButtonClicked() {
                openAnalysisPage();
            }
        };
    }

    /**
     * #Method: openAnalysisPage
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-29 오후 7:49
     * #Description: 다음 진행 버튼을 누를 시, 분석 화면으로 이동해주는 ConsentNavigator 인터페이스
     *               에 대한 Override 메소드.
     **/
    @Override
    public void openAnalysisPage() {
        Intent intent = getIntent().setClass(this, UserInfoActivity.class);
        startActivity(intent);
        finish();
    }
}
