package com.hongbog.irismanualanalysis.loading;

import android.app.Application;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.util.Log;

import com.hongbog.irismanualanalysis.data.dataSource.ManualIrisAnalysisDataSource;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.orhanobut.logger.Logger;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class LoadingViewModel extends AndroidViewModel implements ManualIrisAnalysisDataSource.IrisInfoTransferToRemoteCallback{
    private ManualIrisAnalysisRepository mRepository;
    private File mFtpFolder;
    private MutableLiveData<Boolean> mEnableLoading = new MutableLiveData<>();

    public LoadingViewModel(@NonNull Application app) {
        super(app);
        this.mRepository = ManualIrisAnalysisRepository.getInstance(app);
    }

    public void start(File ftpPath) {
        mFtpFolder = ftpPath;
        mEnableLoading.setValue(true);
    }

    public void transferIrisInfoToRemote(ConnectivityManager connectivityManager, FTPInfoVO ftpInfoVO) {
        boolean wifiEnable = false;
        Network[] networks = connectivityManager.getAllNetworks();
        for (int i = 0; i < networks.length; i++) {
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(networks[i]);
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                wifiEnable = true;
            }
        }

        if (wifiEnable) {
            mRepository.transferIrisInfoToRemote(mFtpFolder.getPath(), ftpInfoVO, this);
        } else {
            mEnableLoading.setValue(false);
        }
    }

    @Override
    public void succeedToTransferIrisInfo() {
        Logger.d("kyh: succeedToTransferIrisInfo", "");
        mEnableLoading.setValue(false);
    }

    @Override
    public void failToTransferIrisInfo(String errorMessage) {
        Logger.d("kyh: failToTransferIrisInfo - " + errorMessage, "");
        mEnableLoading.setValue(false);
    }

    public MutableLiveData<Boolean> getEnableLoading() {
        return mEnableLoading;
    }
}
