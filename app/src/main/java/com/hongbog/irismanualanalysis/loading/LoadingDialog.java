package com.hongbog.irismanualanalysis.loading;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ProgressBar;

import com.hongbog.irismanualanalysis.R;

public class LoadingDialog extends Dialog {
    public LoadingDialog(Activity activity) {
        super(activity);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_loading);

        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ProgressBar progressBar = findViewById(R.id.analysis_progress);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.rgb(255, 255, 141), PorterDuff.Mode.MULTIPLY);
    }
}
