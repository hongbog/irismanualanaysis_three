package com.hongbog.irismanualanalysis.loading;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.hongbog.irismanualanalysis.databinding.ActivityLoadingBinding;
import com.hongbog.irismanualanalysis.login.LoginActivity;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

public class LoadingActivity extends AppCompatActivity {
    private LoadingViewModel mViewModel;
    private ActivityLoadingBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        this.binding = DataBindingUtil.setContentView(LoadingActivity.this, R.layout.activity_loading);
        mViewModel = obtainViewModel(this);
        this.binding.setLifecycleOwner(this);
        this.binding.setViewModel(mViewModel);
        mViewModel.start(getFilesDir());

        setUpObserver();

//        transferIrisInfoToRemote();
    }

    private LoadingViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(LoadingViewModel.class);
    }

    private void setUpObserver() {
        /*mViewModel.getEnableLoading().observe(this, mEnableLoading -> {
            if (!mViewModel.getEnableLoading().getValue()) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void transferIrisInfoToRemote() {
        Resources res = getResources();

        FTPInfoVO ftpInfoVO = new FTPInfoVO();
        ftpInfoVO.setHost(res.getString(R.string.host));
        ftpInfoVO.setPort(res.getInteger(R.integer.port));
        ftpInfoVO.setUsername(res.getString(R.string.username));
        ftpInfoVO.setPassword(res.getString(R.string.password));
        ftpInfoVO.setRemoteRootDir(res.getString(R.string.remote_root_dir));
        mViewModel.transferIrisInfoToRemote((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE), ftpInfoVO);
    }
}