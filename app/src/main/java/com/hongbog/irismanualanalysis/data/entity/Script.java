package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Nonnegative;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;

/**
 * 분석 코드, 항목, 증상, 예방법, 좋은 음식, 나쁜 음식
 * @Author jslee
 * */
@AllArgsConstructor
@Entity(tableName = "script",
        primaryKeys = {"script_code", "language_code"})
public class Script implements Parcelable {
    @NonNull
    @ColumnInfo(name = "script_code")
    private String scriptCode;

    @NonNull
    @ColumnInfo(name = "language_code")
    private String languageCode;

    @NonNull
    @ColumnInfo(name = "category")
    private String category;

    @NonNull
    @ColumnInfo(name = "symptom")
    private String symptom;

    @NonNull
    @ColumnInfo(name = "precaution")
    private String precaution;

    @NonNull
    @ColumnInfo(name = "healthy_food")
    private String healthyFood;

    @NonNull
    @ColumnInfo(name = "unhealthy_food")
    private String unhealthyFood;

    public Script() {
    }

    protected Script(Parcel in) {
        scriptCode = in.readString();
        languageCode = in.readString();
        category = in.readString();
        symptom = in.readString();
        precaution = in.readString();
        healthyFood = in.readString();
        unhealthyFood = in.readString();
    }

    public static final Creator<Script> CREATOR = new Creator<Script>() {
            @Override
            public Script createFromParcel(Parcel in) {
                    return new Script(in);
            }

            @Override
            public Script[] newArray(int size) {
                    return new Script[size];
            }
    };


    @Override
    public int describeContents() {
            return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(scriptCode);
        parcel.writeString(languageCode);
        parcel.writeString(category);
        parcel.writeString(symptom);
        parcel.writeString(precaution);
        parcel.writeString(healthyFood);
        parcel.writeString(unhealthyFood);
    }

    @NonNull
    public String getScriptCode() {
            return scriptCode;
    }

    public void setScriptCode(@NonNull String scriptCode) {
            this.scriptCode = scriptCode;
    }

    @NonNull
    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(@NonNull String languageCode) {
        this.languageCode = languageCode;
    }

    @NonNull
    public String getCategory() {
            return category;
    }

    public void setCategory(@NonNull String category) {
            this.category = category;
    }

    @NonNull
    public String getSymptom() {
            return symptom;
    }

    public void setSymptom(@NonNull String symptom) {
            this.symptom = symptom;
    }

    @NonNull
    public String getPrecaution() {
            return precaution;
    }

    public void setPrecaution(@NonNull String precaution) {
            this.precaution = precaution;
    }

    @NonNull
    public String getHealthyFood() {
            return healthyFood;
    }

    public void setHealthyFood(@NonNull String healthyFood) {
            this.healthyFood = healthyFood;
    }

    @NonNull
    public String getUnhealthyFood() {
            return unhealthyFood;
    }

    public void setUnhealthyFood(@NonNull String unhealthyFood) {
            this.unhealthyFood = unhealthyFood;
    }
}

