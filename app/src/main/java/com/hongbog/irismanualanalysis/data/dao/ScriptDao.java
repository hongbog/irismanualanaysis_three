package com.hongbog.irismanualanalysis.data.dao;

import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.entity.Script;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface ScriptDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAllScript(List<Script> scriptList);

    @Query("select * from script where script_code = :scriptCode")
    Script selectOne(String scriptCode);

    @Query("DELETE FROM script")
    void deleteScript();

    @Query("SELECT * from script WHERE script_code=:scriptCode and language_code =:languageCode ")
    Script getScriptByID(String scriptCode, String languageCode);

    @Query("select a.user_history_id, a.side, b.* " +
            "from (select min(a.user_history_id) user_history_id, a.side, min(b.stress_ring_code) stress_ring_code, min(b.cholesterol_ring_code) cholesterol_ring_code " +
            "from image a " +
            "inner join iris_analysis b " +
            "on (a.user_history_id = b.user_history_id " +
            "and a.side = b.side) where a.user_history_id = :userHistoryId " +
            "group by a.side) a " +
            "inner join script b " +
            "on (a.stress_ring_code = b.script_code " +
            "or a.cholesterol_ring_code = b.script_code) " +
            "where b.language_code = :languageCode")
    List<AnalysisScriptResult> getAnalysisScript(int userHistoryId, String languageCode);


    @Query("select a.user_history_id as user_history_id, a.side as side, a.organ_code as organ_code, b.category as category, b.symptom as symptom, b.precaution as precaution, b.healthy_food as healthy_food, b.unhealthy_food as unhealthy_food from (select a.user_history_id, a.side, a.organ_code||b.pattern_name organ_code from (select a.user_history_id, a.side, b.organ_code, (case c.normal when :yes then 0 when :no then case when c.lacuna = :yes then 1 when c.pigment_spot = :yes then 2 when c.spoke = :yes then 3 when c.defect = :yes then 4 end end) as pattern_code from image a inner join iris_analysis b on (a.user_history_id = b.user_history_id and a.side = b.side) inner join organ_analysis c on (b.user_history_id = c.user_history_id and b.side = c.side and b.organ_code = c.organ_code) where a.user_history_id = :userHistoryId) a inner join pattern_code b on (a.pattern_code = b.pattern_code)) a inner join script b on (a.organ_code = b.script_code) where b.language_code = :languageCode")
    List<OrganScriptResult> getOrganScript(int userHistoryId, String languageCode, String yes, String no);



}
