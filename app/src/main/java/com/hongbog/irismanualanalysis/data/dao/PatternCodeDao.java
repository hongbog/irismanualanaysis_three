package com.hongbog.irismanualanalysis.data.dao;


import com.hongbog.irismanualanalysis.data.entity.PatternCode;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface PatternCodeDao {
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertPatternAllCode(List<PatternCode> patternCodeList);

    @Update
    void updatePatternCode(PatternCode patternCode);

    @Query("DELETE FROM pattern_code")
    void deletePatternCode();

}
