package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;

/**
 * 장기코드, 장기이름
 * @Author jslee
 * */
@AllArgsConstructor
@Entity(tableName = "organ_code")
public class OrganCode implements Parcelable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "organ_code")
    private String organCode;

    @NonNull
    @ColumnInfo(name = "organ_name")
    private String organName;

    protected OrganCode(Parcel in) {
        organCode = in.readString();
        organName = in.readString();
    }

    public OrganCode() {
    }

    public static final Creator<OrganCode> CREATOR = new Creator<OrganCode>() {
        @Override
        public OrganCode createFromParcel(Parcel in) {
            return new OrganCode(in);
        }

        @Override
        public OrganCode[] newArray(int size) {
            return new OrganCode[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(organCode);
        parcel.writeString(organName);
    }

    @NonNull
    public String getOrganCode() {
        return organCode;
    }

    public void setOrganCode(@NonNull String organCode) {
        this.organCode = organCode;
    }

    @NonNull
    public String getOrganName() {
        return organName;
    }

    public void setOrganName(@NonNull String organName) {
        this.organName = organName;
    }
}
