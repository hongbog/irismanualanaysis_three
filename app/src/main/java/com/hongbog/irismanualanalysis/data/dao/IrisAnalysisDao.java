package com.hongbog.irismanualanalysis.data.dao;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface IrisAnalysisDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAllIrisAnalysis(List<IrisAnalysis> irisAnalyses);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertIrisAnalysis(IrisAnalysis select);

    @Query("DELETE FROM iris_analysis")
    void deleteAllIrisAnalysis();

    @Update
    void updateIrisAnalysis(IrisAnalysis irisAnalysis);

    @Query("SELECT * from iris_analysis ORDER BY user_history_id ASC")
    LiveData<List<IrisAnalysis>> getIrisAnalysisAlphabetized();

    @Query("DELETE FROM iris_analysis")
    void deleteIrisAnalysis();

}
