package com.hongbog.irismanualanalysis.data.repository;

import android.app.Application;

import com.hongbog.irismanualanalysis.data.AppDatabase;
import com.hongbog.irismanualanalysis.data.AppExecutors;
import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.dao.ImageDao;
import com.hongbog.irismanualanalysis.data.dao.IrisAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.ManagerDao;
import com.hongbog.irismanualanalysis.data.dao.OrganAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.OrganCodeDao;
import com.hongbog.irismanualanalysis.data.dao.PatternCodeDao;
import com.hongbog.irismanualanalysis.data.dao.ScriptDao;
import com.hongbog.irismanualanalysis.data.dao.UserDao;
import com.hongbog.irismanualanalysis.data.dao.UserHistoryDao;
import com.hongbog.irismanualanalysis.data.dataSource.ManualIrisAnalysisDataSource;
import com.hongbog.irismanualanalysis.data.dataSource.ManualIrisAnalysisLocalDataSourceImpl;
import com.hongbog.irismanualanalysis.data.dataSource.ManualIrisAnalysisRemoteDatasourceImpl;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.OrganCode;
import com.hongbog.irismanualanalysis.data.entity.PatternCode;
import com.hongbog.irismanualanalysis.data.entity.Script;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.hongbog.irismanualanalysis.data.vo.OrganPointVO;

import java.io.File;
import java.util.List;

public class ManualIrisAnalysisRepository implements ManualIrisAnalysisDataSource {

    private ManualIrisAnalysisDataSource mLocalDataSource;
    private ManualIrisAnalysisDataSource mRemoteDataSource;
    private static volatile ManualIrisAnalysisRepository INSTANCE;

    public ManualIrisAnalysisRepository(Application app) {
        AppDatabase db = AppDatabase.getInstance(app);

        ManagerDao managerDao = db.mManagerDao();
        UserDao userDao = db.mUserDao();
        UserHistoryDao userHistoryDao = db.mUserHistoryDao();
        ImageDao imageDao = db.mImageDao();
        IrisAnalysisDao irisAnalysisDao = db.mIrisAnalysisDao();
        OrganAnalysisDao organAnalysisDao = db.mOrganAnalysisDao();
        ScriptDao scriptDao = db.mScriptDao();
        OrganCodeDao organCodeDao = db.mOrganCodeDao();
        PatternCodeDao patternCodeDao = db.mPatternCodeDao();

        mLocalDataSource = ManualIrisAnalysisLocalDataSourceImpl.getInstance(userDao, managerDao, userHistoryDao, imageDao, irisAnalysisDao, organAnalysisDao, scriptDao, organCodeDao, patternCodeDao, new AppExecutors());
        mRemoteDataSource = ManualIrisAnalysisRemoteDatasourceImpl.getInstance(userDao, managerDao, userHistoryDao, imageDao, irisAnalysisDao, organAnalysisDao, scriptDao, organCodeDao, patternCodeDao, new AppExecutors());
    }

    public static ManualIrisAnalysisRepository getInstance(Application app) {
        if (INSTANCE == null) {
            synchronized (ManualIrisAnalysisRepository.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ManualIrisAnalysisRepository(app);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void transferIrisInfoToRemote(String ftpPath, FTPInfoVO ftpInfoVO, IrisInfoTransferToRemoteCallback callback) {
        mRemoteDataSource.transferIrisInfoToRemote(ftpPath, ftpInfoVO, callback);
    }

    /**
     * 왼쪽/오른쪽 홍채 및 홍채맵에 필요한 정보를 초기화하는 함수
     * @param side
     */
    public List<OrganPointVO> initAnalysisInformation(Image currImageDto, String side) {
        return mLocalDataSource.initAnalysisInformation(currImageDto, side);
    }

    /**
     * touch 된 좌표 값에 해당하는 장기를 찾는 함수
     * @param x
     * @param y
     * @param currImageDto
     * @return
     */
    public String organCheckOfTouch(int x, int y, Image currImageDto, List<OrganPointVO> organPointList) {
        String organCode = mLocalDataSource.organCheckOfTouch(x, y, currImageDto, organPointList);
        return organCode;
    }

    public void deleteAll() {
        mLocalDataSource.deleteAll();
    }

    public boolean assertLogin(String manager_id, String password) {
        return mLocalDataSource.assertLogin(manager_id, password);
    }

    public boolean assertIdentity(String managerId) {
        return mLocalDataSource.assertIdentity(managerId);
    }

    public void insert(Manager manager) {
        mLocalDataSource.insert(manager);
    }

    public void insertUserHistory(UserHistory userHistory) {
        mLocalDataSource.insertUserHistory(userHistory);
    }

    public void updateUserDateByID(String analysisDate, String managerId, String userId) {
        mLocalDataSource.updateUserDateByID(analysisDate, managerId, userId);

    }

    public int getUserHistoryId(String managerId, String userId, String analysisDate) {
        return mLocalDataSource.getUserHistoryId(managerId, userId, analysisDate);
    }

    /** 분석 정보 저장 로직 **/

    public void insertAllImage(List<Image> images) {
        mLocalDataSource.insertAllImage(images);
    }

    public void insertIrisAnalysis(IrisAnalysis irisAnalysis) {
        mLocalDataSource.insertIrisAnalysis(irisAnalysis);
    }

    public void insertAllIrisAnalysis(List<IrisAnalysis> irisAnalysis) {
        mLocalDataSource.insertAllIrisAnalysis(irisAnalysis);
    }

    public void insertOrganAnalysis(OrganAnalysis organAnalysis) {
        mLocalDataSource.insertOrganAnalysis(organAnalysis);
    }

    public void insertAllOrganAnalysis(List<OrganAnalysis> organAnalyses) {
        mLocalDataSource.insertAllOrganAnalysis(organAnalyses);
    }

    /** 스크립트 코드 가져오기 **/

    public Script getScriptByID(String scriptCode, String languageCode) {
        return mLocalDataSource.getScriptByID(scriptCode, languageCode);
    }

    public List<AnalysisScriptResult> getAnalysisScript(int userHistoryId, String languageCode) {
        return mLocalDataSource.getAnalysisScript(userHistoryId, languageCode);
    }

    public List<OrganScriptResult> getOrganScript(int userHistoryId, String languageCode) {
        return mLocalDataSource.getOrganScript(userHistoryId, languageCode);
    }

        /**
     * @author taein
     * @param galleryPath
     * @param callback
     */
    public void saveIrisImageTo(File galleryPath, ManualIrisAnalysisDataSource.IrisImageSaveToGalleryCallback callback) {
        mLocalDataSource.saveIrisImageTo(galleryPath, callback);
    }

    /**
     * @author taein
     * @param userHistory
     * @param ftpPath
     * @param callback
     */
    public void saveIrisInfoToFtpPath(UserHistory userHistory, File ftpPath, ManualIrisAnalysisDataSource.IrisInfoSaveToFtpCallback callback) {
        mLocalDataSource.saveIrisInfoToFtpPath(userHistory, ftpPath, callback);
    }

    /**
     * @author taein
     * @param managerId
     * @param callback
     */
    public void getManagerBy(String managerId, ManualIrisAnalysisDataSource.GetManagerCallback callback) {
        mLocalDataSource.getManagerBy(managerId, callback);
    }

    @Override
    public void validateUser(User user,
                             ManualIrisAnalysisDataSource.ValidateUserCallback callback) throws Exception {
        mLocalDataSource.validateUser(user, callback);
    }

}
