package com.hongbog.irismanualanalysis.data.vo;

public class FTPInfoVO {
    private String host;
    private int port;
    private String username;
    private String password;
    private String remoteRootDir;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemoteRootDir() {
        return remoteRootDir;
    }

    public void setRemoteRootDir(String remoteRootDir) {
        this.remoteRootDir = remoteRootDir;
    }
}
