package com.hongbog.irismanualanalysis.data.dao;

import com.hongbog.irismanualanalysis.data.entity.UserHistory;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface UserHistoryDao {
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertUserHistory(UserHistory userHistory);

    @Update
    void updateUserHistory(UserHistory userHistory);

    @Query("SELECT user_history_id from user_history WHERE analysis_date = :analysisDate and user_id = :userId and manager_id = :managerId")
    int getUserHistoryId(String managerId, String userId, String analysisDate);


    @Query("SELECT * from user_history ORDER BY analysis_date ASC")
    LiveData<List<UserHistory>> getUserHistoryAlphabetized();

    @Query("DELETE FROM user_history")
    void deleteUserHistory();
}
