package com.hongbog.irismanualanalysis.data.entity;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;

/**
 * 관리자 아이디, 관리자 비밀번호, 관리자 이름, 관리자기관, 가입일시
 * @Author jslee
 * */
@AllArgsConstructor
@Entity(tableName = "manager")
public class Manager implements Parcelable {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "manager_id")
    private String managerId;
    @ColumnInfo(name = "manager_password")
    private String managerPassword;
    @ColumnInfo(name = "manager_name")
    private String managerName;
    @ColumnInfo(name = "manager_agency")
    private String managerAgency;
    @ColumnInfo(name = "join_date")
    private String joinDate;
    @Ignore
    private List<User> users;

    public Manager() {}

    protected Manager(Parcel in) {
        managerId = in.readString();
        managerPassword = in.readString();
        managerName = in.readString();
        managerAgency = in.readString();
        joinDate = in.readString();
    }

    public static final Creator<Manager> CREATOR = new Creator<Manager>() {
        @Override
        public Manager createFromParcel(Parcel in) {
            return new Manager(in);
        }

        @Override
        public Manager[] newArray(int size) {
            return new Manager[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(managerId);
        parcel.writeString(managerName);
        parcel.writeString(managerPassword);
        parcel.writeString(managerAgency);
        parcel.writeString(joinDate);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @NonNull
    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(@NonNull String managerId) {
        this.managerId = managerId;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public void setManagerPassword(@NonNull String managerPassword) {
        this.managerPassword = managerPassword;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(@NonNull String managerName) {
        this.managerName = managerName;
    }

    public String getManagerAgency() {
        return managerAgency;
    }

    public void setManagerAgency(@NonNull String managerAgency) {
        this.managerAgency = managerAgency;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(@NonNull String joinDate) {
        this.joinDate = joinDate;
    }
}
