package com.hongbog.irismanualanalysis.data.dao;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface OrganAnalysisDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAllOrganAnalysis(List<OrganAnalysis> organAnalyses);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertOrganAnalysis(OrganAnalysis organAnalysis);

    @Query("DELETE FROM organ_analysis")
    void deleteOrganAnalysis();


}
