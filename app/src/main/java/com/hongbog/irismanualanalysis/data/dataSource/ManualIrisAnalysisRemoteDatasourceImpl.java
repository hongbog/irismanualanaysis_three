package com.hongbog.irismanualanalysis.data.dataSource;

import android.graphics.Bitmap;

import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.AppExecutors;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.dao.ImageDao;
import com.hongbog.irismanualanalysis.data.dao.IrisAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.ManagerDao;
import com.hongbog.irismanualanalysis.data.dao.OrganAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.OrganCodeDao;
import com.hongbog.irismanualanalysis.data.dao.PatternCodeDao;
import com.hongbog.irismanualanalysis.data.dao.ScriptDao;
import com.hongbog.irismanualanalysis.data.dao.UserDao;
import com.hongbog.irismanualanalysis.data.dao.UserHistoryDao;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.OrganCode;
import com.hongbog.irismanualanalysis.data.entity.PatternCode;
import com.hongbog.irismanualanalysis.data.entity.Script;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.hongbog.irismanualanalysis.data.vo.OrganPointVO;
import com.hongbog.irismanualanalysis.util.AndroidFTPClient;
import com.orhanobut.logger.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;

public class ManualIrisAnalysisRemoteDatasourceImpl implements ManualIrisAnalysisDataSource{
    private static volatile ManualIrisAnalysisRemoteDatasourceImpl INSTANCE;
    private ManagerDao mManagerDao;
    private UserDao mUserDao;
    private AppExecutors mAppExecutors;
    private UserHistoryDao mUserHistoryDao;
    private ImageDao mImageDao;
    private IrisAnalysisDao mIrisAnalysisDao;
    private OrganAnalysisDao mOrganAnalysisDao;
    private ScriptDao mScriptDao;
    private OrganCodeDao mOrganCodeDao;
    private PatternCodeDao mPatternCodeDao;
    private final Bitmap.CompressFormat BITMAP_FORMAT = Bitmap.CompressFormat.PNG;
    private final String JSON_EXT = "json";

    private ManualIrisAnalysisRemoteDatasourceImpl(@NonNull UserDao userDao, @NonNull ManagerDao managerDao,
                                                  @NonNull UserHistoryDao userHistoryDao, @NonNull ImageDao imageDao,
                                                  @NonNull IrisAnalysisDao irisAnalysisDao, @NonNull OrganAnalysisDao organAnalysisDao,
                                                  @NonNull ScriptDao scriptDao, @NonNull OrganCodeDao organCodeDao, @NonNull PatternCodeDao patternCodeDao,
                                                  AppExecutors appExecutors) {
        mUserDao = userDao;
        mManagerDao = managerDao;
        mUserHistoryDao = userHistoryDao;
        mImageDao = imageDao;
        mIrisAnalysisDao = irisAnalysisDao;
        mOrganAnalysisDao = organAnalysisDao;
        mScriptDao = scriptDao;
        mOrganCodeDao = organCodeDao;
        mPatternCodeDao = patternCodeDao;
        mAppExecutors = appExecutors;
    }

    public static ManualIrisAnalysisRemoteDatasourceImpl getInstance(
            @NonNull UserDao userDao, @NonNull ManagerDao managerDao, @NonNull UserHistoryDao userHistoryDao, @NonNull ImageDao imageDao,
            @NonNull IrisAnalysisDao irisAnalysisDao, @NonNull OrganAnalysisDao organAnalysisDao,
            @NonNull ScriptDao scriptDao, @NonNull OrganCodeDao organCodeDao, @NonNull PatternCodeDao patternCodeDao,
            @NonNull AppExecutors appExecutors) {
        if (INSTANCE == null) {
            synchronized (ManualIrisAnalysisRemoteDatasourceImpl.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ManualIrisAnalysisRemoteDatasourceImpl(userDao, managerDao, userHistoryDao, imageDao,
                            irisAnalysisDao, organAnalysisDao, scriptDao, organCodeDao, patternCodeDao, appExecutors);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void transferIrisInfoToRemote(String ftpPath, FTPInfoVO ftpInfoVO, IrisInfoTransferToRemoteCallback callback) {
        AndroidFTPClient androidFTPClient = AndroidFTPClient.getInstance();
        ArrayList<File> uploadFileList = androidFTPClient.uploadIrisInfoList(ftpPath);

        if (uploadFileList != null && uploadFileList.size() != 0) {
            Runnable runnable = () -> {
                try {
                    if (!androidFTPClient.connectFTP(ftpInfoVO)) throw new Exception("Connection Error !");

                    for (File uploadFile: uploadFileList) {
                        String relativePath = uploadFile.getAbsolutePath().substring(ftpPath.length() + 1);
                        String dateFolder = relativePath.substring(0, relativePath.indexOf("/"));

                        androidFTPClient.makeDirectory(ftpInfoVO.getRemoteRootDir(), dateFolder);

                        boolean fileUploadBol = androidFTPClient.fileUploadFTP(uploadFile);
                        if (!fileUploadBol) throw new Exception("Can't store file !");

                        boolean fileDeleteBol = androidFTPClient.deleteFile(uploadFile);
                        if (!fileDeleteBol) throw new Exception("Can't delete file !");
                    }

                    mAppExecutors.mainThread().execute(() -> callback.succeedToTransferIrisInfo());
                } catch (Exception e) {
                    mAppExecutors.mainThread().execute(() -> callback.failToTransferIrisInfo(e.getMessage()));
                } finally {
                    try {
                        androidFTPClient.disconnectFTP();
                    } catch (IOException ioe) {
                        mAppExecutors.mainThread().execute(() -> callback.failToTransferIrisInfo(ioe.getMessage()));
                    }
                }
            };

            mAppExecutors.networkIO().execute(runnable);
        } else {
            mAppExecutors.mainThread().execute(() -> callback.failToTransferIrisInfo("Upload IrisInfo List is null !"));
        }
    }

    @Override
    public void validateUser(User user, ValidateUserCallback callback) throws Exception {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public boolean assertLogin(String manager_id, String password) {
        return false;
    }

    @Override
    public boolean assertIdentity(String manager_id) {
        return false;
    }

    @Override
    public void insert(Manager manager) {

    }


    @Override
    public void insertUserHistory(UserHistory userHistory) {

    }

    @Override
    public void updateUserDateByID(String analysisDate, String managerId, String userId) {

    }

    @Override
    public int getUserHistoryId(String managerId, String userId, String analysisDate) {
        return 0;
    }

    /** 분석 정보 저장 로직 **/

    @Override
    public void insertAllImage(List<Image> images) {

    }

    @Override
    public void insertIrisAnalysis(IrisAnalysis irisAnalysis) {

    }

    @Override
    public void insertAllIrisAnalysis(List<IrisAnalysis> irisAnalysis) {

    }

    @Override
    public void insertOrganAnalysis(OrganAnalysis organAnalysis) {

    }

    @Override
    public void insertAllOrganAnalysis(List<OrganAnalysis> organAnalyses) {

    }
    /** 스크립트 코드 가져오기 **/

    @Override
    public Script getScriptByID(String scriptCode, String languageCode) {
        return null;
    }

    @Override
    public List<AnalysisScriptResult> getAnalysisScript(int userHistoryId, String languageCode) {
        return null;
    }

    @Override
    public List<OrganScriptResult> getOrganScript(int userHistoryId, String languageCode) {
        return null;
    }

    @Override
    public void saveIrisImageTo(File galleryPath, IrisImageSaveToGalleryCallback callback) {

    }

    @Override
    public void saveIrisInfoToFtpPath(UserHistory userHistory, File ftpPath, IrisInfoSaveToFtpCallback callback) {

    }

    @Override
    public void getManagerBy(String managerId, GetManagerCallback callback) {

    }

    @Override
    public List<OrganPointVO> initAnalysisInformation(Image currImageDto, String side) {
        return null;
    }

    @Override
    public String organCheckOfTouch(int x, int y, Image currImageDto, List<OrganPointVO> organPointList) {
        return null;
    }
}
