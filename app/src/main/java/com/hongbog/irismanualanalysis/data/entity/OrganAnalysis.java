package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.hongbog.irismanualanalysis.util.PatternExistenceToBooleanConverter;
import com.orhanobut.logger.Logger;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 * 사용자 이력 아이디, 방향, 장기 코드,
 * 정상, 열공, 바퀴살, 색소 반점, 결절
 * @Author jslee
 */
@ToString
@AllArgsConstructor
@Entity(tableName = "organ_analysis",
        primaryKeys = {"user_history_id", "side", "organ_code"},
        foreignKeys = {
                @ForeignKey(entity = IrisAnalysis.class,
                        parentColumns = {"user_history_id", "side", "organ_code"},
                        childColumns = {"user_history_id", "side", "organ_code"},
                        onDelete = ForeignKey.CASCADE)})

public class OrganAnalysis implements Parcelable, Cloneable {

    @Ignore
    private static final String EXISTENCE = "Y";
    @Ignore
    private static final String NON_EXISTENCE = "N";

    @NonNull
    @ColumnInfo(name = "user_history_id")
    private int userHistoryId;

    @NonNull
    @ColumnInfo(name = "side")
    private String side; // left, right

    @NonNull
    @ColumnInfo(name = "organ_code")
    private String organCode; // BR, LV, ...

    @NonNull
    @ColumnInfo(name = "normal")
    private String normal;

    @NonNull
    @ColumnInfo(name = "lacuna")
    private String lacuna;

    @NonNull
    @ColumnInfo(name = "spoke")
    private String spoke;

    @NonNull
    @ColumnInfo(name = "pigment_spot")
    private String pigmentSpot;

    @NonNull
    @ColumnInfo(name = "defect")
    private String defect;

    public OrganAnalysis() {
    }

    protected OrganAnalysis(Parcel in) {
        userHistoryId = in.readInt();
        side = in.readString();
        organCode = in.readString();
        normal = in.readString();
        lacuna = in.readString();
        spoke = in.readString();
        pigmentSpot = in.readString();
        defect = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userHistoryId);
        dest.writeString(side);
        dest.writeString(organCode);
        dest.writeString(normal);
        dest.writeString(lacuna);
        dest.writeString(spoke);
        dest.writeString(pigmentSpot);
        dest.writeString(defect);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrganAnalysis> CREATOR = new Creator<OrganAnalysis>() {
        @Override
        public OrganAnalysis createFromParcel(Parcel in) {
            return new OrganAnalysis(in);
        }

        @Override
        public OrganAnalysis[] newArray(int size) {
            return new OrganAnalysis[size];
        }
    };

    @NonNull
    public int getUserHistoryId() {
        return userHistoryId;
    }

    public void setUserHistoryId(@NonNull int userHistoryId) {
        this.userHistoryId = userHistoryId;
    }

    @NonNull
    public String getSide() {
        return side;
    }

    public void setSide(@NonNull String side) {
        this.side = side;
    }

    @NonNull
    public String getOrganCode() {
        return organCode;
    }

    public void setOrganCode(@NonNull String organCode) {
        this.organCode = organCode;
    }

    @NonNull
    public String getNormal() {
        return normal;
    }

    public void setNormal(@NonNull String normal) {
        this.normal = normal;
    }

    @NonNull
    public String getLacuna() {
        return lacuna;
    }

    public void setLacuna(@NonNull String lacuna) {
        this.lacuna = lacuna;
    }

    @NonNull
    public String getSpoke() {
        return spoke;
    }

    public void setSpoke(@NonNull String spoke) {
        this.spoke = spoke;
    }

    @NonNull
    public String getPigmentSpot() {
        return pigmentSpot;
    }

    public void setPigmentSpot(@NonNull String pigmentSpot) {
        this.pigmentSpot = pigmentSpot;
    }

    @NonNull
    public String getDefect() {
        return defect;
    }

    public void setDefect(@NonNull String defect) {
        this.defect = defect;
    }

    public void existNormal() {
        normal = EXISTENCE;
    }

    public void existLacuna() {
        lacuna = EXISTENCE;
    }

    public void existSpoke() {
        spoke = EXISTENCE;
    }

    public void existPigmentSpot() {
        pigmentSpot = EXISTENCE;
    }

    public void existDefect() {
        defect = EXISTENCE;
    }

    public void nonExistNormal() {
        normal = NON_EXISTENCE;
    }

    public void nonExistLacuna() {
        lacuna = NON_EXISTENCE;
    }

    public void nonExistSpoke() {
        spoke = NON_EXISTENCE;
    }

    public void nonExistPigmentSpot() {
        pigmentSpot = NON_EXISTENCE;
    }

    public void nonExistDefect() {
        defect = NON_EXISTENCE;
    }

    public boolean isNormalPattern() {
        return PatternExistenceToBooleanConverter.existenceToBoolean(normal);
    }

    public boolean isLacunaPattern() {
        return PatternExistenceToBooleanConverter.existenceToBoolean(lacuna);
    }

    public boolean isSpokePattern() {
        return PatternExistenceToBooleanConverter.existenceToBoolean(spoke);
    }

    public boolean isPigmentSpotPattern() {
        return PatternExistenceToBooleanConverter.existenceToBoolean(pigmentSpot);
    }

    public boolean isDefectPattern() {
        return PatternExistenceToBooleanConverter.existenceToBoolean(defect);
    }

    /**
     * 레퍼런스를 끊고 불변 타입에 데이터를 복사 한다.
     * @return
     * @throws CloneNotSupportedException
     * @author taein
     */
    @Override
    protected OrganAnalysis clone() throws CloneNotSupportedException {
        return (OrganAnalysis) super.clone();
    }

    /**
     * side 와 패턴들이 비워 있는지 검사 한다.
     * @return
     * @author taein
     */
    public boolean isEmpty() {
        return TextUtils.isEmpty(side) ||
                TextUtils.isEmpty(organCode) ||
                TextUtils.isEmpty(normal) ||
                TextUtils.isEmpty(lacuna) ||
                TextUtils.isEmpty(spoke) ||
                TextUtils.isEmpty(pigmentSpot) ||
                TextUtils.isEmpty(defect);
    }

    /**
     * 장기 패턴에 디폴트 값을 'N'으로 설정 하는 정적 팩토리 메서드
     * @return
     * @author taein
     */
    public static OrganAnalysis newInstanceDefaultPattern() {
        OrganAnalysis organAnalysis = new OrganAnalysis();
        organAnalysis.setNormal(NON_EXISTENCE);
        organAnalysis.setLacuna(NON_EXISTENCE);
        organAnalysis.setSpoke(NON_EXISTENCE);
        organAnalysis.setPigmentSpot(NON_EXISTENCE);
        organAnalysis.setDefect(NON_EXISTENCE);
        return organAnalysis;
    }

    /**
     * 모든 장기 패턴을 'N' 으로 설정 한다.
     * @author taein
     */
    public void setAllPatternsToNoExist() {
        normal = NON_EXISTENCE;
        lacuna = NON_EXISTENCE;
        spoke = NON_EXISTENCE;
        pigmentSpot = NON_EXISTENCE;
        defect = NON_EXISTENCE;
    }
}
