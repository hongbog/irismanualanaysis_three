package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 *  사용자 이력 아이디, 관리자 아이디, 사용자 아이디, 분석 일시
 *  @Author jslee
 * */
@ToString
@AllArgsConstructor
@Entity(tableName = "user_history",
        foreignKeys = {
                @ForeignKey(entity = User.class,
                        parentColumns = {"manager_id", "user_id"},
                        childColumns = {"manager_id", "user_id"},
                        onDelete = ForeignKey.CASCADE)})

public class UserHistory implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "user_history_id")
    private int userHistoryId;

    @NonNull
    @ColumnInfo(name = "manager_id")
    private String managerId;

    @NonNull
    @ColumnInfo(name = "user_id")
    private String userId;

    @NonNull
    @ColumnInfo(name = "analysis_date")
    private String analysisDate;

    @Ignore
    private Image leftImage;

    @Ignore
    private Image rightImage;

    public UserHistory() {
    }

    public UserHistory(@NonNull String managerId, @NonNull String userId, @NonNull String analysisDate) {
        this.managerId = managerId;
        this.userId = userId;
        this.analysisDate = analysisDate;
    }

    protected UserHistory(Parcel in) {
        userHistoryId = in.readInt();
        managerId = in.readString();
        userId = in.readString();
        analysisDate = in.readString();
        leftImage = in.readParcelable(Image.class.getClassLoader());
        rightImage = in.readParcelable(Image.class.getClassLoader());
    }

    public UserHistory(String managerId, String userId) {
        this.managerId = managerId;
        this.userId = userId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userHistoryId);
        dest.writeString(managerId);
        dest.writeString(userId);
        dest.writeString(analysisDate);
        dest.writeParcelable(leftImage, flags);
        dest.writeParcelable(rightImage, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserHistory> CREATOR = new Creator<UserHistory>() {
        @Override
        public UserHistory createFromParcel(Parcel in) {
            return new UserHistory(in);
        }

        @Override
        public UserHistory[] newArray(int size) {
            return new UserHistory[size];
        }
    };

    public Image getLeftImage() {
        return leftImage;
    }

    public void setLeftImage(Image image) {
        this.leftImage = image;
    }

    public Image getRightImage() {
        return rightImage;
    }

    public void setRightImage(Image image) {
        this.rightImage = image;
    }

    public int getUserHistoryId() {
        return userHistoryId;
    }

    public void setUserHistoryId(int userHistoryId) {
        this.userHistoryId = userHistoryId;
    }

    @NonNull
    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(@NonNull String managerId) {
        this.managerId = managerId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    @NonNull
    public String getAnalysisDate() {
        return analysisDate;
    }

    public void setAnalysisDate(@NonNull String analysisDate) {
        this.analysisDate = analysisDate;
    }

    /**
     * UserHistory 멤버 변수들 유효성 검사
     * @return
     * @author taein
     */
    public boolean isEmpty() {
        return (userHistoryId < 0) ||
                TextUtils.isEmpty(managerId) ||
                TextUtils.isEmpty(userId) ||
                TextUtils.isEmpty(analysisDate) ||
                (leftImage == null) ||
                (rightImage == null);
    }
}