package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;

/**
 * @Author jslee
 */
@AllArgsConstructor
@Entity(tableName = "pattern_code")
public class PatternCode implements Parcelable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "pattern_code")
    private int patternCode;

    @NonNull
    @ColumnInfo(name = "pattern_name")
    private String patternName;

    public PatternCode() {
    }

    protected PatternCode(Parcel in) {
        patternCode = in.readInt();
        patternName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(patternCode);
        dest.writeString(patternName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PatternCode> CREATOR = new Creator<PatternCode>() {
        @Override
        public PatternCode createFromParcel(Parcel in) {
            return new PatternCode(in);
        }

        @Override
        public PatternCode[] newArray(int size) {
            return new PatternCode[size];
        }
    };

    public int getPatternCode() {
        return patternCode;
    }

    public void setPatternCode(int patternCode) {
        this.patternCode = patternCode;
    }

    @NonNull
    public String getPatternName() {
        return patternName;
    }

    public void setPatternName(@NonNull String patternName) {
        this.patternName = patternName;
    }
}
