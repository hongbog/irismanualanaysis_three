package com.hongbog.irismanualanalysis.data;

/**
 * getOrganScript함수 수행을 통한 Query수행 결과테이블을 저장하는 클래스
 * (변수명이 Query문에 들어가는 이름과 같아야함)
 * @author jslee
 */
public class AnalysisScriptResult {
    public int user_history_id;
    public String side;
    public String script_code;
    public String country_code;
    public String category;
    public String symptom;
    public String precaution;
    public String healthy_food;
    public String unhealthy_food;

    public int getUser_history_id() {
        return user_history_id;
    }

    public void setUser_history_id(int user_history_id) {
        this.user_history_id = user_history_id;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getScript_code() {
        return script_code;
    }

    public void setScript_code(String script_code) {
        this.script_code = script_code;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getPrecaution() {
        return precaution;
    }

    public void setPrecaution(String precaution) {
        this.precaution = precaution;
    }

    public String getHealthy_food() {
        return healthy_food;
    }

    public void setHealthy_food(String healthy_food) {
        this.healthy_food = healthy_food;
    }

    public String getUnhealthy_food() {
        return unhealthy_food;
    }

    public void setUnhealthy_food(String unhealthy_food) {
        this.unhealthy_food = unhealthy_food;
    }
}
