package com.hongbog.irismanualanalysis.data.entity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import lombok.AllArgsConstructor;
import lombok.ToString;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis.Organ;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.orhanobut.logger.Logger;

/**
 * 사용자 이력 아이디, 방향,
 * 홍채 비트맵 경로, 홍채 맵 비트맵 경로, 중심 x, 중심 y,
 * 동공 반지름, 자율신경환 반지름, 홍채 반지름, 회전 각도, 가로, 세로
 * @Author jslee
 */
@ToString
@AllArgsConstructor
@Entity(tableName = "image",
        primaryKeys = {"user_history_id", "side"},
        foreignKeys = {
                @ForeignKey(entity = UserHistory.class,
                        parentColumns = "user_history_id",
                        childColumns = "user_history_id",
                        onDelete = ForeignKey.CASCADE)})
public class Image implements Parcelable, Cloneable {

    @NonNull
    @ColumnInfo(name = "user_history_id")
    private int userHistoryId;

    @NonNull
    @ColumnInfo(name = "side")
    private String side; // left, right

    @NonNull
    @ColumnInfo(name = "iris_bitmap_path")
    private String irisBitmapPath;

    @NonNull
    @ColumnInfo(name = "iris_map_bitmap_path")
    private String irisMapBitmapPath;

    @NonNull
    @ColumnInfo(name = "center_x")
    private float centerX;

    @NonNull
    @ColumnInfo(name = "center_y")
    private float centerY;

    @NonNull
    @ColumnInfo(name = "pupil_radius")
    private float pupilRadius;

    @NonNull
    @ColumnInfo(name = "autonomic_nerve_wave_radius")
    private float autonomicNerveWaveRadius;

    @NonNull
    @ColumnInfo(name = "iris_radious")
    private float irisRadious;

    @NonNull
    @ColumnInfo(name = "rotation_angle")
    private int rotationAngle;

    @NonNull
    @ColumnInfo(name = "width")
    private float width;

    @NonNull
    @ColumnInfo(name = "length")
    private int length;

    @Ignore
    private IrisAnalysis brainOrganAnalysis;
    @Ignore
    private IrisAnalysis kidneyOrganAnalysis;
    @Ignore
    private IrisAnalysis pancreasOrganAnalysis;
    @Ignore
    private IrisAnalysis lungsOrganAnalysis;
    @Ignore
    private IrisAnalysis liverOrganAnalysis;

    public static Bitmap leftIrisBitmap;
    public static Bitmap leftIrisMapBitmap;
    public static Bitmap leftIrisMapColorBitmap;
    public static Bitmap leftCurrIrisMapColorBitmap;
    public static Bitmap rightIrisBitmap;
    public static Bitmap rightIrisMapBitmap;
    public static Bitmap rightIrisMapColorBitmap;
    public static Bitmap rightCurrIrisMapColorBitmap;

    public static Map<Integer, Bitmap> numberImage = new HashMap<Integer, Bitmap>();
    public static Bitmap checkBitmap;

    public static int colorBrain;
    public static int colorKidney;
    public static int colorLiver;
    public static int colorLungs;
    public static int colorPancreas;

    public Image() {
    }

    protected Image(Parcel in) {
        userHistoryId = in.readInt();
        side = in.readString();
        irisBitmapPath = in.readString();
        irisMapBitmapPath = in.readString();
        centerX = in.readFloat();
        centerY = in.readFloat();
        pupilRadius = in.readFloat();
        autonomicNerveWaveRadius = in.readFloat();
        irisRadious = in.readFloat();
        rotationAngle = in.readInt();
        width = in.readFloat();
        length = in.readInt();
        brainOrganAnalysis = in.readParcelable(IrisAnalysis.class.getClassLoader());
        kidneyOrganAnalysis = in.readParcelable(IrisAnalysis.class.getClassLoader());
        pancreasOrganAnalysis = in.readParcelable(IrisAnalysis.class.getClassLoader());
        lungsOrganAnalysis = in.readParcelable(IrisAnalysis.class.getClassLoader());
        liverOrganAnalysis = in.readParcelable(IrisAnalysis.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userHistoryId);
        dest.writeString(side);
        dest.writeString(irisBitmapPath);
        dest.writeString(irisMapBitmapPath);
        dest.writeFloat(centerX);
        dest.writeFloat(centerY);
        dest.writeFloat(pupilRadius);
        dest.writeFloat(autonomicNerveWaveRadius);
        dest.writeFloat(irisRadious);
        dest.writeInt(rotationAngle);
        dest.writeFloat(width);
        dest.writeInt(length);
        dest.writeParcelable(brainOrganAnalysis, flags);
        dest.writeParcelable(kidneyOrganAnalysis, flags);
        dest.writeParcelable(pancreasOrganAnalysis, flags);
        dest.writeParcelable(lungsOrganAnalysis, flags);
        dest.writeParcelable(liverOrganAnalysis, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public IrisAnalysis getBrainOrganAnalysis() {
        return brainOrganAnalysis;
    }

    public void setBrainOrganAnalysis(IrisAnalysis brainOrganAnalysis) {
        this.brainOrganAnalysis = brainOrganAnalysis;
    }

    public IrisAnalysis getKidneyOrganAnalysis() {
        return kidneyOrganAnalysis;
    }

    public void setKidneyOrganAnalysis(IrisAnalysis kidneyOrganAnalysis) {
        this.kidneyOrganAnalysis = kidneyOrganAnalysis;
    }

    public IrisAnalysis getPancreasOrganAnalysis() {
        return pancreasOrganAnalysis;
    }

    public void setPancreasOrganAnalysis(IrisAnalysis pancreasOrganAnalysis) {
        this.pancreasOrganAnalysis = pancreasOrganAnalysis;
    }

    public IrisAnalysis getLungsOrganAnalysis() {
        return lungsOrganAnalysis;
    }

    public void setLungsOrganAnalysis(IrisAnalysis lungsOrganAnalysis) {
        this.lungsOrganAnalysis = lungsOrganAnalysis;
    }

    public IrisAnalysis getLiverOrganAnalysis() {
        return liverOrganAnalysis;
    }

    public void setLiverOrganAnalysis(IrisAnalysis liverOrganAnalysis) {
        this.liverOrganAnalysis = liverOrganAnalysis;
    }

    @NonNull
    public int getUserHistoryId() {
        return userHistoryId;
    }

    public void setUserHistoryId(@NonNull int userHistoryId) {
        this.userHistoryId = userHistoryId;
    }

    @NonNull
    public String getSide() {
        return side;
    }

    public void setSide(@NonNull String side) {
        this.side = side;
    }

    @NonNull
    public String getIrisBitmapPath() {
        return irisBitmapPath;
    }

    public void setIrisBitmapPath(@NonNull String irisBitmapPath) {
        this.irisBitmapPath = irisBitmapPath;
    }

    @NonNull
    public String getIrisMapBitmapPath() {
        return irisMapBitmapPath;
    }

    public void setIrisMapBitmapPath(@NonNull String irisMapBitmapPath) {
        this.irisMapBitmapPath = irisMapBitmapPath;
    }

    public float getCenterX() {
        return centerX;
    }

    public void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public float getCenterY() {
        return centerY;
    }

    public void setCenterY(float centerY) {
        this.centerY = centerY;
    }

    public float getPupilRadius() {
        return pupilRadius;
    }

    public void setPupilRadius(float pupilRadius) {
        this.pupilRadius = pupilRadius;
    }

    public float getAutonomicNerveWaveRadius() {
        return autonomicNerveWaveRadius;
    }

    public void setAutonomicNerveWaveRadius(float autonomicNerveWaveRadius) {
        this.autonomicNerveWaveRadius = autonomicNerveWaveRadius;
    }

    public float getIrisRadious() {
        return irisRadious;
    }

    public void setIrisRadious(float irisRadious) {
        this.irisRadious = irisRadious;
    }

    public int getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    @NonNull
    public float getWidth() {
        return width;
    }

    public void setWidth(@NonNull float width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    /**
     * static 변수들을 제거 한다.
     *
     * @author taein
     */
    public static void removeStaticVariable() {
        leftIrisBitmap = null;
        leftIrisMapBitmap = null;
        rightIrisBitmap = null;
        rightIrisMapBitmap = null;
        leftIrisMapColorBitmap = null;
        leftCurrIrisMapColorBitmap = null;
        rightIrisMapColorBitmap = null;
        rightCurrIrisMapColorBitmap = null;
    }

    /**
     * organCode 를 보고 해당 장기 IrisAnalysis 에 넣어 준다.
     * @param irisAnalysis
     * @author taein
     */
    public void setOrganAnalysisBy(IrisAnalysis irisAnalysis) {
        final String organCode = irisAnalysis.getOrganCode();
        switch (organCode) {
            case Organ.BRAIN_ORGAN:
                brainOrganAnalysis = irisAnalysis;
                break;
            case Organ.KIDENY_ORGAN:
                kidneyOrganAnalysis = irisAnalysis;
                break;
            case Organ.LIVER_ORGAN:
                liverOrganAnalysis = irisAnalysis;
                break;
            case Organ.LUNGS_ORGAN:
                lungsOrganAnalysis = irisAnalysis;
                break;
            case Organ.PANCREAS_ORGAN:
                pancreasOrganAnalysis = irisAnalysis;
                break;
        }
    }

    public IrisAnalysis getIrisAnalysisBy(String organCode) {
        switch (organCode) {
            case Organ.BRAIN_ORGAN:
                if (brainOrganAnalysis == null)
                    return IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
                return brainOrganAnalysis;
            case Organ.KIDENY_ORGAN:
                if (kidneyOrganAnalysis == null)
                    return IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
                return kidneyOrganAnalysis;
            case Organ.LIVER_ORGAN:
                if (liverOrganAnalysis == null)
                    return IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
                return liverOrganAnalysis;
            case Organ.LUNGS_ORGAN:
                if (lungsOrganAnalysis == null)
                    return IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
                return lungsOrganAnalysis;
            case Organ.PANCREAS_ORGAN:
                if (pancreasOrganAnalysis == null)
                    return IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
                return pancreasOrganAnalysis;
            default:
                return IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
        }
    }

    /**
     * side 와 장기별 IrisAnalysis 가 비워 있는지 검사 한다.
     * @return
     * @author taein
     */
    public boolean isEmpty() {
        if(TextUtils.equals(UserInfoActivity.IRIS_LEFT, side)) {
            return isEmptyLeftImage();

        } else if(TextUtils.equals(UserInfoActivity.IRIS_RIGHT, side)) {
            return isEmptyRightImage();
        }
        return true;
    }

    /**
     * 오른쪽 이미지에 side 와 장기별 IrisAnalysis 가 비워 있는지 검사 한다.
     * @return
     * @author taein
     */
    private boolean isEmptyRightImage() {
        return TextUtils.isEmpty(side) ||
                (brainOrganAnalysis == null || brainOrganAnalysis.isEmpty()) ||
                (kidneyOrganAnalysis == null || kidneyOrganAnalysis.isEmpty()) ||
                (pancreasOrganAnalysis == null || pancreasOrganAnalysis.isEmpty()) ||
                (lungsOrganAnalysis == null || lungsOrganAnalysis.isEmpty()) ||
                (liverOrganAnalysis == null || liverOrganAnalysis.isEmpty());
    }

    /**
     * 왼쪽 이미지에 side 와 장기별 IrisAnalysis 가 비워 있는지 검사 한다.
     * 왼쪽 홍채 이미지에 경우 liverOrganAnalysis 항상 null 이다.
     * @return
     * @author taein
     */
    private boolean isEmptyLeftImage() {
        return TextUtils.isEmpty(side) ||
                (brainOrganAnalysis == null || brainOrganAnalysis.isEmpty()) ||
                (kidneyOrganAnalysis == null || kidneyOrganAnalysis.isEmpty()) ||
                (pancreasOrganAnalysis == null || pancreasOrganAnalysis.isEmpty()) ||
                (lungsOrganAnalysis == null || lungsOrganAnalysis.isEmpty());
    }
}
