package com.hongbog.irismanualanalysis.data.dao;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.Script;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ImageDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAllImage(List<Image> imageList);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertImage(Image image);

    @Query("DELETE FROM image")
    void deleteImage();

    @Update
    void updateImage(Image irisImage);
}
