package com.hongbog.irismanualanalysis.data.vo;

import java.util.ArrayList;
import java.util.List;

public class OrganPointVO {
    private String organCode;
    private List<PointVO> organPoint;
    private PointVO organCenterPoint;
    private Boolean organCheck;

    public OrganPointVO(String organCode) {
        this.organCode = organCode;
        organPoint = new ArrayList<PointVO>();
        this.organCheck = true;
    }

    public String getOrganCode() {
        return organCode;
    }

    public void setOrganCode(String organCode) {
        this.organCode = organCode;
    }

    public List<PointVO> getOrganPoint() {
        return organPoint;
    }

    public void setOrganPoint(List<PointVO> organPoint) {
        this.organPoint = organPoint;
    }

    public PointVO getOrganCenterPoint() {
        return organCenterPoint;
    }

    public void setOrganCenterPoint(PointVO organCenterPoint) {
        this.organCenterPoint = organCenterPoint;
    }

    public Boolean getOrganCheck() {
        return organCheck;
    }

    public void setOrganCheck(Boolean organCheck) {
        this.organCheck = organCheck;
    }
}
