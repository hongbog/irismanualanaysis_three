package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 *  사용자 이력 아이디, 방향, 장기 코드,
 *  동공 코드, 자율신경환 코드, 스트레스링 코드, 콜레스테롤링 코드,
 *  @Author jslee
 *
 * */
@ToString
@AllArgsConstructor
@Entity(tableName = "iris_analysis",
        primaryKeys = {"user_history_id", "side", "organ_code"},
        foreignKeys = {
                @ForeignKey(entity = Image.class,
                        parentColumns = {"user_history_id", "side"},
                        childColumns = {"user_history_id", "side"},
                        onDelete = ForeignKey.CASCADE)})
public class IrisAnalysis implements Parcelable, Cloneable {

    public interface Organ {
        String BRAIN_ORGAN = "BR";
        String KIDENY_ORGAN = "KD";
        String LIVER_ORGAN = "LV";
        String LUNGS_ORGAN = "LG";
        String PANCREAS_ORGAN = "PC";
    }

    @NonNull
    @ColumnInfo(name = "user_history_id")
    private int userHistoryId;

    @NonNull
    @ColumnInfo(name = "side")
    private String side; // left, right

    @NonNull
    @ColumnInfo(name = "organ_code")
    private String organCode;

    @NonNull
    @ColumnInfo(name = "stress_ring_code")
    private String stressRingCode; // 스트레스링

    @NonNull
    @ColumnInfo(name = "cholesterol_ring_code")
    private String cholesterolRingCode;

    @Ignore
    private OrganAnalysis organAnalysis;

    public IrisAnalysis() {
    }

    public IrisAnalysis(int userHistoryId, @NonNull String side) {
        this.userHistoryId = userHistoryId;
        this.side = side;
    }

    public IrisAnalysis(int userHistoryId, @NonNull String side, @NonNull String organCode,
                        @NonNull String stressRingCode, @NonNull String cholesterolRingCode) {
        this.userHistoryId = userHistoryId;
        this.side = side;
        this.organCode = organCode;
        this.stressRingCode = stressRingCode;
        this.cholesterolRingCode = cholesterolRingCode;
    }

    protected IrisAnalysis(Parcel in) {
        userHistoryId = in.readInt();
        side = in.readString();
        organCode = in.readString();
        stressRingCode = in.readString();
        cholesterolRingCode = in.readString();
        organAnalysis = in.readParcelable(OrganAnalysis.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userHistoryId);
        dest.writeString(side);
        dest.writeString(organCode);
        dest.writeString(stressRingCode);
        dest.writeString(cholesterolRingCode);
        dest.writeParcelable(organAnalysis, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IrisAnalysis> CREATOR = new Creator<IrisAnalysis>() {
        @Override
        public IrisAnalysis createFromParcel(Parcel in) {
            return new IrisAnalysis(in);
        }

        @Override
        public IrisAnalysis[] newArray(int size) {
            return new IrisAnalysis[size];
        }
    };

    public OrganAnalysis getOrganAnalysis() {
        return organAnalysis;
    }

    public void setOrganAnalysis(OrganAnalysis organAnalysis) {
        this.organAnalysis = organAnalysis;
    }

    @NonNull
    public int getUserHistoryId() {
        return userHistoryId;
    }

    public void setUserHistoryId(@NonNull int userHistoryId) {
        this.userHistoryId = userHistoryId;
    }

    @NonNull
    public String getSide() {
        return side;
    }

    public void setSide(@NonNull String side) {
        this.side = side;
    }

    @NonNull
    public String getOrganCode() {
        return organCode;
    }

    public void setOrganCode(@NonNull String organCode) {
        this.organCode = organCode;
    }

    public String getStressRingCode() {
        return stressRingCode;
    }

    public void setStressRingCode(String stressRingCode) {
        this.stressRingCode = stressRingCode;
    }

    public String getCholesterolRingCode() {
        return cholesterolRingCode;
    }

    public void setCholesterolRingCode(String cholesterolRingCode) {
        this.cholesterolRingCode = cholesterolRingCode;
    }

    /**
     * 레퍼런스를 끊고 새로운 객체를 복사 한다.
     * 장기 마다에 중복된 데이터를 복사 하기 위해 사용 된다.
     * @return
     * @throws CloneNotSupportedException
     * @author taein
     */
    @Override
    public IrisAnalysis clone() throws CloneNotSupportedException {
        IrisAnalysis irisAnalysis = (IrisAnalysis) super.clone();
        irisAnalysis.organAnalysis = organAnalysis.clone();
        return irisAnalysis;
    }

    /**
     * 멤버 변수들이 비워 있는지 검사 한다.
     * @return
     * @author taein
     */
    public boolean isEmpty() {
        return TextUtils.isEmpty(side) ||
                TextUtils.isEmpty(organCode) ||
                TextUtils.isEmpty(stressRingCode) ||
                TextUtils.isEmpty(cholesterolRingCode) ||
                (organAnalysis == null || organAnalysis.isEmpty());
    }

    /**
     * OrganAnalysis 에 장기 패턴을 디폴트 'N' 으로 설정 하는
     * 정적 팩토리 메서드
     * @return
     */
    public static IrisAnalysis newInstanceAsDefaultOrganAnalysis() {
        IrisAnalysis irisAnalysis = new IrisAnalysis();
        irisAnalysis.setOrganAnalysis(OrganAnalysis.newInstanceDefaultPattern());
        return irisAnalysis;
    }
}
