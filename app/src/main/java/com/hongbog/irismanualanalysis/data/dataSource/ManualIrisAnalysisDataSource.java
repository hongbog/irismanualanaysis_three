package com.hongbog.irismanualanalysis.data.dataSource;


import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.OrganCode;
import com.hongbog.irismanualanalysis.data.entity.PatternCode;
import com.hongbog.irismanualanalysis.data.entity.Script;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.hongbog.irismanualanalysis.data.vo.OrganPointVO;

import java.io.File;
import java.util.List;

public interface ManualIrisAnalysisDataSource {

    /** manager 가입 로직 **/

    void deleteAll();

    boolean assertLogin(String manager_id, String password);

    boolean assertIdentity(String manager_id);

    void insert(Manager manager);

    /** UserHistory 존재여부 및 입력 로직 **/

    void insertUserHistory(UserHistory userHistory);

    void updateUserDateByID(String analysisDate, String managerId, String userId);

    int getUserHistoryId(String managerId, String userId, String analysisDate);

    /** 분석 정보 저장 로직 **/

    void insertAllImage(List<Image> images);

    void insertIrisAnalysis(IrisAnalysis irisAnalysis);

    void insertAllIrisAnalysis(List<IrisAnalysis> irisAnalysis);

    void insertOrganAnalysis(OrganAnalysis organAnalysis);

    void insertAllOrganAnalysis(List<OrganAnalysis> organAnalyses);


    /** 스크립트 코드 가져오기 **/

    Script getScriptByID(String scriptCode, String languageCode);

    List<AnalysisScriptResult> getAnalysisScript(int userHistoryId, String languageCode);

    List<OrganScriptResult> getOrganScript(int userHistoryId, String languageCode);


    void saveIrisImageTo(File galleryPath, IrisImageSaveToGalleryCallback callback);

    void saveIrisInfoToFtpPath(UserHistory userHistory, File ftpPath, IrisInfoSaveToFtpCallback callback);

    interface IrisImageSaveToGalleryCallback {

        void succeedToSaveIrisImageAndScanTo(File mediaScanPath);

        void failToSaveIrisImage();
    }

    interface IrisInfoSaveToFtpCallback {

        void succeedToSaveIrisInfo();

        void failToSaveIrisInfo();
    }

    interface GetManagerCallback {

        void onManagerLoaded(Manager manager);

        void onManagerNotAvailable();
    }

    void getManagerBy(String managerId, GetManagerCallback callback);

    /**
     * 분석 정보 초기화 관련 함수
     */
    List<OrganPointVO> initAnalysisInformation(Image currImageDto, String side);

    String organCheckOfTouch(int x, int y, Image currImageDto, List<OrganPointVO> organPointList);

    /**
     * 원격지 파일 전송 관련 함수
     */
    void transferIrisInfoToRemote(String ftpPath, FTPInfoVO ftpInfoVO, IrisInfoTransferToRemoteCallback callback);

    interface IrisInfoTransferToRemoteCallback {
        void succeedToTransferIrisInfo();
        void failToTransferIrisInfo(String errorMessage);
    }

    /**
     * EditText로 받아온 UserID를 User에 저장후, UserID(Email)가 존재하는 지 확인.
     * 존재하면 user정보 update 하고, 만약 존재하지 않으면 user insert.
     * @param user
     * @param callback
     * @throws Exception
     * @Author jslee
     */
    void validateUser(User user,ValidateUserCallback callback) throws Exception;

    interface ValidateUserCallback{
        void succeedToValidateUser();
        void failToValidateUser();
    }
}
