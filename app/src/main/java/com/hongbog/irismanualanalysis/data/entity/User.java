package com.hongbog.irismanualanalysis.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.login.LoginActivity;
import com.hongbog.irismanualanalysis.util.DateUtil;
import com.hongbog.irismanualanalysis.util.FileUtil;
import com.hongbog.irismanualanalysis.util.SexToStringConverter;
import com.hongbog.irismanualanalysis.util.StringWithIntConverter;
import com.orhanobut.logger.Logger;

import org.w3c.dom.Text;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.TypeConverters;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 관리자 아이디, 사용자 아이디, 나이, 몸무게, 키, 성별, 최근 분석 일시
 * @Author jslee
 */
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity(tableName = "user",
        primaryKeys = {"manager_id", "user_id"},
        foreignKeys = {
                @ForeignKey(entity = Manager.class,
                        parentColumns = "manager_id",
                        childColumns = "manager_id",
                        onDelete = ForeignKey.CASCADE)})
public class User implements Parcelable {

    @Ignore
    private static final int INVALID_INT_VALUE = -999;

    /**
     * 성별을 강제화 시킨 성별 타입
     * 성별에 따른 현지화 언어를 멤버 변수로 갖는다
     * @author taein
     */
    public enum Sex {
        M, F;
        @Getter
        @Setter
        private String localizedMale;
        @Getter @Setter
        private String localizedFemale;
    }

    @NonNull
    @ColumnInfo(name = "manager_id")
    private String managerId;

    @NonNull
    @ColumnInfo(name = "user_id")
    private String userId;

    @NonNull
    private int age;

    @NonNull
    private int weight;

    @NonNull
    private int height;

    @NonNull
    @TypeConverters({SexToStringConverter.class})
    private Sex sex;

    @NonNull
    @ColumnInfo(name = "lately_analysis_date")
    private String latelyAnalysisDate;

    @Ignore
    private UserHistory userHistory;

    public User() {
    }


    protected User(Parcel in) {
        managerId = in.readString();
        userId = in.readString();
        age = in.readInt();
        weight = in.readInt();
        height = in.readInt();
        sex = Sex.valueOf(in.readString());
        latelyAnalysisDate = in.readString();
        userHistory = in.readParcelable(UserHistory.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(managerId);
        dest.writeString(userId);
        dest.writeInt(age);
        dest.writeInt(weight);
        dest.writeInt(height);
        dest.writeString(sex.toString());
        dest.writeString(latelyAnalysisDate);
        dest.writeParcelable(userHistory, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public UserHistory getUserHistory() {
        return userHistory;
    }

    public void setUserHistory(UserHistory userHistory) {
        this.userHistory = userHistory;
    }

    @NonNull
    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(@NonNull String managerId) {
        this.managerId = managerId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @NonNull
    public Sex getSex() {
        return sex;
    }

    public void setSex(@NonNull Sex sex) {
        this.sex = sex;
    }

    @NonNull
    public String getLatelyAnalysisDate() {
        return latelyAnalysisDate;
    }

    public void setLatelyAnalysisDate(@NonNull String latelyAnalysisDate) {
        this.latelyAnalysisDate = latelyAnalysisDate;
    }

    /**
     * managerId, 현지화 된 성별 언어 값을 가지고 User 객체를
     * 생성하는 정적 팩토리 매서드
     * @author taein
     * @param managerId
     * @param localizedMale
     * @param localizedFemale
     * @return User
     */
    public static User newInstance(String managerId, String localizedMale, String localizedFemale) {
        Sex.M.setLocalizedMale(localizedMale);
        Sex.M.setLocalizedFemale(localizedFemale);
        Sex.F.setLocalizedMale(localizedMale);
        Sex.F.setLocalizedFemale(localizedFemale);

        final UserHistory userHistory = new UserHistory(managerId, "");

        return new User(managerId, "",
                INVALID_INT_VALUE, INVALID_INT_VALUE, INVALID_INT_VALUE, Sex.M,
                DateUtil.getCurrentDate(DateUtil.DatePattern.NoWhiteSpace), userHistory);
    }

    /**
     * User 멤버 변수들 유효성 검사
     * @return
     * @author taein
     */
    public boolean isEmpty() {
        return TextUtils.isEmpty(managerId) ||
            TextUtils.isEmpty(userId) ||
            TextUtils.isEmpty(latelyAnalysisDate) ||
            (age <= 0) ||
            (weight <= 0) ||
            (height <= 0) ||
            (sex == null) ||
            (userHistory == null) ||
            (userHistory.getLeftImage() == null) ||
            (userHistory.getRightImage() == null);
    }
}

