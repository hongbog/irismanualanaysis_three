package com.hongbog.irismanualanalysis.data.dao;

import com.hongbog.irismanualanalysis.data.entity.User;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


@Dao
public interface UserDao {

    @Query("SELECT * from user WHERE manager_id = :managerId and user_id = :userId")
    User getUserByID( String managerId, String userId);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertUser(User user);

    @Query("update user set lately_analysis_date = :analysisDate where manager_id = :managerId and user_id = :userId")
    void updateUserDateByID(String analysisDate, String managerId, String userId );

    @Update
    void updateUser(User user);

    @Query("DELETE FROM user")
    void deleteUser();

}
