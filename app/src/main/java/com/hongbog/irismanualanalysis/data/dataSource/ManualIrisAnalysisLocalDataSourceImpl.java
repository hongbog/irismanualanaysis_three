package com.hongbog.irismanualanalysis.data.dataSource;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.hongbog.irismanualanalysis.capture.ImageConstant;
import com.hongbog.irismanualanalysis.data.AppExecutors;
import com.hongbog.irismanualanalysis.data.AnalysisScriptResult;
import com.hongbog.irismanualanalysis.data.OrganScriptResult;
import com.hongbog.irismanualanalysis.data.dao.ImageDao;
import com.hongbog.irismanualanalysis.data.dao.IrisAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.ManagerDao;
import com.hongbog.irismanualanalysis.data.dao.OrganAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.OrganCodeDao;
import com.hongbog.irismanualanalysis.data.dao.PatternCodeDao;
import com.hongbog.irismanualanalysis.data.dao.ScriptDao;
import com.hongbog.irismanualanalysis.data.dao.UserDao;
import com.hongbog.irismanualanalysis.data.dao.UserHistoryDao;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.OrganCode;
import com.hongbog.irismanualanalysis.data.entity.PatternCode;
import com.hongbog.irismanualanalysis.data.entity.Script;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;
import com.hongbog.irismanualanalysis.data.vo.OrganPointVO;
import com.hongbog.irismanualanalysis.data.vo.PointVO;
import com.hongbog.irismanualanalysis.util.Encryption;
import com.hongbog.irismanualanalysis.util.FileUtil;
import com.hongbog.irismanualanalysis.util.ImageUtil;
import com.orhanobut.logger.Logger;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.hongbog.irismanualanalysis.util.FileUtil.FileWithDotForExtension;

import javax.crypto.Cipher;

import androidx.annotation.NonNull;

public class ManualIrisAnalysisLocalDataSourceImpl implements ManualIrisAnalysisDataSource {

    private static volatile ManualIrisAnalysisLocalDataSourceImpl INSTANCE;
    private ManagerDao mManagerDao;
    private UserDao mUserDao;
    private AppExecutors mAppExecutors;
    private UserHistoryDao mUserHistoryDao;
    private ImageDao mImageDao;
    private IrisAnalysisDao mIrisAnalysisDao;
    private OrganAnalysisDao mOrganAnalysisDao;
    private ScriptDao mScriptDao;
    private OrganCodeDao mOrganCodeDao;
    private PatternCodeDao mPatternCodeDao;
    private final Bitmap.CompressFormat BITMAP_FORMAT = Bitmap.CompressFormat.PNG;
    private final String JSON_EXT = "json";

    private ManualIrisAnalysisLocalDataSourceImpl(@NonNull UserDao userDao, @NonNull ManagerDao managerDao,
                                                  @NonNull UserHistoryDao userHistoryDao, @NonNull ImageDao imageDao,
                                                  @NonNull IrisAnalysisDao irisAnalysisDao, @NonNull OrganAnalysisDao organAnalysisDao,
                                                  @NonNull ScriptDao scriptDao, @NonNull OrganCodeDao organCodeDao, @NonNull PatternCodeDao patternCodeDao,
                                                  AppExecutors appExecutors) {
        mUserDao = userDao;
        mManagerDao = managerDao;
        mUserHistoryDao = userHistoryDao;
        mImageDao = imageDao;
        mIrisAnalysisDao = irisAnalysisDao;
        mOrganAnalysisDao = organAnalysisDao;
        mScriptDao = scriptDao;
        mOrganCodeDao = organCodeDao;
        mPatternCodeDao = patternCodeDao;
        mAppExecutors = appExecutors;
    }

    public static ManualIrisAnalysisLocalDataSourceImpl getInstance(
            @NonNull UserDao userDao, @NonNull ManagerDao managerDao, @NonNull UserHistoryDao userHistoryDao, @NonNull ImageDao imageDao,
            @NonNull IrisAnalysisDao irisAnalysisDao, @NonNull OrganAnalysisDao organAnalysisDao,
            @NonNull ScriptDao scriptDao, @NonNull OrganCodeDao organCodeDao, @NonNull PatternCodeDao patternCodeDao,
            @NonNull AppExecutors appExecutors) {
        if (INSTANCE == null) {
            synchronized (ManualIrisAnalysisLocalDataSourceImpl.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ManualIrisAnalysisLocalDataSourceImpl(userDao, managerDao, userHistoryDao, imageDao,
                            irisAnalysisDao, organAnalysisDao, scriptDao, organCodeDao, patternCodeDao, appExecutors);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * manager 가입 로직
     **/
    @Override
    public void deleteAll() {
        mManagerDao.deleteAll();
    }

    @Override
    public boolean assertLogin(String manager_id, String password) {
        try {
            return new assertLoginAsyncTask(mManagerDao).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, manager_id, password).get();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean assertIdentity(String manager_id) {
        try {
            return new assertIdentityAsyncTask(mManagerDao).execute(manager_id).get();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void insert(Manager manager) {
        new insertAsyncTask(mManagerDao).execute(manager);
    }

    private static class assertLoginAsyncTask extends AsyncTask<String, Void, Boolean> {
        private ManagerDao asyncTaskDao;
        private boolean RESULT_CORRECT = true;
        private boolean RESULT_INVALID = false;

        assertLoginAsyncTask(ManagerDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            Manager manager = asyncTaskDao.assertLogin(params[0], params[1]);
            if (manager == null) {
                return RESULT_INVALID;
            } else if (manager.getManagerId().equals(params[0]) && manager.getManagerPassword().equals(params[1])) {
                return RESULT_CORRECT;
            }
            return RESULT_INVALID;
        }
    }

    private static class assertIdentityAsyncTask extends AsyncTask<String, Void, Boolean> {
        private ManagerDao asyncTaskDao;
        private boolean RESULT_OK = true;
        private boolean RESULT_FAIL = false;

        assertIdentityAsyncTask(ManagerDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            Manager manager = asyncTaskDao.assertIdentity(params[0]);
            if (manager == null) {
                return RESULT_OK;
            } else {
                return RESULT_FAIL;
            }
        }
    }

    private static class insertAsyncTask extends AsyncTask<Manager, Void, Void> {
        private ManagerDao asyncTaskDao;

        insertAsyncTask(ManagerDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Manager... params) {
            asyncTaskDao.insertManager(params[0]);
            return null;
        }
    }


    /**
     * user 존재여부 및 입력 로직
     **/

    // 1. 기존 user_id 가 존재하는 지 확인: getUserByID
    public boolean isExistUser(String managerId, String userId) {
        try {
            return new isExistUserAsyncTask(mUserDao).execute(managerId, userId).get();
        } catch (Exception e) {
            return false;
        }
    }

    private class isExistUserAsyncTask extends AsyncTask<String, Void, Boolean> {
        private UserDao mAsyncTaskDao;
        private boolean RESULT_OK = true;
        private boolean RESULT_FAIL = false;

        public isExistUserAsyncTask(UserDao mUserDao) {
            mAsyncTaskDao = mUserDao;
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            User user = mAsyncTaskDao.getUserByID(strings[0], strings[1]);
            Log.d("jseseon", "getUserByID: " + String.valueOf(user));
            if (user == null) {
                return RESULT_FAIL;
            } else {
                return RESULT_OK;
            }
        }
    }

    public void insertUserHistory(UserHistory userHistory) {
        new insertUserHistoryAsyncTask(mUserHistoryDao).execute(userHistory);
    }

    private static class insertUserHistoryAsyncTask extends AsyncTask<UserHistory, Void, Void> {
        private UserHistoryDao mAsyncTaskDao;

        insertUserHistoryAsyncTask(UserHistoryDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final UserHistory... params) {
            mAsyncTaskDao.insertUserHistory(params[0]);
            return null;
        }
    }

    // 3. 기존 user_id 가 존재하는 경우: insertUserHistory, updateUserDateByID


    /**
     * 분석 정보 저장 로직
     **/
    public void insertAllImage(List<Image> images) {
        new insertAllImageAsyncTask(mImageDao).execute(images);
    }
    private static class insertAllImageAsyncTask extends AsyncTask<List<Image>, Void, Void> {
        private ImageDao mAsyncTaskDao;

        insertAllImageAsyncTask(ImageDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<Image>... params) {
            mAsyncTaskDao.insertAllImage(params[0]);
            return null;
        }
    }

    // 5. 홍채 분석 정보 저장: insertIrisAnalysis
    public void insertIrisAnalysis(IrisAnalysis irisAnalysis) {
        new insertIrisAnalysisAsyncTask(mIrisAnalysisDao).execute(irisAnalysis);
    }

    private static class insertIrisAnalysisAsyncTask extends AsyncTask<IrisAnalysis, Void, Void> {
        private IrisAnalysisDao mAsyncTaskDao;

        insertIrisAnalysisAsyncTask(IrisAnalysisDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final IrisAnalysis... params) {
            mAsyncTaskDao.insertIrisAnalysis(params[0]);
            return null;
        }
    }

    public void insertAllIrisAnalysis(List<IrisAnalysis> irisAnalyses) {
        new insertAllIrisAnalysisAsyncTask(mIrisAnalysisDao).execute(irisAnalyses);
    }
    private static class insertAllIrisAnalysisAsyncTask extends AsyncTask<List<IrisAnalysis>, Void, Void> {
        private IrisAnalysisDao mAsyncTaskDao;

        insertAllIrisAnalysisAsyncTask(IrisAnalysisDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<IrisAnalysis>... params) {
            mAsyncTaskDao.insertAllIrisAnalysis(params[0]);
            return null;
        }
    }

    //  6. 장기 분석 정보 저장: insertOrganAnalysis
    public void insertOrganAnalysis(OrganAnalysis organAnalysis) {
        new insertOrganAnalysisAsyncTask(mOrganAnalysisDao).execute(organAnalysis);
    }
    private static class insertOrganAnalysisAsyncTask extends AsyncTask<OrganAnalysis, Void, Void> {
        private OrganAnalysisDao mAsyncTaskDao;

        insertOrganAnalysisAsyncTask(OrganAnalysisDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final OrganAnalysis... params) {
            mAsyncTaskDao.insertOrganAnalysis(params[0]);
            return null;
        }
    }

    @Override
    public void insertAllOrganAnalysis(List<OrganAnalysis> organAnalyses) {
        new insertAllOrganAnalysisAsyncTask(mOrganAnalysisDao).execute(organAnalyses);
    }
    private static class insertAllOrganAnalysisAsyncTask extends AsyncTask<List<OrganAnalysis>, Void, Void> {
        private OrganAnalysisDao mAsyncTaskDao;

        insertAllOrganAnalysisAsyncTask(OrganAnalysisDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<OrganAnalysis>... params) {

            mAsyncTaskDao.insertAllOrganAnalysis(params[0]);
            return null;
        }
    }

    /** 동공/자율신경환/스트레스링/콜레스테롤링 스크립트 출력 SQL **/
    @Override
    public Script getScriptByID(String scriptCode, String languageCode) {
        try {
            return new getScriptByIDAsyncTask(mScriptDao).execute(scriptCode, languageCode).get();
        } catch (Exception e) {
            return null;
        }
    }

    private static class getScriptByIDAsyncTask extends AsyncTask<String, Void, Script> {
        private ScriptDao mAsyncTaskDao;
        getScriptByIDAsyncTask(ScriptDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Script doInBackground(String... strings) {
            return mAsyncTaskDao.getScriptByID(strings[0], strings[1]);
        }
    }
    /** 장기 패턴 스크립트 출력 SQL **/

    @Override
    public List<AnalysisScriptResult> getAnalysisScript(int userHistoryId, String languageCode){
        try {
            return new getAnalysisScriptAsyncTask(mScriptDao).execute(userHistoryId, languageCode).get();
        }catch (Exception e) {
            return null;
        }
    }

    private static class getAnalysisScriptAsyncTask extends AsyncTask<Object, Void, List<AnalysisScriptResult>> {
        private ScriptDao mAsyncTaskDao;
        getAnalysisScriptAsyncTask(ScriptDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected List<AnalysisScriptResult> doInBackground(Object... objects) {
            List<AnalysisScriptResult> sr =  mAsyncTaskDao.getAnalysisScript(Integer.parseInt(objects[0].toString()), objects[1].toString());
            return sr;
        }
    }


    @Override
    public List<OrganScriptResult> getOrganScript(int userHistoryId, String languageCode){
        try {
            return new getOrganScriptAsyncTask(mScriptDao).execute(userHistoryId, languageCode).get();
        }catch (Exception e) {
            return null;
        }
    }

    private static class getOrganScriptAsyncTask extends AsyncTask<Object, Void, List<OrganScriptResult>> {
        private ScriptDao mAsyncTaskDao;
        getOrganScriptAsyncTask(ScriptDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected List<OrganScriptResult> doInBackground(Object... objects) {
            List<OrganScriptResult> sr =  mAsyncTaskDao.getOrganScript(Integer.parseInt(objects[0].toString()), objects[1].toString(), "Y", "N");
            return sr;
        }
    }


    /**
     * 왼쪽 홍채 이미지, 오른쪽 홍채 이미지
     * DCIM/hongbog/agencyName/currentDate 에 파일을 저장 하고
     * 미디어 스캔을 위해 해당 디렉토리를 콜백으로 반환 한다.
     * @author taein
     * @param galleryPath
     * @param callback
     */
    @Override
    public void saveIrisImageTo(File galleryPath, IrisImageSaveToGalleryCallback callback) {
        final String leftIrisImageFileName = FileUtil.getUniqueFileName();
        final String rightIrisImageFileName = FileUtil.getUniqueFileName();

        final File leftIrisImageForGallery = new FileWithDotForExtension(galleryPath,
                leftIrisImageFileName, BITMAP_FORMAT.toString());
        final File rightIrisImageForGallery = new FileWithDotForExtension(galleryPath,
                rightIrisImageFileName, BITMAP_FORMAT.toString());

        Runnable runnable = () -> {
            try {
                ImageUtil.saveBitmap(leftIrisImageForGallery, Image.leftIrisBitmap, BITMAP_FORMAT);
                ImageUtil.saveBitmap(rightIrisImageForGallery, Image.rightIrisBitmap, BITMAP_FORMAT);

                mAppExecutors.mainThread().execute(() ->
                        callback.succeedToSaveIrisImageAndScanTo(galleryPath));
            } catch (IOException e) {
                Logger.e(e, "raise Exception for save Iris info Thread");
                mAppExecutors.mainThread().execute(callback::failToSaveIrisImage);
            }
        };

        mAppExecutors.diskIO().execute(runnable);
    }

    /**
     * - 왼쪽 홍채 이미지, 오른쪽 홍채 이미지
     * - 왼쪽 홍채맵 이미지, 오른쪽 홍채맵 이미지
     * - UserHistory json 파일
     * 을 ftpPath 에 저장 한다.
     * @author taein
     * @param userHistory
     * @param ftpPath
     * @param callback
     */
    @Override
    public void saveIrisInfoToFtpPath(UserHistory userHistory, File ftpPath, IrisInfoSaveToFtpCallback callback) {
        /* final String leftIrisImageFileName = FileUtil.getUniqueFileName();
        final String rightIrisImageFileName = FileUtil.getUniqueFileName();
        final String leftIrisMapImageFileName = FileUtil.getUniqueFileName();
        final String rightIrisMapImageFileName = FileUtil.getUniqueFileName(); */
        final String userHistoryJsonFileName = FileUtil.getUniqueFileName();

        // ftp 저장 (왼쪽 홍채 이미지, 오른쪽 홍채 이미지)
        /*final File leftIrisImageForFtp = new FileWithDotForExtension(ftpPath,
                leftIrisImageFileName, BITMAP_FORMAT.toString());
        final File rightIrisImageForFtp = new FileWithDotForExtension(ftpPath,
                rightIrisImageFileName, BITMAP_FORMAT.toString());

        // ftp 저장 (왼쪽 홍채맵, 오른쪽 홍채맵)
        final File leftIrisMapImageForFtp = new FileWithDotForExtension(ftpPath,
                leftIrisMapImageFileName, BITMAP_FORMAT.toString());
        final File rightIrisMapImageForFtp = new FileWithDotForExtension(ftpPath,
                rightIrisMapImageFileName, BITMAP_FORMAT.toString());*/

        // ftp 저장 (UserHistory json)
        final File jsonFileForFtp = new FileWithDotForExtension(ftpPath, userHistoryJsonFileName, JSON_EXT);

        Runnable runnable = () -> {
            try {
                // ftp 저장 (왼쪽 홍채 이미지, 오른쪽 홍채 이미지)
                ImageUtil.saveBitmap(new File(userHistory.getLeftImage().getIrisBitmapPath()), Image.leftIrisBitmap, BITMAP_FORMAT);
                ImageUtil.saveBitmap(new File(userHistory.getRightImage().getIrisBitmapPath()), Image.rightIrisBitmap, BITMAP_FORMAT);

                // ftp 저장 (왼쪽 홍채맵, 오른쪽 홍채맵)
                ImageUtil.saveBitmap(new File(userHistory.getLeftImage().getIrisMapBitmapPath()), Image.leftIrisMapBitmap, BITMAP_FORMAT);
                ImageUtil.saveBitmap(new File(userHistory.getRightImage().getIrisMapBitmapPath()), Image.rightIrisMapBitmap, BITMAP_FORMAT);

                /*userHistory.getLeftImage().setIrisBitmapPath(leftIrisImageForFtp.getAbsolutePath());
                userHistory.getRightImage().setIrisBitmapPath(rightIrisImageForFtp.getAbsolutePath());

                userHistory.getLeftImage().setIrisMapBitmapPath(leftIrisMapImageForFtp.getAbsolutePath());
                userHistory.getRightImage().setIrisMapBitmapPath(rightIrisMapImageForFtp.getAbsolutePath());*/

                // ftp 저장 (UserHistory json)
                FileUtil.toJson(jsonFileForFtp, userHistory);

                mAppExecutors.mainThread().execute(callback::succeedToSaveIrisInfo);
            } catch (IOException e) {
                Logger.e(e, "raise Exception for save Iris info Thread");
                mAppExecutors.mainThread().execute(callback::failToSaveIrisInfo);
            }
        };

        mAppExecutors.diskIO().execute(runnable);
    }

    /**
     * managerId 로 Manager 를 얻어 와
     * GetManagerCallback 으로 반환 한다.
     * @author taein
     * @param managerId
     * @param callback
     */
    @Override
    public void getManagerBy(String managerId, GetManagerCallback callback) {
        Runnable runnable = () -> {
            Manager manager = mManagerDao.getManagerBy(managerId);
            mAppExecutors.mainThread().execute(() -> {
                if (manager != null) {
                    callback.onManagerLoaded(manager);
                } else {
                    callback.onManagerNotAvailable();
                }
            });
        };

        mAppExecutors.diskIO().execute(runnable);
    }
    @Override
    public void validateUser(User user, ValidateUserCallback callback) throws Exception {

        encryptUserId(user);

        Runnable runnable = () -> {

            User myUser = mUserDao.getUserByID(user.getManagerId(), user.getUserId());
            Log.d("jseseon", "getUserByID: " + String.valueOf(user));

            if (myUser == null) {
                mUserDao.insertUser(user);
            } else {
                mUserDao.updateUser(user);
            }

            mAppExecutors.mainThread().execute(() -> {
                if (myUser != null) {
                    callback.failToValidateUser();
                } else {
                    callback.succeedToValidateUser();
                }

            });
        };

        mAppExecutors.diskIO().execute(runnable);

    }


    public void updateUserDateByID(String analysisDate, String managerId, String userId) {
        new updateUserByIDAsyncTask(mUserDao).execute(analysisDate, managerId, userId);
    }

    private static class updateUserByIDAsyncTask extends AsyncTask<String, Void, Void> {
        private UserDao mAsyncTaskDao;

        updateUserByIDAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            mAsyncTaskDao.updateUserDateByID(strings[0], strings[1], strings[2]);
            return null;
        }
    }

    public int getUserHistoryId(String managerId, String userId, String analysisDate) {
        try {
            int a = new getUserHistoryIdAsyncTask(mUserHistoryDao).execute(managerId, userId, analysisDate).get();
            return a;
        } catch (Exception e) {
            return 0;
        }
    }

    private class getUserHistoryIdAsyncTask extends AsyncTask<String, Void, Integer> {
        private UserHistoryDao mAsyncTaskDao;

        public getUserHistoryIdAsyncTask(UserHistoryDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(String... strings) {
            Log.d("userHistory  ", " " + strings[0] + strings[1] + strings[2]);
            return mAsyncTaskDao.getUserHistoryId(strings[0], strings[1], strings[2]);
        }
    }






    private void encryptUserId(User user) throws Exception {
        Encryption encryption = Encryption.getInstance();
        user.setUserId(encryption.encryptOrDecrypt(user.getUserId(),  Cipher.ENCRYPT_MODE));
    }

    @Override
    public List<OrganPointVO> initAnalysisInformation(Image currImageDto, String side) {
        List<OrganPointVO> organPointList = initOrganPoints(currImageDto, side);
        organPointList = initOrganCenterPoint(organPointList, side);

        initIrisMapColor(organPointList, currImageDto);
        initIrisMapNumber(organPointList, side);

        return organPointList;
    }

    /**
     * 처음 화면 로딩 시 장기 영역 별 좌표 값 설정하는 함수
     * @param currImageDto
     * @param side
     */
    public List<OrganPointVO> initOrganPoints(Image currImageDto, String side) {
        int width = (int) currImageDto.getWidth();
        int length = currImageDto.getLength();

        List<OrganPointVO> organPointList = new ArrayList<OrganPointVO>();
        if (side.equals("left")) {
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.BRAIN_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.LUNGS_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.KIDENY_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.PANCREAS_ORGAN));
        } else {
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.BRAIN_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.KIDENY_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.LIVER_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.LUNGS_ORGAN));
            organPointList.add(new OrganPointVO(IrisAnalysis.Organ.PANCREAS_ORGAN));
        }

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < length; y++) {
                String organCode = organCheckAlgorithm(x, y, currImageDto);
                if (!organCode.equals("none")) {
                    for (OrganPointVO organPointVO: organPointList) {
                        if (organPointVO.getOrganCode().equals(organCode)) {
                            List<PointVO> pointList = organPointVO.getOrganPoint();
                            pointList.add(new PointVO(x, y));
                            break;
                        }
                    }
                }
            }
        }

        return organPointList;
    }

    /**
     * 위치(오른쪽/왼쪽)별로 장기 위치를 확인하는 알고리즘
     * @param x
     * @param y
     * @param currImageDto
     * @return
     */
    private String organCheckAlgorithm(int x, int y, Image currImageDto) {
        String selectOrganCode = "none";
        String side = currImageDto.getSide();
        ImageConstant imageConstant = ImageConstant.getInstance();

        int regionNum = imageConstant.getArcNum().get(side);
        double circleEqu = Math.pow(currImageDto.getCenterX() - x, 2) + Math.pow(currImageDto.getCenterY() - y, 2);  // 원의 방정식
        double theta = Math.atan2(y - currImageDto.getCenterY(), x - currImageDto.getCenterX()) * (180 / Math.PI);  // 두 점 사이의 각도
        if (theta < 0) theta = theta + 360;

        for (int i = 0; i < regionNum; i++) {
            // 췌장 영역
            if (imageConstant.getRegionName().get(side)[i].equals(IrisAnalysis.Organ.KIDENY_ORGAN)) {
                int tempPoint1 = (imageConstant.getPancreasArcPoint().get(side)[0] + currImageDto.getRotationAngle() + 360) % 360;
                int tempPoint2 = (imageConstant.getPancreasArcPoint().get(side)[0] + imageConstant.getPancreasArcPoint().get(side)[1] + currImageDto.getRotationAngle() + 360) % 360;

                if (tempPoint1 < tempPoint2) {
                    if ((tempPoint1 <= theta && theta <= tempPoint2) && (Math.pow(currImageDto.getAutonomicNerveWaveRadius(), 2) < circleEqu && Math.pow(currImageDto.getAutonomicNerveWaveRadius() + (currImageDto.getIrisRadious() - currImageDto.getAutonomicNerveWaveRadius()) / 7, 2) >= circleEqu)) {
                        selectOrganCode = imageConstant.getRegionName().get(side)[regionNum];
                        break;
                    }
                } else {
                    if ((tempPoint1 <= theta || tempPoint2 >= theta) && (Math.pow(currImageDto.getAutonomicNerveWaveRadius(), 2) < circleEqu && Math.pow(currImageDto.getAutonomicNerveWaveRadius() + (currImageDto.getIrisRadious() - currImageDto.getAutonomicNerveWaveRadius()) / 7, 2) >= circleEqu)) {
                        selectOrganCode = imageConstant.getRegionName().get(side)[regionNum];
                        break;
                    }
                }
            }

            // 췌장 영역 제외 나머지 부분
            int tempPoint1 = (imageConstant.getArcPoint().get(side)[i * 2] + currImageDto.getRotationAngle() + 360) % 360;
            int tempPoint2 = (imageConstant.getArcPoint().get(side)[i * 2] + imageConstant.getArcPoint().get(side)[i * 2 + 1] + currImageDto.getRotationAngle() + 360) % 360;

            if (tempPoint1 < tempPoint2) {
                if ((tempPoint1 <= theta && theta <= tempPoint2) && (Math.pow(currImageDto.getAutonomicNerveWaveRadius(), 2) <= circleEqu && circleEqu <= Math.pow(currImageDto.getIrisRadious(), 2))) {
                    selectOrganCode = imageConstant.getRegionName().get(side)[i];
                    break;
                }
            } else {
                if ((tempPoint1 <= theta || tempPoint2 >= theta) && (Math.pow(currImageDto.getAutonomicNerveWaveRadius(), 2) <= circleEqu && circleEqu <= Math.pow(currImageDto.getIrisRadious(), 2))) {
                    selectOrganCode = imageConstant.getRegionName().get(side)[i];
                    break;
                }
            }
        }

        return selectOrganCode;
    }

    /**
     * 각 장기 별 중심 좌표 값 구하는 함수
     * @param side
     */
    public List<OrganPointVO> initOrganCenterPoint(List<OrganPointVO> organPointList, String side) {
        for (OrganPointVO organPointVO: organPointList) {
            int minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;
            List<PointVO> pointList = organPointVO.getOrganPoint();

            for (PointVO point: pointList) {
                int y = point.getY();
                if (minY > y) minY = y;
                if (maxY < y) maxY = y;
            }

            int centerX = (pointList.get(0).getX() + pointList.get(pointList.size() - 1).getX()) / 2;
            int centerY = (minY + maxY) / 2;

            organPointVO.setOrganCenterPoint(new PointVO(centerX, centerY));
        }

        return organPointList;
    }

    /**
     * 홍채맵 장기 영역 별 색상 설정하는 함수
     * @param organPointList
     * @param currImageDto
     */
    public void initIrisMapColor(List<OrganPointVO> organPointList, Image currImageDto) {
        ImageConstant imageConstant = ImageConstant.getInstance();
        String side = currImageDto.getSide();

        Bitmap irisMapBitmap = null;
        if (side.equals("left")) irisMapBitmap = Bitmap.createScaledBitmap(Image.leftIrisMapBitmap, (int) currImageDto.getWidth(), (int) currImageDto.getLength(), true);
        else irisMapBitmap = Bitmap.createScaledBitmap(Image.rightIrisMapBitmap, (int) currImageDto.getWidth(), (int) currImageDto.getLength(), true);

        for (int i = 0; i < organPointList.size(); i++) {
            OrganPointVO organPointVO = organPointList.get(i);
            for (PointVO pointVO : organPointVO.getOrganPoint()) {
                irisMapBitmap.setPixel(pointVO.getX(), pointVO.getY(), imageConstant.getRegionColor().get(side)[i]);
            }
        }

        if (side.equals("left")) Image.leftIrisMapColorBitmap = irisMapBitmap;
        else Image.rightIrisMapColorBitmap = irisMapBitmap;
    }

    /**
     * 홍채맵 장기 영역 별 숫자 설정하는 함수
     * @param side
     */
    public void initIrisMapNumber(List<OrganPointVO> organPointList, String side) {
        Bitmap bitmap = null;

        if (side.equals("left")) bitmap = Image.leftIrisMapColorBitmap.copy(Bitmap.Config.ARGB_8888, true);
        else bitmap = Image.rightIrisMapColorBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(bitmap);

        for (int i = 0; i < organPointList.size(); i++) {
            OrganPointVO organPointVO = organPointList.get(i);
            if (organPointVO.getOrganCheck()) {
                PointVO pointVO = organPointVO.getOrganCenterPoint();
                Bitmap numberBitmap = Image.numberImage.get(i);
                canvas.drawBitmap(numberBitmap, pointVO.getX(), pointVO.getY(), null);
            }
        }

        if (side.equals("left")) Image.leftCurrIrisMapColorBitmap = bitmap;
        else Image.rightCurrIrisMapColorBitmap = bitmap;
    }

    @Override
    public String organCheckOfTouch(int x, int y, Image currImageDto, List<OrganPointVO> organPointList) {
        String organCode = organCheckAlgorithm(x, y, currImageDto);
        Bitmap irisMapBitmap = changeIrisMapColor(organCode, currImageDto.getSide(), organPointList);
        changeIrisMapNumber(irisMapBitmap, currImageDto.getSide(), organPointList);

        return organCode;
    }

    /**
     * 장기 영역 터치 시 해당 장기 영역 투명하게 처리하는 함수
     * @param organCode
     * @param side
     * @param organPointList
     * @return
     */
    private Bitmap changeIrisMapColor(String organCode, String side, List<OrganPointVO> organPointList) {
        for (OrganPointVO organPointVO: organPointList) {
            if (organPointVO.getOrganCode().equals(organCode)) {
                organPointVO.setOrganCheck(false);
                break;
            }
        }

        Bitmap bitmap = null;
        if (side.equals("left"))
            bitmap = Image.leftIrisMapColorBitmap.copy(Bitmap.Config.ARGB_8888, true);
        else
            bitmap = Image.rightIrisMapColorBitmap.copy(Bitmap.Config.ARGB_8888, true);

        for (OrganPointVO organPointVO: organPointList) {
            if (!organPointVO.getOrganCheck()) {
                for (PointVO pointVO: organPointVO.getOrganPoint())
                    bitmap.setPixel(pointVO.getX(), pointVO.getY(), Color.argb(0, 255, 255, 255));
            }
        }

        return bitmap;
    }

    /**
     * 클릭 된 홍채맵 상에서 숫자 이미지 출력하는 함수
     * @param bitmap
     * @param side
     */
    private void changeIrisMapNumber(Bitmap bitmap, String side, List<OrganPointVO> organPointList) {
        Canvas canvas = new Canvas(bitmap);

        for (int i = 0; i < organPointList.size(); i++) {
            OrganPointVO organPointVO = organPointList.get(i);
            PointVO pointVO = organPointVO.getOrganCenterPoint();
            if (organPointVO.getOrganCheck()) {
                Bitmap numberBitmap = Image.numberImage.get(i);
                canvas.drawBitmap(numberBitmap, pointVO.getX(), pointVO.getY(), null);
            } else {
                canvas.drawBitmap(Image.checkBitmap, pointVO.getX(), pointVO.getY(), null);
            }
        }

        if (side.equals("left")) {
            Image.leftCurrIrisMapColorBitmap = bitmap;
        } else {
            Image.rightCurrIrisMapColorBitmap = bitmap;
        }
    }

    @Override
    public void transferIrisInfoToRemote(String ftpPath, FTPInfoVO ftpInfoVO, IrisInfoTransferToRemoteCallback callback) {

    }
}
