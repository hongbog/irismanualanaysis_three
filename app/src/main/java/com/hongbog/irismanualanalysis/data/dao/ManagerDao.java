package com.hongbog.irismanualanalysis.data.dao;

import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.User;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface ManagerDao {

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertManager(Manager manager);

    @Delete
    void delete(Manager manager);

    @Query("DELETE from manager")
    void deleteAll();

    @Query("SELECT * FROM manager WHERE manager_id = (:managerIds)")
    Manager assertIdentity(String managerIds);

    @Query("SELECT * FROM manager WHERE manager_id = (:managerIds) AND manager_password = (:password)")
    Manager assertLogin(String managerIds, String password);

    @Query("SELECT * from manager WHERE manager_id = :managerId")
    Manager getManagerBy(String managerId);
}
