package com.hongbog.irismanualanalysis.data.dao;
import com.hongbog.irismanualanalysis.data.entity.OrganCode;


import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
@Dao
public interface OrganCodeDao {
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAllOrganCode(List<OrganCode> organCodeList);

    @Update
    void updateOrganCode(OrganCode organCode);

    @Query("DELETE FROM organ_code")
    void deleteOrganCode();
}
