package com.hongbog.irismanualanalysis.data;

import android.content.Context;
import android.os.AsyncTask;

import com.hongbog.irismanualanalysis.data.dao.ImageDao;
import com.hongbog.irismanualanalysis.data.dao.IrisAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.ManagerDao;
import com.hongbog.irismanualanalysis.data.dao.OrganAnalysisDao;
import com.hongbog.irismanualanalysis.data.dao.OrganCodeDao;
import com.hongbog.irismanualanalysis.data.dao.PatternCodeDao;
import com.hongbog.irismanualanalysis.data.dao.ScriptDao;
import com.hongbog.irismanualanalysis.data.dao.UserDao;
import com.hongbog.irismanualanalysis.data.dao.UserHistoryDao;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.data.entity.OrganCode;
import com.hongbog.irismanualanalysis.data.entity.PatternCode;
import com.hongbog.irismanualanalysis.data.entity.Script;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.util.Encryption;
import com.hongbog.irismanualanalysis.util.ExcelReader;
import com.orhanobut.logger.Logger;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;


@Database(entities = {Manager.class, User.class, UserHistory.class,
        Image.class, IrisAnalysis.class, OrganCode.class,
        OrganAnalysis.class, Script.class, PatternCode.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ManagerDao mManagerDao();
    public abstract UserDao mUserDao();
    public abstract UserHistoryDao mUserHistoryDao();
    public abstract ImageDao mImageDao();
    public abstract IrisAnalysisDao mIrisAnalysisDao();
    public abstract OrganAnalysisDao mOrganAnalysisDao();
    public abstract ScriptDao mScriptDao();
    public abstract PatternCodeDao mPatternCodeDao();
    public abstract OrganCodeDao mOrganCodeDao();

    private static AppDatabase INSTANCE;
    private static final Object sync = new Object();
    private static Context context;

    public static AppDatabase getInstance(Context context) {
        AppDatabase.context = context;

        if (INSTANCE == null) {
            synchronized (sync) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(),
                            AppDatabase.class, "manualIrisAnalysis.db")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsync(INSTANCE).execute();
        }

        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final ScriptDao scriptDao;
        private final OrganCodeDao organCodeDao;
        private final PatternCodeDao patternCodeDao;

        PopulateDbAsync(AppDatabase db) {
            this.scriptDao = db.mScriptDao();
            this.organCodeDao = db.mOrganCodeDao();
            this.patternCodeDao = db.mPatternCodeDao();
        }

        private List<OrganCode> getInitOrganCodeList() throws Exception {
            Map<String, String> organCodeMap = new HashMap<String, String>();
            organCodeMap.put("BR", "뇌");
            organCodeMap.put("KD", "신장");
            organCodeMap.put("LV", "간");
            organCodeMap.put("LG", "폐");
            organCodeMap.put("PC", "췌장");

            List<OrganCode> organCodeList = new ArrayList<OrganCode>();
            for (Map.Entry<String, String> item: organCodeMap.entrySet()) {
                OrganCode organCode = new OrganCode();
                organCode.setOrganCode(item.getKey());
                organCode.setOrganName(item.getValue());
                organCodeList.add(organCode);
            }

            return organCodeList;
        }

        private List<PatternCode> getInitPatternCodeList() throws Exception {
            Map<Integer, String> patternCodeMap = new HashMap<Integer, String>();
            patternCodeMap.put(0, "NM");
            patternCodeMap.put(1, "LA");
            patternCodeMap.put(2, "PS");
            patternCodeMap.put(3, "SP");
            patternCodeMap.put(4, "DF");

            List<PatternCode> patternCodeList = new ArrayList<PatternCode>();
            for (Map.Entry<Integer, String> item: patternCodeMap.entrySet()) {
                PatternCode patternCode = new PatternCode();
                patternCode.setPatternCode(item.getKey());
                patternCode.setPatternName(item.getValue());
                patternCodeList.add(patternCode);
            }

            return patternCodeList;
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            try {
                InputStream isJA = context.getResources().getAssets().open("script_ja.xls");
                ExcelReader excelReaderJA = ExcelReader.getInstance();

                List<Script> scriptListJA = excelReaderJA.xslAllScriptList(isJA, "ja");
                scriptDao.insertAllScript(scriptListJA);

                InputStream isKR = context.getResources().getAssets().open("script_ko.xls");
                ExcelReader excelReaderKR = ExcelReader.getInstance();

                List<Script> scriptListKR = excelReaderKR.xslAllScriptList(isKR, "ko");
                scriptDao.insertAllScript(scriptListKR);

                InputStream isUS = context.getResources().getAssets().open("script_en.xls");
                ExcelReader excelReaderUS = ExcelReader.getInstance();

                List<Script> scriptListUS = excelReaderUS.xslAllScriptList(isUS, "en");
                scriptDao.insertAllScript(scriptListUS);

                InputStream isCN = context.getResources().getAssets().open("script_zh.xls");
                ExcelReader excelReaderCN = ExcelReader.getInstance();

                List<Script> scriptListCN = excelReaderCN.xslAllScriptList(isCN, "zh");
                scriptDao.insertAllScript(scriptListCN);

                List<OrganCode> organCodeList = getInitOrganCodeList();
                organCodeDao.insertAllOrganCode(organCodeList);

                List<PatternCode> patternCodeList = getInitPatternCodeList();
                patternCodeDao.insertPatternAllCode(patternCodeList);
            } catch (Exception e) {
                Logger.e(e, e.getMessage());
            }

            return null;
        }
    }
}
