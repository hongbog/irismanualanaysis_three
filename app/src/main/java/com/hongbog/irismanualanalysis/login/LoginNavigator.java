package com.hongbog.irismanualanalysis.login;

public interface LoginNavigator {
    void openConsent();
    void openRegister();
}
