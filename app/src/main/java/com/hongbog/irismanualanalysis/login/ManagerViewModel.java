package com.hongbog.irismanualanalysis.login;

import android.app.Application;

import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;

import androidx.lifecycle.AndroidViewModel;

public class ManagerViewModel extends AndroidViewModel {
private ManualIrisAnalysisRepository mRepository;

    public ManagerViewModel(Application app) {
        super(app);
        mRepository = ManualIrisAnalysisRepository.getInstance(app);
    }

    public boolean assertIdentity(String manager_id) {
        return mRepository.assertIdentity(manager_id);
    }

    public void insert(Manager manager) {
        mRepository.insert(manager);
    }

    public boolean assertLogin(String manager_id, String password) {
        return mRepository.assertLogin(manager_id, password);
    }
}