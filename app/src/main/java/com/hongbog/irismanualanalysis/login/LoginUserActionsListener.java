package com.hongbog.irismanualanalysis.login;

public interface LoginUserActionsListener {
    void onLoginButtonClicked();
    void onRegisterButtonClicked();
}
