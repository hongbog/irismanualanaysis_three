package com.hongbog.irismanualanalysis.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.consent.ConsentActivity;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.databinding.ActivityLoginBinding;
import com.hongbog.irismanualanalysis.register.RegisterActivity;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;
import com.orhanobut.logger.Logger;

import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

public class LoginActivity extends AppCompatActivity implements LoginNavigator {

    public static final String TAG = LoginActivity.class.getSimpleName();
    public static final int REGISTER_ACTIVITY_REQUEST_CODE = 1;
    public static final String MANAGER_ID = "MANAGER_ID";
    public static final String REMEMBER_LOGIN = "REMEMBER_LOGIN";
    public static final String ID = "ID";
    public static final String IRIS_MANUAL_ANALYSIS = "REMEMBER";

    private UserLoginTask mAuthTask = null;
    private InputMethodManager imm;

    // UI references.
    private ActivityLoginBinding binding;
    private ManagerViewModel mViewModel;
    private String mId;
    private String mPassword;
    private boolean rememberLoginBol;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initParameter();

        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_login);
        final LoginUserActionsListener listener = getLoginUserActionsListener();
        binding.setListener(listener);
        binding.setLifecycleOwner(this);

        /* Created by 조원태 2019-04-24 오후 2:23
         * #Description: 키보드 자판에 대한 관리 변수.
         */
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        binding.rootLayout.setOnClickListener(mOnClickListener);
        binding.loginLayout.setOnClickListener(mOnClickListener);
        mViewModel = obtainViewModel(this);

        mSharedPreferences = getSharedPreferences(IRIS_MANUAL_ANALYSIS, MODE_PRIVATE);
        loadLogin();

        /* Created by 조원태 2019-04-24 오후 2:37
         * #Description: 로그인 시 아이디 입력할 때 영문과 숫자만 입력 가능한 필터 설정(한글 x).
         */
        binding.id.setFilters(new InputFilter[]{alphanum});

        /* Created by 조원태 2019-04-24 오후 2:21
         * #Description: 로그인 시 비밀번호(EditText) 입력 후 키보드 자판에서 확인 버튼 누르면
         *               자동으로 자판이 화면 내에서 사라진다.
         */
        binding.password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    listener.onLoginButtonClicked();
                    return true;
                }
                return false;
            }
        });

        /* Created by 조원태 2019-04-24 오후 2:38
         * #Description: rememberLoginBol(로그인 정보 기억에 대한 boolean 변수) 가 참일 경우,
         *               다음 로그인 시 아이디가 자동으로 설정된다.
         */
        if (rememberLoginBol) {
            binding.id.setText(mId);
            binding.rememberChbox.setChecked(rememberLoginBol);
        }

        /* Created by 조원태 2019-04-29 오후 7:55
         * #Description: 로그인 시, 로그인 정보 기억이 없으면 아이디의 EditText에 focus를 둔다.
         *               그렇지 않으면, 비밀번호의 EditText에 focus를 둔다.
         */
        if (binding.id.getText().toString().isEmpty() || binding.id.getText().toString().equals("")) {
            binding.id.requestFocus();
        } else {
            binding.password.requestFocus();
        }
    }

    private void initParameter() {
        Image.numberImage.put(0, ((BitmapDrawable) getResources().getDrawable(R.drawable.number1, null)).getBitmap());
        Image.numberImage.put(1, ((BitmapDrawable) getResources().getDrawable(R.drawable.number2, null)).getBitmap());
        Image.numberImage.put(2, ((BitmapDrawable) getResources().getDrawable(R.drawable.number3, null)).getBitmap());
        Image.numberImage.put(3, ((BitmapDrawable) getResources().getDrawable(R.drawable.number4, null)).getBitmap());
        Image.numberImage.put(4, ((BitmapDrawable) getResources().getDrawable(R.drawable.number5, null)).getBitmap());
        Image.checkBitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.check, null)).getBitmap();

        Image.colorBrain = ContextCompat.getColor(this, R.color.colorBrain);
        Image.colorKidney = ContextCompat.getColor(this, R.color.colorKidney);
        Image.colorLiver = ContextCompat.getColor(this, R.color.colorLiver);
        Image.colorLungs = ContextCompat.getColor(this, R.color.colorLungs);
        Image.colorPancreas = ContextCompat.getColor(this, R.color.colorPancreas);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /* Created by 조원태 2019-04-24 오후 2:40
         * #Description: 회원가입 완료 시 로그인 창에서 "회원가입 완료" 메세지를 뜨게 해준다.
         */
        if (requestCode == REGISTER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            showSnackBar(R.string.register_success);
        }
    }

    public static ManagerViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(ManagerViewModel.class);
    }

    /* Created by 조원태 2019-04-24 오후 2:24
     * #Description: 해당 리스너를 쓰는 레이아웃은 백버튼을 안 눌리고 화면을 눌려도 키보드가
     *               자동으로 사라지게 한다.
     */
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    };

    /**
     * #Method: loadLogin
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:41
     * #Description: 로그인 창이 뜨면 이전의 로그인 정보 세팅해주는 메소드.
     **/
    private void loadLogin() {
        rememberLoginBol = mSharedPreferences.getBoolean(REMEMBER_LOGIN, false);
        mId = mSharedPreferences.getString(ID, "");
    }

    /* Created by 조원태 2019-04-24 오후 2:43
     * #Description: 영문과 숫자만 필터링해주는 변수(한글 x).
     */
    public InputFilter alphanum = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Pattern ps = Pattern.compile("^[a-zA-Z0-9]*$");
            if (!ps.matcher(source).matches()) {
                return "";
            }
            return null;
        }
    };

    /**
     * #Method: isIdValid
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:44
     * #Description: 로그인 시 아이디 validate 메소드.
     **/
    private boolean isIdValid(String id) {
        return id.length() >= 4;
    }

    /**
     * #Method: isPasswordValid
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:44
     * #Description: 로그인 시 비밀번호 validate 메소드.
     **/
    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

    /**
     * #Method: showSnackBar
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:45
     * #Description: 필요에 따라 UI 상에서 Custom한 스낵 바를 보여주는 메소드.
     **/
    private void showSnackBar(int resId) {
        Snackbar mSnackbar = Snackbar.make(binding.rootLayout, resId, Snackbar.LENGTH_LONG);
        View view = mSnackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP | Gravity.CENTER;
        view.setLayoutParams(params);
        TextView snackTextView = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
        snackTextView.setTextColor(getResources().getColor(R.color.colorTitle));
        snackTextView.setTypeface(snackTextView.getTypeface(), Typeface.BOLD);
        snackTextView.setGravity(Gravity.CENTER);
        snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        mSnackbar.show();
    }

    /**
     * #Method: showProgress
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:45
     * #Description: 로그인 버튼 클릭 시 ProgressBar를 보여주는 메소드.
     **/
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            binding.loginScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
            binding.loginScrollView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    binding.loginScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            binding.loginProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            binding.loginProgress.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    binding.loginProgress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            binding.loginProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            binding.loginScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * #Method: openConsent
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:46
     * #Description: LoginNavigator 인터페이스의 openPatientInfo()를 Override한 메소드.
     *               로그인 성공 시, 로그인 정보를 기억하고 UserInfoActivity로 이동한다.
     **/
    @Override
    public void openConsent() {
        final String managerId = binding.id.getText().toString().trim();

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(REMEMBER_LOGIN, binding.rememberChbox.isChecked());
        editor.putString(ID, managerId);
        editor.apply();

        Intent intent = new Intent(this, ConsentActivity.class);
        intent.putExtra(MANAGER_ID, managerId);
        startActivity(intent);
        finish();
    }

    /**
     * #Method: openRegister
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 3:02
     * #Description: LoginNavigator 인터페이스의 openRegister()를 Override한 메소드.
     *               로그인 창에서 회원가입 버튼 클릭 시 RegisterActivity로 이동한다.
     **/
    @Override
    public void openRegister() {
        Intent intent =new Intent(LoginActivity.this, RegisterActivity.class);
        startActivityForResult(intent, REGISTER_ACTIVITY_REQUEST_CODE);
    }

    /**
     * #Method: UserLoginTask
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 3:04
     * #Description: AsyncTask를 이용하여 로그인 창에서 아이디와 비밀번호를 입력 받아와서
     *               DB와 연동하여 아이디와 비밀번호 매칭을 실시한다.
     **/
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mId;
        private final String mPassword;

        UserLoginTask(String id, String password) {
            mId = id;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
            return mViewModel.assertLogin(mId, mPassword);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                openConsent();
            } else {
                showSnackBar(R.string.login_error_msg);
                binding.password.setText("");
                binding.id.setText("");
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    /**
     * #Method: getLoginUserActionsListener
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 3:05
     * #Description: LoginUserActionsListener 인터페이스를 이용한 리스너 메소드.
     *               onLoginButtonClicked() -> 로그인 버튼 클릭 시 수행 메소드.
     *               onRegisterButtonClicked() -> 회원가입 버튼 클릭 시 수행 메소드.
     **/
    private LoginUserActionsListener getLoginUserActionsListener() {
        return new LoginUserActionsListener() {
            @Override
            public void onLoginButtonClicked() {
                if (mAuthTask != null) { return; }

                mId = binding.id.getText().toString().trim();
                mPassword = binding.password.getText().toString().trim();

                boolean cancel = false;
                View focusView = null;

                if (TextUtils.isEmpty(mPassword) || !isPasswordValid(mPassword)) {
                    showSnackBar(R.string.password_error_msg);
                    focusView = binding.password;
                    cancel = true;
                }

                if (TextUtils.isEmpty(mId)) {
                    showSnackBar(R.string.id_error_msg_empty);
                    focusView = binding.id;
                    cancel = true;
                } else if (!isIdValid(mId)) {
                    showSnackBar(R.string.id_error_msg);
                    focusView = binding.id;
                    cancel = true;
                }

                if (cancel) {
                    focusView.requestFocus();
                } else {
                    focusView = binding.rootLayout;
                    imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
                    showProgress(true);
                    mAuthTask = new UserLoginTask(mId, mPassword);
                    mAuthTask.execute((Void) null);
                }
            }

            @Override
            public void onRegisterButtonClicked() {
                openRegister();
            }
        };
    }
}

