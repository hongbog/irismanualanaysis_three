package com.hongbog.irismanualanalysis;

import android.app.Application;
import com.facebook.stetho.Stetho;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * Application을 상속한 class로 App 최초 로딩시 초기화 작업을 수행 한다.
 * @author taein
 * @date 2019-03-07 오후 5:58
 **/
public class BasicApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }

        // Default Logger TAG : PRETTY_LOGGER
        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
    }
}
