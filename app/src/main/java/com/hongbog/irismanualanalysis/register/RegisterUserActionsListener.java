package com.hongbog.irismanualanalysis.register;

public interface RegisterUserActionsListener {
    void onRegisterButtonClicked();
    void onBackButtonClicked();
    void onConfirmButtonClicked();
}
