package com.hongbog.irismanualanalysis.register;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.entity.Manager;
import com.hongbog.irismanualanalysis.databinding.ActivityRegisterBinding;
import com.hongbog.irismanualanalysis.login.ManagerViewModel;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

public class RegisterActivity extends AppCompatActivity implements RegisterNavigator {

    public static final String TAG = RegisterActivity.class.getSimpleName();
    public static final String REGISTER_SUCCESS = "registerSuccess";

    /* Created by 조원태 2019-04-24 오후 2:29
     * #Description: 아이디 중복체크에 대한 validate 변수 (Default: false).
     */
    public static boolean firstIdConfirm = false;

    private ActivityRegisterBinding binding;
    private ManagerViewModel mViewModel;
    private InputMethodManager imm;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        final RegisterUserActionsListener listener = getRegisterUserActionsListener();
        binding.setListener(listener);
        binding.setLifecycleOwner(this);

        /* Created by 조원태 2019-04-24 오후 2:23
         * #Description: 키보드 자판에 대한 관리 변수.
         */
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        binding.rootLayout.setOnClickListener(mOnClickListener);
        binding.registerLayout.setOnClickListener(mOnClickListener);
        mViewModel = obtainViewModel(this);

        /* Created by 조원태 2019-04-24 오후 2:21
         * #Description: 회원가입 시 기관(EditText) 입력 후 키보드 자판에서 확인 버튼 누르면
         *               자동으로 자판이 화면 내에서 사라진다.
         */
        binding.registerAgency.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    public static ManagerViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(ManagerViewModel.class);
    }

    /* Created by 조원태 2019-04-24 오후 2:24
     * #Description: 해당 리스너를 쓰는 레이아웃은 백버튼을 안 눌리고 화면을 눌려도 키보드가
     *               자동으로 사라지게 한다.
     */
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    };

    /**
     * #Method: validateId
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:26
     * #Description: 회원가입 시 아이디에 관한 유효성을 검사한다.
     **/
    private boolean validateId() {
        String idInput = binding.registerId.getText().toString().trim();
        if (idInput.isEmpty() | idInput.length() < 4) {
            binding.registerId.setError(getResources().getText(R.string.id_error_msg));
            binding.registerId.requestFocus();
            return false;
        } else {
            binding.registerId.setError(null);
            return true;
        }
    }

    /**
     * #Method: validateName
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:26
     * #Description: 회원가입 시 이름에 관한 유효성을 검사한다.
     **/
    private boolean validateName() {
        String nameInput = binding.registerName.getText().toString().trim();
        if (nameInput.isEmpty() | nameInput.length() <=1) {
            binding.registerName.setError(getResources().getText(R.string.name_error_msg_short));
            binding.registerId.requestFocus();
            return false;
        } else if (nameInput.length() > 10) {
            binding.registerName.setError(getResources().getText(R.string.name_error_msg_long));
            binding.registerId.requestFocus();
            return false;
        } else {
            binding.registerName.setError(null);
            return true;
        }
    }

    /**
     * #Method: validateAgency
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:27
     * #Description: 회원가입 시 기관에 관한 유효성을 검사한다.
     **/
    private boolean validateAgency() {
        String agencyInput = binding.registerAgency.getText().toString().trim();
        if (agencyInput.isEmpty()) {
            binding.registerAgency.setError(getResources().getText(R.string.agency_error_msg));
            return false;
        } else {
            binding.registerAgency.setError(null);
            return true;
        }
    }

    /**
     * #Method: validatePassword
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:27
     * #Description: 회원가입 시 비밀번호에 관한 유효성을 검사한다.
     **/
    private boolean validatePassword() {
        String passwordInput = binding.registerPassword.getText().toString().trim();
        if (passwordInput.isEmpty() | passwordInput.length() < 6) {
            binding.registerPassword.setError(getResources().getText(R.string.password_error_msg));
            binding.registerPassword.requestFocus();
            binding.registerPassword.setText("");
            binding.registerPasswordCopy.setText("");
            return false;
        } else {
            binding.registerPassword.setError(null);
            return true;
        }
    }

    /**
     * #Method: validateConfirmPassword
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:27
     * #Description: 회원가입 시 비밀번호와 비밀번호 확인이 일치하는 지 검사한다.
     **/
    private boolean validateConfirmPassword() {
        String passwordInput = binding.registerPassword.getText().toString().trim();
        String confirmPassword = binding.registerPasswordCopy.getText().toString().trim();
        if (passwordInput.equals(confirmPassword)) {
            return true;
        } else {
            binding.registerPasswordCopy.setError(getResources().getString(R.string.password_confirm_error_msg));
            binding.registerPasswordCopy.requestFocus();
            binding.registerPasswordCopy.setText("");
            binding.registerPassword.setText("");
            return false;
        }
    }

    /**
     * #Method: confimInput
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:28
     * #Description: validateConfirmPassword 메소드를 제외한 모든 유효성 검사가 일치할 시
     *              true를 리턴한다. 그렇지 않으면 false 리턴.
     **/
    public boolean confirmInput() {
        return (validateAgency() && validateId() && validateName() && validatePassword() && validateConfirmPassword());
    }

    /**
     * #Method: showSnackBar
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:45
     * #Description: 필요에 따라 UI 상에서 Custom한 스낵 바를 보여주는 메소드.
     **/
    private void showSnackBar() {
        Snackbar mSnackbar = Snackbar.make(binding.rootLayout, R.string.id_confirm_msg, Snackbar.LENGTH_LONG);
        View view = mSnackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP | Gravity.CENTER;
        view.setLayoutParams(params);
        TextView snackTextView = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
        snackTextView.setTextColor(getResources().getColor(R.color.colorTitle));
        snackTextView.setTypeface(snackTextView.getTypeface(), Typeface.BOLD);
        snackTextView.setGravity(Gravity.CENTER);
        snackTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        mSnackbar.show();
    }

    /**
     * #Method: successRegister
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:30
     * #Description: RegisterNavigator 인터페이스의 successRegister()를 Override한 메소드.
     *               회원가입 창에서 회원가입 버튼을 클릭 시 수행되는 메소드.
     **/
    @Override
    public void successRegister() {
        Intent replyIntent = new Intent();
        if (confirmInput() && firstIdConfirm) {
            long now = System.currentTimeMillis();
            Date date = new Date(now);
            SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMdd HHmmss", Locale.KOREA);
            String formatDate  = sdfNow.format(date);

            Manager newManager = new Manager(
                    binding.registerId.getText().toString().trim(),
                    binding.registerPassword.getText().toString().trim(),
                    binding.registerName.getText().toString().trim(),
                    binding.registerAgency.getText().toString().trim(),
                    formatDate, null);
            mViewModel.insert(newManager);

            replyIntent.putExtra(REGISTER_SUCCESS, true);
            setResult(RESULT_OK, replyIntent);
            finish();
        } else if (confirmInput() && !firstIdConfirm){
            binding.registerId.requestFocus();
            binding.registerId.setError(getResources().getString(R.string.id_not_confirm_click_msg));
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            setResult(RESULT_CANCELED, replyIntent);
        }
    }

    /**
     * #Method: returnBack
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:34
     * #Description: RegisterNavigator 인터페이스의 returnBack()을 Override한 메소드.
     *               회원가입 창에서 돌아가기 버튼을 클릭 시 수행되는 메소드.
     **/
    @Override
    public void returnBack() {
        finish();
    }

    /**
     * #Method: getRegisterUserActionsListener
     * #Author: 조원태
     * #Version: 1.0
     * #date: 2019-04-24 오후 2:34
     * #Description: RegisterUserActionsListener 인터페이스를 이용한 리스너 메소드.
     *               OnRegisterButtonClicked() -> 회원가입 버튼 클릭 시 수행 메소드.
     *               onBackButtonClicked() -> 돌아가기 버튼 클릭 시 수행 메소드.
     *               onConfirmButtonClicked() -> 아이디 중복체크 버튼 클릭 시 수행 메소드.
     **/
    private RegisterUserActionsListener getRegisterUserActionsListener() {
        return new RegisterUserActionsListener() {
            @Override
            public void onRegisterButtonClicked() {
                successRegister();
            }

            @Override
            public void onBackButtonClicked() {
                returnBack();
            }

            @Override
            public void onConfirmButtonClicked() {
                String assertId = binding.registerId.getText().toString().trim();
                if (assertId.isEmpty() | assertId.length() < 4) {
                    binding.registerId.setError(getResources().getText(R.string.id_error_msg));
                    binding.registerId.requestFocus();
                    return;
                }

                if (mViewModel.assertIdentity(binding.registerId.getText().toString().trim())) {
                    showSnackBar();
                    firstIdConfirm = true;

                } else {
                    binding.registerId.setError(getResources().getString(R.string.id_confirm_error_msg));
                    binding.registerId.requestFocus();
                }
            }
        };
    }
}
