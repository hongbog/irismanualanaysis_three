package com.hongbog.irismanualanalysis.register;

public interface RegisterNavigator {
    void successRegister();
    void returnBack();
}
