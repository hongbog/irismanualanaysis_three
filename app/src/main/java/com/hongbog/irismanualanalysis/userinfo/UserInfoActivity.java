package com.hongbog.irismanualanalysis.userinfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.analysis.AnalysisActivity;
import com.hongbog.irismanualanalysis.camera.CameraActivity;
import com.hongbog.irismanualanalysis.camera.CameraForEmualator;
import com.hongbog.irismanualanalysis.capture.CaptureActivity;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.databinding.ActivityUserInfoBinding;
import com.hongbog.irismanualanalysis.login.LoginActivity;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.Encryption;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;
import com.orhanobut.logger.Logger;

import javax.crypto.Cipher;

/**
 * 메인 화면으로 환자 정보를 입력 받는다
 *
 * @author taein
 * @date 2019-03-07 오후 6:00
 **/
public class UserInfoActivity extends AppCompatActivity implements UserInfoNavigator {

    public static final String TAG = UserInfoActivity.class.getSimpleName();
    private static final String EMULATOR_DEVICE_MODEL = "Android SDK built for x86";
    private static final String EMULATOR_DEVICE_MODEL2 = "sdk_google_phone_armv7";
    public static UserInfoActivity userInfoActivity;
    private static final int REQUEST_CAMERA = 1;
    public static final String IRIS_IMAGE = "IRIS_IMAGE";
    public static final String IRIS_LEFT = "left";
    public static final String IRIS_RIGHT = "right";
    private UserInfoViewModel mViewModel;
    private final long FINISH_INTERVAL_TIME = 2000L;
    private long backPressedTime = 0;
    private InputMethodManager imm;
    private ActivityUserInfoBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userInfoActivity = UserInfoActivity.this;
        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_info);
        mViewModel = obtainViewModel(this);

        /* Created by 조원태 2019-04-24 오후 2:23
         * #Description: 키보드 자판에 대한 관리 변수.
         */
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        binding.userInfoTitleLayout.setOnClickListener(mOnClickListener);
        binding.userInfoContentLayout.setOnClickListener(mOnClickListener);
        binding.irisCaptureLayout.setOnClickListener(mOnClickListener);
        binding.toolBar.setOnClickListener(mOnClickListener);
        binding.userEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    if (mViewModel.isEmailValid(binding.userEmail.getText().toString().trim())) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        binding.userEmail.setError(null);
                        binding.userEmail.clearFocus();
                    } else {
                        binding.userEmail.setError(getResources().getString(R.string.user_email_error));
                        binding.userEmail.requestFocus();
                    }
                    return true;
                }
                return false;
            }
        });

        User user = User.newInstance(getIntent().getStringExtra(LoginActivity.MANAGER_ID),
                getString(R.string.man), getString(R.string.women));
        mViewModel.start(user);

        final UserInfoActionsListener listener = getUserInfoUserActionsListener();
        binding.setUser(user);
        binding.setListener(listener);
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(this);

        setupViewModelObserve();
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            if (mViewModel.isEmailValid(binding.userEmail.getText().toString().trim())) {
                binding.userEmail.setError(null);
                binding.userEmail.clearFocus();
            } else {
                binding.userEmail.setError(getResources().getString(R.string.user_email_error));
                binding.userEmail.clearFocus();
            }

        }
    };

    @NonNull
    public static UserInfoViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(UserInfoViewModel.class);
    }

    private void setupViewModelObserve() {
        this.mViewModel.getOpenAnalysisEvent().observe(this, (user -> {

            if (user != null) {
                openAnalysis(user);
            } else {
                Toast.makeText(this, R.string.validator_error
                        , Toast.LENGTH_SHORT).show();
            }
        }));

        this.mViewModel.getOpenCameraEvent().observe(this, (irisImageEvent -> {
            final Image cameraPosition = irisImageEvent.getContentIfNotHandled();
            if (cameraPosition != null) {
                openCamera(cameraPosition);
            }
        }));
    }

    @Override
    public void openCamera(Image irisImage) {
        if (TextUtils.equals(android.os.Build.MODEL, EMULATOR_DEVICE_MODEL) ||
                TextUtils.equals(Build.MODEL, EMULATOR_DEVICE_MODEL2)) {
            Intent intent = new Intent(getApplicationContext(), CameraForEmualator.class);
            intent.putExtra(IRIS_IMAGE, irisImage);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
            intent.putExtra(IRIS_IMAGE, irisImage);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }
    @Override
    public void openAnalysis(User user) {
        user.getUserHistory().setUserId(user.getUserId());

        try {
            mViewModel.validateUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Intent intent = new Intent(this, AnalysisActivity.class);
        intent.putExtra(AnalysisActivity.USER_HISTORY, user.getUserHistory());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == CaptureActivity.CAPTURE_RESULT_OK) {
                if (data == null) return;

                final Image irisImage = data.getParcelableExtra(CaptureActivity.IRIS_IMAGE);
                if (irisImage != null) {
                    if (TextUtils.equals(irisImage.getSide(), IRIS_LEFT)) {
                        mViewModel.setLeftIrisImage(irisImage);
                    } else {
                        mViewModel.setRightIrisImage(irisImage);
                    }
                }
            }
        }
    }

    private UserInfoActionsListener getUserInfoUserActionsListener() {
        return new UserInfoActionsListener() {
            @Override
            public void onLeftCameraClicked() {
                Image irisImage = new Image();
                irisImage.setSide(IRIS_LEFT);
                mViewModel.setLeftIrisImage(irisImage);
                mViewModel.openCamera(irisImage);
            }

            @Override
            public void onRightCameraClicked() {
                Image irisImage = new Image();
                irisImage.setSide(IRIS_RIGHT);
                mViewModel.setRightIrisImage(irisImage);
                mViewModel.openCamera(irisImage);
            }

            @Override
            public void onAnalysisClicked(User user) {
                mViewModel.openAnalysis(user);
            }

            @Override
            public void showNumberPicker(View view) {
                NumberPickerDialog newFragment = new NumberPickerDialog();
                newFragment.setFocusView(view.getId());

                if (view.getId() == R.id.age_txt) {
                    newFragment.setMinValue(10);
                    newFragment.setMaxValue(90);
                    newFragment.setMsg(
                            getResources().getString(R.string.age) + "(" +
                            getResources().getString(R.string.age_unit) + ") " +
                                    getResources().getString(R.string.selection));

                } else if (view.getId() == R.id.weight_txt) {
                    newFragment.setMinValue(0);
                    newFragment.setMaxValue(110);
                    newFragment.setMsg(getResources().getString(R.string.weight) + "(" +
                            getResources().getString(R.string.weight_unit) + ") " +
                            getResources().getString(R.string.selection));

                } else if (view.getId() == R.id.tall_txt) {
                    newFragment.setMinValue(0);
                    newFragment.setMaxValue(100);
                    newFragment.setMsg(getResources().getString(R.string.tall) + "(" +
                            getResources().getString(R.string.tall_unit) + ") " +
                            getResources().getString(R.string.selection));

                } else if (view.getId() == R.id.sex_txt) {
                    newFragment.setMinValue(0);
                    newFragment.setMaxValue(1);
                    newFragment.setMsg(getResources().getString(R.string.sex) + " " +
                            getResources().getString(R.string.selection));
                }

                newFragment.setValueChangeListener(this);
                newFragment.show(getSupportFragmentManager(), "numberPicker");
            }

            @Override
            public void onValueChange(NumberPicker numberPicker, int id, int value) {
                TextView focusView = findViewById(id);

                if (focusView.getId() == R.id.age_txt) {
                    String format = Integer.toString(value) + " " +
                            getResources().getString(R.string.age_unit);
                    focusView.setText(format);

                } else if (focusView.getId() == R.id.weight_txt) {
                    String format = numberPicker.getDisplayedValues()[value] + " " +
                            getResources().getString(R.string.weight_unit);
                    focusView.setText(format);

                } else if (focusView.getId() == R.id.tall_txt) {
                    String format = numberPicker.getDisplayedValues()[value] + " " +
                            getResources().getString(R.string.tall_unit);
                    focusView.setText(format);

                } else {
                    String format = numberPicker.getDisplayedValues()[value];
                    focusView.setText(format);
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        long tmpTime = System.currentTimeMillis();
        long intervalTime = tmpTime - backPressedTime;
        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
            removeImageBitmap();
            super.onBackPressed();
            finishAffinity();
        } else {
            backPressedTime = tmpTime;
            Toast.makeText(
                    getApplicationContext(), R.string.double_back_btn, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Image 에 static Bitmap 메모리를 정리 한다.
     * @author taein
     */
    private void removeImageBitmap() {
        Image.removeStaticVariable();
    }
}