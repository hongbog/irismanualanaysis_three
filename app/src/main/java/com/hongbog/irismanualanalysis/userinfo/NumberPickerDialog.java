package com.hongbog.irismanualanalysis.userinfo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.NumberPicker;
import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.util.UnitUtil;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class NumberPickerDialog extends DialogFragment {
    private NumberPicker.OnValueChangeListener valueChangeListener;
    private String sysLanguage;
    private NumberPicker numberPicker;
    private String msg;
    private int minValue;
    private int maxValue;
    private int focusView;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Locale sysLocale = getActivity().getApplicationContext().getResources().getConfiguration().locale;
        sysLanguage = sysLocale.getLanguage();

        final String sex[] = {this.getContext().getString(R.string.man), this.getContext().getString(R.string.women)};
        numberPicker = new NumberPicker(getActivity());
        setDividerColor(numberPicker, getResources().getColor(R.color.colorBasic));
        numberPicker.setMinValue(getMinValue());
        numberPicker.setMaxValue(getMaxValue());
        if (numberPicker.getMaxValue() < 2) {
            numberPicker.setDisplayedValues(sex);
            numberPicker.setValue(0);
        } else if (numberPicker.getMaxValue() == 90){
            numberPicker.setValue(numberPicker.getMaxValue() - 50);
        } else {
            setNumberPickerValueList(numberPicker);
        }
        // 숫자 입력 방지.
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getMsg());

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                valueChangeListener.onValueChange(numberPicker,
                        getFocusView(), numberPicker.getValue());
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setView(numberPicker);
        return builder.create();
    }

    public NumberPicker.OnValueChangeListener getValueChangeListener() {
        return valueChangeListener;
    }

    public void setValueChangeListener(NumberPicker.OnValueChangeListener valueChangeListener) {
        this.valueChangeListener = valueChangeListener;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getFocusView() {
        return focusView;
    }

    public void setFocusView(int focusView) {
        this.focusView = focusView;
    }

    public void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void setNumberPickerValueList(NumberPicker numberPicker) {
        String[] selection = new String[getMaxValue() + 1];
        // 키
        if (numberPicker.getMaxValue() == 100) {
            for (int i = 0; i < selection.length; i++) {
//                if (sysLanguage.equals("en")) {
//                    selection[i] = UnitUtil.converHeightUnit(100 + i);
//                }  else {
                    selection[i] = Integer.toString(100 + i);
//                }
            }
            // 몸무게
        } else if (numberPicker.getMaxValue() == 110) {
            for (int i = 0; i < selection.length; i++) {
//                if (sysLanguage.equals("en")){
//                    selection[i] = UnitUtil.convertWeightUnit(20 + i);
//                } else {
                    selection[i] = Integer.toString(20 + i);
//                }
            }
        }
        numberPicker.setDisplayedValues(selection);
        numberPicker.setValue(numberPicker.getMaxValue() / 2);
    }

}
