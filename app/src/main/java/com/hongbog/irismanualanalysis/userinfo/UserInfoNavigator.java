package com.hongbog.irismanualanalysis.userinfo;

import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.User;

// PatientInfo 화면에 Navigator interface
public interface UserInfoNavigator {
    void openAnalysis(User irisAnalysisInfo);
    void openCamera(Image irisImage);
}
