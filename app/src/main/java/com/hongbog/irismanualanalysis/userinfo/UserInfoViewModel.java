package com.hongbog.irismanualanalysis.userinfo;

import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.hongbog.irismanualanalysis.consent.ConsentActivity;
import com.hongbog.irismanualanalysis.data.dataSource.ManualIrisAnalysisDataSource;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.hongbog.irismanualanalysis.util.Event;
import com.orhanobut.logger.Logger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * PatientInfo 화면에 ViewModel
 *
 * @author taein
 * @date 2019-03-21 오후 2:08
 **/
public class UserInfoViewModel extends AndroidViewModel implements ManualIrisAnalysisDataSource.ValidateUserCallback{

    private MutableLiveData<User> openAnalysisEvent = new MutableLiveData<>();
    private MutableLiveData<Event<Image>> openCameraEvent = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mLeftIrisImageBitmap = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mRightIrisImageBitmap = new MutableLiveData<>();
    private User mUser;
    private final ManualIrisAnalysisRepository mRepository;

    public UserInfoViewModel(@NonNull Application application) {
        super(application);
        mRepository = ManualIrisAnalysisRepository.getInstance(application);
    }

    public LiveData<Bitmap> getLeftIrisImageBitmap() {
        return mLeftIrisImageBitmap;
    }
    public LiveData<Bitmap> getRightIrisImageBitmap() {
        return mRightIrisImageBitmap;
    }
    public LiveData<User> getOpenAnalysisEvent() {
        return openAnalysisEvent;
    }
    public LiveData<Event<Image>> getOpenCameraEvent() {
        return openCameraEvent;
    }

    @Nullable
    public User getUser() {
        return mUser;
    }

    public void start(User user) {
        mUser = user;
    }

    /**
     * 왼쪽 홍채 이미지를 User 에 넣어 준다.
     * @param irisImage
     * @author taein
     */
    public void setLeftIrisImage(@Nullable Image irisImage) {
        if (irisImage != null) {
            mUser.getUserHistory().setLeftImage(irisImage);
            mLeftIrisImageBitmap.setValue(Image.leftIrisBitmap);
        }
    }

    /**
     * 오른쪽 홍채 이미지를 User 에 넣어 준다.
     * @param irisImage
     * @author taein
     */
    public void setRightIrisImage(@Nullable Image irisImage) {
        if (irisImage != null) {
            mUser.getUserHistory().setRightImage(irisImage);
            mRightIrisImageBitmap.setValue(Image.rightIrisBitmap);
        }
    }

    /**
     * Analysis 화면으로 이동 한다.
     * @param user
     * @author taein
     */
    public void openAnalysis(User user) {
        if (!user.isEmpty()) {
            user.getUserHistory().setUserId(user.getUserId());
            this.openAnalysisEvent.setValue(user);
        } else {
            this.openAnalysisEvent.setValue(null);
        }
    }

    /**
     * Camera 화면으로 이동 한다.
     * @param irisImage
     * @author taein
     */
    public void openCamera(Image irisImage) {
        this.openCameraEvent.setValue(new Event(irisImage));
    }

    /** user 존재여부 및 입력 로직 **/

    public boolean isEmailValid(String userId) {
        return Patterns.EMAIL_ADDRESS.matcher(userId).matches();
    }

    /**
     * EditText로 받아온 UserID를 User에 저장후, UserID(Email)가 존재하는 지 확인.
     * 존재하면 user정보 update 하고, 만약 존재하지 않으면 user insert.
     * @param user
     * @throws Exception
     * @Author jslee
     */
    public void validateUser(User user) throws Exception {
        mRepository.validateUser(user, this);
    }
    @Override
    public void succeedToValidateUser() {
        Log.d("LJS", "succeedToValidateUser");
    }
    @Override
    public void failToValidateUser() {
        Log.d("LJS", "failToValidateUser");
    }



}