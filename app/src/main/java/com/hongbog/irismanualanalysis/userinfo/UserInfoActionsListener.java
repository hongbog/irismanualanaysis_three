package com.hongbog.irismanualanalysis.userinfo;

import android.view.View;
import android.widget.NumberPicker;

import com.hongbog.irismanualanalysis.data.entity.User;

// PatientInfo 화면에 user Action을 정의 한다.
public interface UserInfoActionsListener extends NumberPicker.OnValueChangeListener {
    void onAnalysisClicked(User user);
    void showNumberPicker(View view);
    void onLeftCameraClicked();
    void onRightCameraClicked();
}
