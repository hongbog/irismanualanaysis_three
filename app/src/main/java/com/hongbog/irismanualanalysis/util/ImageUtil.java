package com.hongbog.irismanualanalysis.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Environment;

import androidx.annotation.Keep;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Image를 다루는 Util 클래스
 *
 * @author taein
 * @date 2019-03-12 오전 10:21
 **/
public class ImageUtil {
    private static final String TAG = ImageUtil.class.getSimpleName();

    private ImageUtil() {
        throw new AssertionError();
    }

    /**
     * 해당 File 에 디렉토리에 해당 compressFormat 확장자로
     * Bitmap 을 저장 한다. 만약 부모 디렉토리가 존재 하지 않다면 생성 한다.
     * @param file
     * @param bitmap
     * @param compressFormat
     * @return
     * @throws IOException
     * @author taein
     */
    public static boolean saveBitmap(final File file, @Nullable Bitmap bitmap,
                                     Bitmap.CompressFormat compressFormat) throws IOException {
        if (bitmap == null) return false;

        file.getParentFile().mkdirs();

        try (final OutputStream out = new FileOutputStream(file)) {
            return bitmap.compress(compressFormat, 100, out);
        }
    }

    /**
     * Saves a Bitmap object to disk for analysis.
     *
     * @param dirName directory Name
     * @return extract Bitmap[] in dirName
     */
    public static ArrayList<Bitmap> extractBitmap(final String dirName) {
        final String root =
                Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "dlib" + File.separator + dirName;

        if (root == null) return null;

        final File myDir = new File(root);

        if (!myDir.exists()) return null;

        ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();

        for (File file : myDir.listFiles()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            bitmapArrayList.add(bitmap);
        }

        return bitmapArrayList;
    }

    /**
     * Saves a Bitmap object to disk for analysis.
     *
     * @param filePath full file path
     * @return extract Bitmap from path
     */
    public static Bitmap extractBitmapFromDirName(String filePath) {
        try {
            FileInputStream is = new FileInputStream(filePath);
            Bitmap bmp = BitmapFactory.decodeStream(is);
            is.close();
            return bmp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * 1. 메소드명 : BufferedImage2Mat
     * 2. 작성일 : 2018. 12. 23. 오후 12:01:26
     * 3. 작성자 : taein
     * 4. 설명 : java.awt.image.BufferedImage to org.opencv.core.Mat
     * @param image
     * @return
     * @throws IOException
     */
    /*public static Mat bufferedImage2Mat(BufferedImage image) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "png", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
    }*/

    /**
     *
     * 1. 메소드명 : bufferedImage2ByteArray
     * 2. 작성일 : 2019. 1. 17. 오후 5:27:15
     * 3. 작성자 : taein
     * 4. 설명 :
     * @param image
     * @return
     */
    /*public static byte[] bufferedImage2ByteArray(BufferedImage image) throws Exception{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write( image, "jpg", baos );
        baos.flush();
        byte[] byteArray = baos.toByteArray();
        baos.close();
        return byteArray;
    }*/

    /**
     *
     * 1. 메소드명 : writeImage
     * 2. 작성일 : 2019. 1. 8. 오후 2:09:03
     * 3. 작성자 : taein
     * 4. 설명 : org.opencv.core.Mat과 java.awt.image.BufferedImage 타입만 write 할 수 있다.
     * @param folderName 저장할 폴더명, 또는 생성 할 폴더명
     * @param value 저장할 파일명에 규칙은 UUID_[value].jpg 로 저장 할 수 있다.
     * @param image org.opencv.core.Mat과 java.awt.image.BufferedImage 타입만  write 할 수 있다.
     * @throws IOException
     */
    /*public static <T> void writeImage(String folderName, String value, T image) throws IOException {
        File folder = new File(folderName);
        folder.mkdirs();
        String uuid = UUID.randomUUID().toString();
        File file = new File(folder, uuid + "_" + value + ".jpg");

        if(image instanceof Mat) {
            Imgcodecs.imwrite(file.getAbsolutePath(), (Mat)image);
        }else if(image instanceof BufferedImage) {
            ImageIO.write((BufferedImage)image, "jpg", file);
        }
    }*/

    public static byte[] bitmap2ByteArray(Bitmap bitmap) throws IOException {
        try (final ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }
    }

    public static Bitmap createSynthesisBitmap(Bitmap OutterBitmap, Bitmap innerBitmap) {
        Bitmap resultBitmap = Bitmap.createBitmap(OutterBitmap.getWidth(), OutterBitmap.getHeight(), OutterBitmap.getConfig());

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(OutterBitmap, new Matrix(), null);
        canvas.drawBitmap(innerBitmap, new Matrix(), null);

        return resultBitmap;
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }
}
