package com.hongbog.irismanualanalysis.util;

import android.content.Context;

import com.hongbog.irismanualanalysis.data.entity.Script;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ExcelReader {
    private ExcelReader() {}

    private static class SingleTonHolder {
        private static final ExcelReader instance = new ExcelReader();
    }

    public static ExcelReader getInstance() {
        return SingleTonHolder.instance;
    }

    public List<Script> xslAllScriptList(InputStream is, String languageCode) throws Exception {
        List<Script> scriptList = new ArrayList<Script>();

        HSSFWorkbook workbook = null;

        try {
            workbook = new HSSFWorkbook(is);

            HSSFSheet HSSFSheet;
            HSSFRow HSSFRow;
            HSSFCell HSSFCell;

            for (int sheetIdx = 0; sheetIdx < workbook.getNumberOfSheets(); sheetIdx++) {
                HSSFSheet = workbook.getSheetAt(sheetIdx);
                for (int rowIdx = 0; rowIdx < HSSFSheet.getPhysicalNumberOfRows(); rowIdx++) {
                    HSSFRow = HSSFSheet.getRow(rowIdx);
                    if ("".equals(HSSFRow.getCell(0).getStringCellValue())) break;

                    Script script = new Script();
                    script.setLanguageCode(languageCode);
                    for (int cellIdx = 0; cellIdx < HSSFRow.getPhysicalNumberOfCells(); cellIdx++) {
                        HSSFCell = HSSFRow.getCell(cellIdx);
                        String cellValue = HSSFCell.getStringCellValue()+"";

                        switch (cellIdx) {
                            case 0:
                                script.setCategory(cellValue);
                                break;
                            case 1:
                                script.setScriptCode(cellValue);
                                break;
                            case 2:
                                script.setSymptom(cellValue);
                                break;
                            case 3:
                                script.setPrecaution(cellValue);
                                break;
                            case 4:
                                script.setHealthyFood(cellValue);
                                break;
                            case 5:
                                script.setUnhealthyFood(cellValue);
                                break;
                        }
                    }
                    scriptList.add(script);
                }
            }
        } finally {
            if (is != null) is.close();
        }

        return scriptList;
    }
}
