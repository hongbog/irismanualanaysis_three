/*
 *  Copyright 2017 Google Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.hongbog.irismanualanalysis.util;

import android.annotation.SuppressLint;
import android.app.Application;

import com.hongbog.irismanualanalysis.analysis.AnalysisViewModel;
import com.hongbog.irismanualanalysis.camera.CameraViewModel;
import com.hongbog.irismanualanalysis.capture.CaptureViewModel;
import com.hongbog.irismanualanalysis.consent.ConsentViewModel;
import com.hongbog.irismanualanalysis.loading.LoadingViewModel;
import com.hongbog.irismanualanalysis.login.ManagerViewModel;
import com.hongbog.irismanualanalysis.userinfo.UserInfoViewModel;
import com.hongbog.irismanualanalysis.result.ResultViewModel;

import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * A creator is used to inject the product ID into the ViewModel
 * <p>
 * This creator is to showcase how to inject dependencies into ViewModels. It's not
 * actually necessary in this case, as the product ID can be passed in a public method.
 */
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    @SuppressLint("StaticFieldLeak")
    private static volatile ViewModelFactory INSTANCE;

//    private final DataRepository mDataRepository;
    private Application mApplication;

    public ViewModelFactory(Application application) {
        mApplication = application;
    }
    public static ViewModelFactory getInstance(Application application) {

        if (INSTANCE == null) {
            synchronized (ViewModelFactory.class) {
                if (INSTANCE == null) {
                    // TODO: Version 2.0에서 수행할 것.
                    /*INSTANCE = new ViewModelFactory(
                            Injection.provideTasksRepository(application.getApplicationContext()));*/

                    INSTANCE = new ViewModelFactory(application);
                }
            }
        }
        return INSTANCE;
    }

//TODO: Version 2.0에서 수행할 것.
/*    public DataRepository getTasksRepository() {
        return mDataRepository;
    }*/

    @VisibleForTesting
    public static void destroyInstance() {
        INSTANCE = null;
    }

    //TODO: Version 2.0에서 수행할 것.
/*    private ViewModelFactory(DataRepository repository) {
        mDataRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AnalysisViewModel.class)) {
            //noinspection unchecked
            return (T) new AnalysisViewModel(mDataRepository);
        } else if (modelClass.isAssignableFrom(CameraViewModel.class)) {
            //noinspection unchecked
            return (T) new CameraViewModel(mDataRepository);
        } else if (modelClass.isAssignableFrom(CaptureViewModel.class)) {
            //noinspection unchecked
            return (T) new CaptureViewModel(mDataRepository);
        } else if (modelClass.isAssignableFrom(UserInfoViewModel.class)) {
            //noinspection unchecked
            return (T) new UserInfoViewModel(mDataRepository);
        } else if (modelClass.isAssignableFrom(ResultViewModel.class)) {
            //noinspection unchecked
            return (T) new ResultViewModel(mDataRepository);
        } else if (modelClass.isAssignableFrom(ManagerViewModel.class)) {
            //noinspection unchecked
            return (T) new ManagerViewModel(mDataRepository);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }*/

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AnalysisViewModel.class)) {
            //noinspection unchecked
            return (T) new AnalysisViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(CameraViewModel.class)) {
            //noinspection unchecked
            return (T) new CameraViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(CaptureViewModel.class)) {
            //noinspection unchecked
            return (T) new CaptureViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(UserInfoViewModel.class)) {
            //noinspection unchecked
            return (T) new UserInfoViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(ResultViewModel.class)) {
            //noinspection unchecked
            return (T) new ResultViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(ManagerViewModel.class)) {
            //noinspection unchecked
            return (T) new ManagerViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(ConsentViewModel.class)) {
            return (T) new ConsentViewModel(mApplication);
        } else if (modelClass.isAssignableFrom(LoadingViewModel.class)) {
            return (T) new LoadingViewModel(mApplication);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
