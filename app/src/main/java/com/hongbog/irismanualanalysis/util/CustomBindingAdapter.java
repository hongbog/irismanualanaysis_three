package com.hongbog.irismanualanalysis.util;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.entity.User;
import com.orhanobut.logger.Logger;

import java.util.regex.Pattern;

import javax.annotation.Nullable;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;

/**
 * xml 에 View 에 대한 속성 값을 도메인 UI 로직에 맞게 수정 하였다.
 * Util 성 class 이기 때문에 abstract 로 Instantiate 를 막음
 * @author taein
 */
public abstract class CustomBindingAdapter {

    private static final int INVALID_INT_VALUE = -9999;
    private static final Pattern onlyNumberPattern = Pattern.compile("[^0-9]");

    /**
     * bitmap 가 있으면 imageView 에 설정 하고
     * 없으면 defaultResourceId 를 imageView 에 설정 한다.
     * @author taein
     * @param imageView
     * @param bitmap
     * @param defaultResourceId
     */
    @BindingAdapter(value = {"bind:setBitmap", "bind:setDefaultResourceId"}, requireAll = false)
    public static void setBitmap(ImageView imageView, @Nullable Bitmap bitmap, int defaultResourceId) {
        if (bitmap == null) {
            if (defaultResourceId < 0) return;
            imageView.setImageResource(defaultResourceId);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    /**
     * imageView 에 OnTouchListener 를 설정 한다.
     * @author taein
     * @param imageView
     * @param listener
     */
    @BindingAdapter({"bind:setOnTouchListener"})
    public static void setOnTouchListener(ImageView imageView, View.OnTouchListener listener) {
        imageView.setOnTouchListener(listener);
    }

    /**
     * intValue 가 있으면 textView text 를 설정 하고
     * 없으면 defaultResourceId 를 설정 한다.
     * @author taein
     * @param textView
     * @param intValue
     * @param defaultResourceId
     */
    @BindingAdapter(value = {"bind:convertIntWithString", "bind:setDefaultTextResourceId"}, requireAll = false)
    public static void convertIntToString(TextView textView, int intValue, int defaultResourceId) {
        if (intValue <= 0) {
            textView.setText(defaultResourceId);
        } else {
            final String stringValue = String.valueOf(intValue);
            textView.setText(stringValue);
        }
    }

    /**
     * TextView::getText 에 숫자 값만 뽑아 낸다.
     * @author taein
     * @param textView
     * @return
     */
    @InverseBindingAdapter(attribute = "bind:convertIntWithString", event = "android:textAttrChanged")
    public static int convertStringToInt(TextView textView) {
        final String stringValue = textView.getText().toString();
        final String numberValue = stringValue.replaceAll(onlyNumberPattern.pattern(), "").trim();

        try {
            int intValue = Integer.valueOf(numberValue);
            return intValue;
        } catch (NumberFormatException e) {
            return INVALID_INT_VALUE;
        }
    }

    /**
     * TextView 에 User.Sex 타입 들어 오면
     * 현지화 된 성별 언어를 TextView 에 설정 한다.
     * @author taein
     * @param textView
     * @param sex
     */
    @BindingAdapter(value = {"bind:convertSexWithString"})
    public static void fromSex(TextView textView, User.Sex sex) {
        if (TextUtils.equals(sex.toString(), User.Sex.M.toString())) {
            textView.setText(sex.getLocalizedMale());
        } else if (TextUtils.equals(sex.toString(), User.Sex.F.toString())) {
            textView.setText(sex.getLocalizedFemale());
        }
    }

    /**
     * 현지화된 성별 언어 값을 보고 User.Sex 타입으로 반환 한다.
     * @author taein
     * @param textView
     * @return
     */
    @InverseBindingAdapter(attribute = "bind:convertSexWithString", event = "android:textAttrChanged")
    public static User.Sex sexToString(TextView textView) {
        if (TextUtils.equals(textView.getText(), User.Sex.M.getLocalizedMale())) {
            return User.Sex.M;
        } else if (TextUtils.equals(textView.getText(), User.Sex.M.getLocalizedFemale())) {
            return User.Sex.F;
        } else {
            return null;
        }
    }
//
//    private static final int INVALID_INT_VALUE = -9999;
//    private static final Pattern onlyNumberPattern = Pattern.compile("[^0-9.]");
//
//    /**
//     * bitmap 가 있으면 imageView 에 설정 하고
//     * 없으면 defaultResourceId 를 imageView 에 설정 한다.
//     * @author taein
//     * @param imageView
//     * @param bitmap
//     * @param defaultResourceId
//     */
//    @BindingAdapter(value = {"bind:setBitmap", "bind:setDefaultResourceId"}, requireAll = false)
//    public static void setBitmap(ImageView imageView, @Nullable Bitmap bitmap, int defaultResourceId) {
//        if (bitmap == null) {
//            if (defaultResourceId < 0) return;
//            imageView.setImageResource(defaultResourceId);
//        } else {
//            imageView.setImageBitmap(bitmap);
//        }
//    }
//
//    /**
//     * imageView 에 OnTouchListener 를 설정 한다.
//     * @author taein
//     * @param imageView
//     * @param listener
//     */
//    @BindingAdapter({"bind:setOnTouchListener"})
//    public static void setOnTouchListener(ImageView imageView, View.OnTouchListener listener) {
//        imageView.setOnTouchListener(listener);
//    }
//
//    /**
//     * intValue 가 있으면 textView text 를 설정 하고
//     * 없으면 defaultResourceId 를 설정 한다.
//     * @author taein
//     * @param textView
//     * @param intValue
//     * @param defaultResourceId
//     */
//    @BindingAdapter(value = {"bind:convertIntWithString", "bind:setDefaultTextResourceId"}, requireAll = false)
//    public static void convertIntToString(TextView textView, int intValue, int defaultResourceId) {
//
//        if (intValue <= 0) {
//            textView.setText(defaultResourceId);
//        } else {
//            final String stringValue = String.valueOf(intValue);
//            textView.setText(stringValue);
//        }
//    }
//
//    /**
//     * TextView::getText 에 숫자 값만 뽑아 낸다.
//     * @author taein
//     * @param textView
//     * @return
//     */
///*    @InverseBindingAdapter(attribute = "bind:convertIntWithString", event = "android:textAttrChanged")
//    public static int convertStringToInt(TextView textView) {
//        final String stringValue = textView.getText().toString();
//        final String numberValue = stringValue.replaceAll(onlyNumberPattern.pattern(), "").trim();
//
//        try {
//            if (stringValue.contains("lb")) {
//                return UnitUtil.reverseWeightUnit(Double.parseDouble(numberValue));
//            } else if (stringValue.contains("ft")) {
//                return UnitUtil.reverseHeightUnit(Double.parseDouble(numberValue));
//            } else {
//                return Integer.valueOf(numberValue);
//            }
//        } catch (NumberFormatException e) {
//            return INVALID_INT_VALUE;
//        }
//    }*/
//
//    @InverseBindingAdapter(attribute = "bind:convertIntWithString", event = "android:textAttrChanged")
//    public static int convertStringToInt(TextView textView) {
//        final String stringValue = textView.getText().toString();
//        final String numberValue = stringValue.replaceAll(onlyNumberPattern.pattern(), "").trim();
//        try {
//            int intValue = Integer.valueOf(numberValue);
//            return intValue;
//        } catch (NumberFormatException e) {
//            return INVALID_INT_VALUE;
//        }
//    }
//
//    /**
//     * TextView 에 User.Sex 타입 들어 오면
//     * 현지화 된 성별 언어를 TextView 에 설정 한다.
//     * @author taein
//     * @param textView
//     * @param sex
//     */
//    @BindingAdapter(value = {"bind:convertSexWithString"})
//    public static void fromSex(TextView textView, User.Sex sex) {
//        if (TextUtils.equals(sex.toString(), User.Sex.M.toString())) {
//            textView.setText(sex.getmLocalizedMale());
//        } else if (TextUtils.equals(sex.toString(), User.Sex.F.toString())) {
//            textView.setText(sex.getmLocalizedFemale());
//        }
//    }
//
//    /**
//     * 현지화된 성별 언어 값을 보고 User.Sex 타입으로 반환 한다.
//     * @author taein
//     * @param textView
//     * @return
//     */
//    @InverseBindingAdapter(attribute = "bind:convertSexWithString", event = "android:textAttrChanged")
//    public static User.Sex sexToString(TextView textView) {
//        if (TextUtils.equals(textView.getText(), User.Sex.M.getmLocalizedMale())) {
//            return User.Sex.M;
//        } else if (TextUtils.equals(textView.getText(), User.Sex.M.getmLocalizedFemale())) {
//            return User.Sex.F;
//        } else {
//            return null;
//        }
//    }

}
