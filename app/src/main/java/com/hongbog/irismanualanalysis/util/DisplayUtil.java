package com.hongbog.irismanualanalysis.util;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;

public class DisplayUtil {
    public static int isVerticalDisplay(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixel = metrics.widthPixels;
        int heightPixel = metrics.heightPixels;
        int minPixel = -1;
        float density = metrics.density;

        if (widthPixel < heightPixel) {
            minPixel = widthPixel;
        } else {
            minPixel = heightPixel;
        }

        float dp = minPixel / density;
        return (int)dp;
    }
}
