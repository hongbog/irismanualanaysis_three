package com.hongbog.irismanualanalysis.util;

import android.text.TextUtils;
import androidx.room.TypeConverter;

public class PatternExistenceToBooleanConverter {

    private static final String EXISTENCE = "Y";
    private static final String NON_EXISTENCE = "N";

    @TypeConverter
    public static boolean existenceToBoolean(String existence) {
        return TextUtils.equals(existence, EXISTENCE);
    }

    @TypeConverter
    public static String booleanToExistence(boolean booleanValue) {
        if (booleanValue) {
            return EXISTENCE;
        } else {
            return NON_EXISTENCE;
        }
    }
}
