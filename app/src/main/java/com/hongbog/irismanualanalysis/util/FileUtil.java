package com.hongbog.irismanualanalysis.util;

import android.os.Environment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * File을 다루는 Util 클래스
 *
 * @author taein
 * @date 2019-03-12 오전 10:17
 **/
public class FileUtil {

    private FileUtil() {
        throw new AssertionError();
    }

    /**
     * 1. 메소드명 : deleteFile
     * 2. 작성일 : 2018. 10. 17. 오후 12:03:29
     * 3. 작성자 : KYH
     * 4. 설명 : 특정 디렉토리의 하위 디렉토리 및 파일들을 삭제하는 함수
     *
     * @param path
     */
    public static void deleteFile(String path) {
        File deleteFolder = new File(path);

        if (deleteFolder.exists()) {
            File[] deleteFolderList = deleteFolder.listFiles();

            for (int i = 0; i < deleteFolderList.length; i++) {
                if (deleteFolderList[i].isFile()) {
                    deleteFolderList[i].delete();
                } else {
                    deleteFile(deleteFolderList[i].getPath());
                }
                deleteFolderList[i].delete();
            }
            deleteFolder.delete();
        }
    }

    public static String getFileNameAsRemovePathAndExt(String includedExtName) {
        int pointIdx = includedExtName.lastIndexOf(".");
        int slashIdx = includedExtName.lastIndexOf("/");
        includedExtName = includedExtName.substring(0, pointIdx);
        includedExtName = includedExtName.substring(slashIdx + 1, includedExtName.length());
        return includedExtName;
    }

    public static String[] makeRandomPaths(int size, String dirName) {
        String[] paths = new String[size];

        for (int i = 0; i < size; i++) {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + dirName + "_" + System.currentTimeMillis() + "_" + i + ".png";
            paths[i] = path;
        }
        return paths;
    }

    public static String makeRandomFileName() {
        String fileName;
        fileName = String.valueOf(System.currentTimeMillis());
        return fileName;
    }

    public static void deleteFileFromFilePath(String filePath) {
        try {
            File file = new File(filePath);

            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 1. 메소드명 : makeFileAfterParentDir
     * 2. 작성일 : 2018. 11. 13. 오후 3:31:59
     * 3. 작성자 : taein
     * 4. 설명 : 부모 디렉터리가 없다면 생성 해준 후 파일명과 함께 File 객체를 반환한다.
     *
     * @param imageFilePath
     * @param fileName
     * @return
     */
    public static File makeFileAfterParentDir(String imageFilePath, String fileName) {
        File fileParent = new File(imageFilePath);
        if (!fileParent.exists()) fileParent.mkdirs();
        return new File(fileParent, fileName);
    }

    /**
     * 1. 메소드명 : getUniqueFileName
     * 2. 작성일 : 2018. 11. 13. 오후 3:31:59
     * 3. 작성자 : taein
     * 4. 설명 : UUID.randomUUID() + SimpleDateFormat("yyMMddHHmmssZ") 로 unipue 이름 생성
     *
     * @return String s
     */
    public static String getUniqueFileName() {
        final Date date = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss", Locale.KOREA);
        String dateFormat = UUID.randomUUID() + sdf.format(date);
        return dateFormat;
    }

    /**
     * 해당 json 의 타입으로 해당 File 에 write 한다.
     * @author taein
     * @param file
     * @param json
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> boolean toJson(File file, T json) throws IOException {
        final Type type = new TypeToken<T>() {}.getType();

        try (final Writer writer = new FileWriter(file)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(json, type, writer);
            return true;
        }
    }

    /**
     * 기존에 File 에 확장자 Dot 를 주입 시켜 준다.
     * @author taein
     */
    public static class FileWithDotForExtension extends File {

        public FileWithDotForExtension(String pathname, String extension) {
            super(pathname + "." + extension);
        }

        public FileWithDotForExtension(String parent, String child, String extension) {
            super(parent, child + "." + extension);
        }

        public FileWithDotForExtension(File parent, String child, String extension) {
            super(parent, child + "." + extension);
        }
    }
}