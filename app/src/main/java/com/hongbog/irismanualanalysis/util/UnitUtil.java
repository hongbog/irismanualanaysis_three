package com.hongbog.irismanualanalysis.util;

import java.util.Locale;

/**
 * IrisManualAnalysis
 * Method: UnitUtil
 * Created by oprime on 2019-05-15
 * Description:
 **/
public class UnitUtil {

    private UnitUtil() { throw new AssertionError();}

    public interface Pattern {
        String getPattern();
    }

    public enum UnitPattern implements Pattern {
        KO("ko"),
        EN("en"),
        ZH("zh");

        UnitPattern(String pattern) {
            mPattern = pattern;
        }

        private String mPattern;

        @Override
        public String getPattern() {
            return mPattern;
        }

    }

    public static String convertWeightUnit(int value) {
        double poundUnit = value * 2.205;
        return String.format("%.3f", poundUnit);
    }

    public static String converHeightUnit(int value) {
        double feetUnit = value / 30.48;
        return String.format("%.3f", feetUnit);
    }

    public static int reverseHeightUnit(double value) {
        double feetUnit = value * 30.48;
        return (int)Math.round(feetUnit);
    }

    public static int reverseWeightUnit(double value) {
        double poundUnit = value / 2.205;
        return (int)Math.round(poundUnit);
    }

}
