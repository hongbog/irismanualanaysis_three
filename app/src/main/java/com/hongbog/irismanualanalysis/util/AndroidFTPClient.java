package com.hongbog.irismanualanalysis.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.vo.FTPInfoVO;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AndroidFTPClient {
    private FTPSClient ftpClient;

    private AndroidFTPClient() {
        ftpClient = new FTPSClient();
    }

    private static class SingleToneHolder {
        static final AndroidFTPClient instance = new AndroidFTPClient();
    }

    public static AndroidFTPClient getInstance() {
        return AndroidFTPClient.SingleToneHolder.instance;
    }

    public boolean connectFTP(FTPInfoVO ftpInfoVO) throws Exception {
        ftpClient.connect(ftpInfoVO.getHost(), ftpInfoVO.getPort());

        if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) return false;  // 접속 실패

        boolean result = ftpClient.login(ftpInfoVO.getUsername(), ftpInfoVO.getPassword());

        if (!result) return false;  // 로그인 실패

        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
        ftpClient.execPROT("P");  // 데이터 연결 (Private 으로 설정)

        return true;
    }

    public boolean disconnectFTP() throws IOException {
        if (ftpClient.isConnected()) {
            ftpClient.logout();
            ftpClient.disconnect();
        }

        return true;
    }

    public ArrayList<File> uploadIrisInfoList(String ftpPath) {
        ArrayList<File> uploadFileList = new ArrayList<>();
        File uploadRootDir = new File(ftpPath);

        if (!uploadRootDir.exists()) {
            return null;
        }

        File[] uploadFileArray = uploadRootDir.listFiles();
        for (File uploadFileDir: uploadFileArray) {
            for (File uploadFile: uploadFileDir.listFiles()) {
                uploadFileList.add(uploadFile);
            }
        }

        return uploadFileList;
    }

    public void makeDirectory(String remoteRootDirPath, String dateFolder) throws IOException {
        if (!ftpClient.changeWorkingDirectory(remoteRootDirPath + "/" + dateFolder)) {
            ftpClient.makeDirectory(remoteRootDirPath + "/" + dateFolder);
        }
    }

    public boolean changeDirectory(String fullRemotePath) throws IOException{
        boolean changeWorkingBol = ftpClient.changeWorkingDirectory(fullRemotePath);

        return changeWorkingBol;
    }

    public boolean fileUploadFTP(File file) throws Exception{
        FileInputStream fis = new FileInputStream(file);

        boolean result = ftpClient.storeFile(file.getName(), fis);
        fis.close();

        return result;
    }

    public boolean deleteFile(File file) throws Exception {
        boolean result = false;

        if (file.exists()) {
            result = file.delete();
        }

        return result;
    }
}