package com.hongbog.irismanualanalysis.util;

import com.orhanobut.logger.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import lombok.Getter;

/**
 * Date를 convert 하는 Util 클래스
 *
 * @author taein
 * @date 2019-03-12 오전 10:16
 **/
public class DateUtil {

    private DateUtil() {
        throw new AssertionError();
    }

    /**
     * 패턴을 노출 시키는 Mark Interface
     * @author taein
     */
    public interface Pattern {
        String getPattern();
    }

    /**
     * 날짜 패턴 라벨링 상수 셋
     * @author taein
     */
    public enum DatePattern implements Pattern {
        NoWhiteSpace("yyyyMMdd"),
        Hyphen("yyyy-MM-dd"),
        Dot("yyyy.MM.dd"),
        Colon("yyyy:MM:dd");

        DatePattern(String pattern) {
            mPattern = pattern;
        }

        private String mPattern;

        @Override
        public String getPattern() {
            return mPattern;
        }
    }

    /**
     * 날짜-시간 패턴 라벨링 상수 셋
     * @author taein
     */
    public enum DateTimePattern implements Pattern {
        NoWhiteSpace("yyyyMMddHHmmss"),
        Hyphen("yyyy-MM-dd-HH-mm-ss"),
        Dot("yyyy.MM.dd.HH.mm.ss"),
        Colon("yyyy:MM:dd:HH:mm:ss");

        DateTimePattern(String pattern) {
            mPattern = pattern;
        }

        private String mPattern;

        @Override
        public String getPattern() {
            return mPattern;
        }
    }

    /**
     * @author taein
     * @param pattern
     * @return
     */
    public static String getCurrentDate(Pattern pattern) {
        final DateFormat dateFormat = new SimpleDateFormat(pattern.getPattern(), Locale.KOREA);
        final Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * @return 20190314214018
     * @author jslee
     */
    public static String getCurrentdateFormet(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);

        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
        return sdfNow.format(date);
    }


}
