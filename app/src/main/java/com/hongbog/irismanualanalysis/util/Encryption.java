package com.hongbog.irismanualanalysis.util;

import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {
    private String TAG = "Encryption";
    private String KEY = "hongbog0102";
    private String TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static String DEFAULT_CODING = "UTF-8";
    private SecretKeySpec secretKey;

    private Encryption() {
        secretKey = generateKey(KEY);
    }

    private static class SingleToneHolder {
        static final Encryption instance = new Encryption();
    }

    public static Encryption getInstance() {
        return Encryption.SingleToneHolder.instance;
    }

    public String encryptOrDecrypt(String str, int mode) throws Exception {
        String resultTxt = "";

        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(mode, secretKey);

        switch (mode) {
            case Cipher.ENCRYPT_MODE:
                byte[] strBytes = str.getBytes(DEFAULT_CODING);
                resultTxt = Base64.encodeToString(cipher.doFinal(strBytes), 0);
                break;
            case Cipher.DECRYPT_MODE:
                byte[] textBytes = Base64.decode(str, 0);
                resultTxt = new String(cipher.doFinal(textBytes), DEFAULT_CODING);
                break;
        }

        return resultTxt;
    }

    public void encryptOrDecrypt(File sourceFile, File destFile, int mode) throws Exception {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(mode, secretKey);

        InputStream is = null;
        OutputStream os = null;

        try {
            int destFileDirIdx = destFile.getAbsolutePath().lastIndexOf(destFile.getName());
            File destFileDir = new File(destFile.getAbsolutePath().substring(0, destFileDirIdx));

            if (!destFileDir.exists()) {
                destFileDir.mkdirs();
            }

            is = new BufferedInputStream(new FileInputStream(sourceFile));
            os = new BufferedOutputStream(new FileOutputStream(destFile));

            byte[] buffer = new byte[1024];
            int read = -1;
            while ((read = is.read(buffer)) != -1) {
                os.write(cipher.update(buffer, 0 ,read));
            }

            os.write(cipher.doFinal());
        } finally {
            if (os != null) os.close();
            if (is != null) is.close();
        }
    }

    private SecretKeySpec generateKey(String key) {
        SecretKeySpec secretKeySpec = null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] digestKey = md.digest(key.getBytes(DEFAULT_CODING));

            secretKeySpec = new SecretKeySpec(digestKey, "AES");
        } catch (Exception e ) {
            Log.d(TAG, e.getMessage());
        }

        return secretKeySpec;
    }
}
