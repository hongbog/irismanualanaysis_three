package com.hongbog.irismanualanalysis.util;

import com.orhanobut.logger.Logger;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

public class StringWithIntConverter {

    @TypeConverter
    public Integer stringToInt(@Nullable String value) {
        if (value == null) return null;
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            Logger.e(e, "raise NumberFormatException");
            return null;
        }
    }

    @TypeConverter
    public String intToString(Integer value) {
        return String.valueOf(value);
    }
}
