package com.hongbog.irismanualanalysis.util;

import java.util.List;

/**
 * 자료구조를 변형하는 Util 클래스
 * @author taein
 * @date 2019-03-12 오전 10:14
 **/
public class DataStructureUtil {
	private static final String TAG = DataStructureUtil.class.getSimpleName();

    /**
     * 
    * 1. 패키지명 : hongbog.eyeoclock.utils
    * 2. 타입명 : Utils.java
    * 3. 작성일 : 2018. 12. 12. 오후 10:44:16
    * 4. 작성자 : taein
    * 5. 설명 : compare 메서드를 추상화 한다.
    * @param <T>
    * @param <V>
     */
    public interface CompareFunc<T, V> {
 	    boolean compare(T t, V v);
    }
    
    /**
     * 
    * 1. 메소드명 : excludeDuplicatedListItem
    * 2. 작성일 : 2018. 12. 12. 오후 10:45:11
    * 3. 작성자 : taein
    * 4. 설명 : List에 중복되는 element가 없으면 해당 value를 추가한다.
    * @param list
    * @param value
    * @param f
     */
    public static <T> void excludeDuplicatedListItem(List<T> list, T value, CompareFunc<T, T> f) {
 	   boolean check = false;
 	   for(T t : list) {
 		   if(f.compare(t, value)) check = true;
 	   }
 	   if(!check) list.add(value);
 	}
}
