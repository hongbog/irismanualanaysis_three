package com.hongbog.irismanualanalysis.util;

import com.hongbog.irismanualanalysis.data.entity.User;
import com.orhanobut.logger.Logger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

/**
 * DB 와 Java Pojo 사이에서 User.Sex, String 타입 변환
 * Util 성 class 이기 때문에 abstract 로 Instantiate 를 막음
 * @author taein
 */
public abstract class SexToStringConverter {

    @NonNull
    @TypeConverter
    public static String fromSex(@Nullable User.Sex sex) {
        if (sex == null) return "";
        return sex.toString();
    }

    @TypeConverter
    public static User.Sex sexToString(String stringValue) {
        try {
            return User.Sex.valueOf(stringValue);
        } catch (IllegalArgumentException e) {
            Logger.e(e, "raise SexToStringConverter::IllegalArgumentException");
            return null;
        }
    }
}
