package com.hongbog.irismanualanalysis.analysis;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.databinding.FragmentStressringBinding;
import com.orhanobut.logger.Logger;

public class StressRingFragment extends Fragment{

    public static final String TAG = StressRingFragment.class.getSimpleName();
    private AnalysisViewModel mViewModel;
    private boolean isStressRing = false;

    public StressRingFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentStressringBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_stressring, container, false);

        mViewModel = AnalysisActivity.obtainViewModel(getActivity());
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(getActivity());

        setUpObserver();

        return binding.getRoot();
    }

    private void setUpObserver() {
        mViewModel.getSelectStressRing().observe(this, radioBtnId -> {

            String stressRingCode = "-1";

            switch (radioBtnId) {
                case R.id.radio_st1:
                    stressRingCode = "ST1";
                    break;
                case R.id.radio_st2:
                    stressRingCode = "ST2";
                    break;

            }

            if(mViewModel.isRightIrisPage()) {
                mViewModel.getRightIrisAnalysisByOrgan().setStressRingCode(stressRingCode);
            } else {
                mViewModel.getLeftIrisAnalysisByOrgan().setStressRingCode(stressRingCode);
            }

            if (stressRingCode.equals("-1")) {
                isStressRing = true;
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            //실제로 사용자 눈에 보이는경우
            if (getActivity() != null) {
                ImageView nextPage = (ImageView) getActivity().findViewById(R.id.right_image_btn);
                if (mViewModel != null) {
                    nextPage.setEnabled(mViewModel.getStressRingChecked());
                    return; }
                nextPage.setEnabled(false);
            }
        }
    }
}
