package com.hongbog.irismanualanalysis.analysis;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.DisplayUtil;
import com.hongbog.irismanualanalysis.util.TabLayoutUtils;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.databinding.ActivityAnalysisBinding;
import com.hongbog.irismanualanalysis.util.ViewModelFactory;
import com.orhanobut.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProviders;

/**
 * 수동 분석을 진행하는 화면
 *
 * @author taein
 * @date 2019-03-07 오후 5:57
 **/
public class AnalysisActivity extends AppCompatActivity {
    private final long FINISH_INTERVAL_TIME = 2000L;
    private long backPressedTime = 0;

    public static final int PARENT_RIGHT_VIEW_PAGER = 1;
    private ParentViewPager mViewPager;
    public static final String USER_HISTORY = "USER_HISTORY";
    private AnalysisViewModel mViewModel;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DisplayUtil.isVerticalDisplay(this) <= 720) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ActivityAnalysisBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_analysis);
        mViewModel = obtainViewModel(this);
        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
        mViewModel.start((UserHistory) getIntent().getExtras().getParcelable(AnalysisActivity.USER_HISTORY));

        openTabLayout();

        setUpObserver();
    }

    private void setUpObserver() {
        mViewModel.getRightIrisPage().observe(this, rightPage -> {
            if (rightPage) {
                mViewPager.setCurrentItem(PARENT_RIGHT_VIEW_PAGER);
                adapter.notifyDataSetChanged();
                mViewModel.setRefresh();
            }
        });
    }

    private void openTabLayout() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.fragment_container);
        mViewPager.setAdapter(adapter);
        mViewPager.setOnTouchListener((v, event) -> true);

        TabLayout tabLayout = findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(mViewPager);
        TabLayoutUtils.enableTabs(tabLayout, false);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<String> mFragmentTitleList = new ArrayList<>(Arrays.asList(getString(R.string.left), getString(R.string.right)));
        private final int MAX_PAGES = mFragmentTitleList.size();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position < 0 || MAX_PAGES <= position) {
                return null;
            }
            return new AnalysisFragment();
        }

        @Override
        public int getCount() {
            return MAX_PAGES;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @NonNull
    public static AnalysisViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(AnalysisViewModel.class);
    }

    @Override
    public void onBackPressed() {
        long tmpTime = System.currentTimeMillis();
        long intervalTime = tmpTime - backPressedTime;
        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
            super.onBackPressed();
        } else {
            backPressedTime = tmpTime;
            Toast.makeText(
                    getApplicationContext(), R.string.analysis_act_2_back_btn, Toast.LENGTH_SHORT).show();
        }
    }
}