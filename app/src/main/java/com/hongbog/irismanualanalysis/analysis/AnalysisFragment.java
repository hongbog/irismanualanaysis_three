package com.hongbog.irismanualanalysis.analysis;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.databinding.FragmentAnalysisBinding;
import com.hongbog.irismanualanalysis.loading.LoadingDialog;
import com.hongbog.irismanualanalysis.result.ResultActivity;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.orhanobut.logger.Logger;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class AnalysisFragment extends Fragment {
    UserInfoActivity userInfoActivity = (UserInfoActivity) UserInfoActivity.userInfoActivity;

    private static final int CHOLESTEROL_RING_VIEW = 0;
    private AnalysisViewModel mViewModel;
    private PagerAdapter mPagerAdapter;
    private List<String> titleList;
    private final List<Fragment> mFragmentList = Arrays.asList(new CholesterolRingFragment(),
            new StressRingFragment(), new HumanOrganPatternFragment());
    private final int VIEW_PAGER_SIZE = mFragmentList.size();
    private FragmentAnalysisBinding binding;
    private LoadingDialog loadingDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_analysis, container, false);

        loadingDialog = new LoadingDialog(getActivity());

        setUpBinding();

        setUpView();

        setupMoveNextViewPager();

        titleList = Arrays.asList(getString(R.string.part3_title),
                getString(R.string.part4_title), getString(R.string.part5_title));

        return binding.getRoot();
    }

    private void setUpBinding() {
        mViewModel = AnalysisActivity.obtainViewModel(getActivity());
        mViewModel.getLoadingBol().observe(getActivity(), loadingBol -> {
            if (mViewModel.getLoadingBol().getValue()) loadingDialog.show();
            else loadingDialog.dismiss();
        });

        AnalysisUserActionListener listener = getUserActionListener();
        binding.setLifecycleOwner(getActivity());
        binding.setViewModel(mViewModel);
        binding.setListener(listener);
    }

    private void setUpView() {
        mPagerAdapter = new PagerAdapter(getChildFragmentManager());
        binding.childContainer.setPagingEnabled(false);
        binding.childContainer.setAdapter(mPagerAdapter);
        binding.childContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mViewModel.getAnalysisTitle().setValue(titleList.get(position));

                if (mFragmentList.get(position) instanceof HumanOrganPatternFragment) {
                    mViewModel.enableIrisMap();
                } else {
                    mViewModel.disableIrisMap();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        binding.indicator.setViewPager(binding.childContainer, 0);

        ViewTreeObserver viewTreeObserver = binding.imgCopyLayout.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int fragLoadCnt =  mViewModel.getFragLoadCnt();

                if (fragLoadCnt == 1) {
                    mViewModel.enableLoadingDialog();

                    Image currImageDto = changeIrisAndIrisMapSize();
                    new initAllInformationAsyncTask(currImageDto, "left").execute();
                }

                mViewModel.setFragLoadCnt(fragLoadCnt + 1);
                binding.imgIrisMap.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        mViewModel.setTitleColor(ContextCompat.getColor(getActivity(), R.color.colorTitle));
    }

    private class initAllInformationAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private Image currImageDto;
        private String side;

        initAllInformationAsyncTask(Image currImageDto, String side) {
            this.currImageDto = currImageDto;
            this.side = side;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            mViewModel.initAnalysisInformation(this.currImageDto, this.side);
            publishProgress();
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            mViewModel.disableLoadingDialog();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (side.equals("left")) {
                mViewModel.setLeftIrisImageView();
                mViewModel.setCurrLeftIrisMapImageView();
            } else {
                mViewModel.setRightIrisImageView();
                mViewModel.setCurrRightIrisMapImageView();
            }
        }
    }

    private void setupMoveNextViewPager() {
        mViewModel.getNextPage().observe(this, nextPage -> {
            if (nextPage.getContentIfNotHandled() != null) {

                mViewModel.setTitleColor(ContextCompat.getColor(getActivity(), R.color.colorTitle));

                // 왼쪽 홍채 페이지에 마지막 프레그먼트
                if (!mViewModel.isRightIrisPage()) {
                    if (binding.childContainer.getCurrentItem() == (VIEW_PAGER_SIZE - 1)) {
                        if(mViewModel.isEmptyIrisAnalysis()) {
                            Toast.makeText(getActivity(), R.string.not_fill_zone, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mViewModel.setRightIrisPage();
                        mViewModel.enableLoadingDialog();
                        Image currImageDto = changeIrisAndIrisMapSize();
                        new initAllInformationAsyncTask(currImageDto, "right").execute();

                        binding.childContainer.setCurrentItem(CHOLESTEROL_RING_VIEW);
                        return;
                    }
                }

                // 오른쪽 홍채 페이지에 마지막 프레그먼트
                if (mViewModel.isRightIrisPage()) {
                    if (binding.childContainer.getCurrentItem() == (VIEW_PAGER_SIZE - 1)) {
                        if(mViewModel.isEmptyIrisAnalysis()) {
                            Toast.makeText(getActivity(), R.string.not_fill_zone, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        final Intent intent = new Intent(getActivity(), ResultActivity.class);
                        intent.putExtra(AnalysisActivity.USER_HISTORY, mViewModel.getUserHistory());
                        startActivity(intent);
                        getActivity().finish();
                        userInfoActivity.finish();
                        return;
                    }
                }

                binding.childContainer.setCurrentItem(binding.childContainer.getCurrentItem() + 1);
            }
        });

        mViewModel.getBeforePage().observe(this, beforePage -> {
            if (beforePage.getContentIfNotHandled() != null) {
                mViewModel.setTitleColor(ContextCompat.getColor(getActivity(), R.color.colorTitle));

                if (binding.childContainer.getCurrentItem() != 0) {
                    binding.childContainer.setCurrentItem(binding.childContainer.getCurrentItem() - 1);
                } else {
                    getActivity().finish();
                }
            }
        });
    }

    /**
     * fragment 로딩 시 image view 크기를 설정하는 함수
     */
    private Image changeIrisAndIrisMapSize() {
        FrameLayout imageLayout = binding.imgCopyLayout;
        ImageView irisImageView = binding.imgCopy;
        ImageView irisMapImageView = binding.imgIrisMap;

        Image currImageDto = mViewModel.getCurrentIrisImage();

        FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) irisImageView.getLayoutParams();
        frameParams.width = imageLayout.getWidth();
        frameParams.height = imageLayout.getWidth() * 3 / 4;

        irisImageView.setLayoutParams(frameParams);
        irisMapImageView.setLayoutParams(frameParams);

        int irisMapWidth = imageLayout.getWidth();
        int irisMapHeight = imageLayout.getWidth() * 3 / 4;
        currImageDto.setCenterX(irisMapWidth * currImageDto.getCenterX() / currImageDto.getWidth());
        currImageDto.setCenterY(irisMapHeight * currImageDto.getCenterY() / currImageDto.getLength());
        currImageDto.setPupilRadius(irisMapWidth * currImageDto.getPupilRadius() / currImageDto.getWidth());
        currImageDto.setAutonomicNerveWaveRadius(irisMapWidth * currImageDto.getAutonomicNerveWaveRadius() / currImageDto.getWidth());
        currImageDto.setIrisRadious(irisMapWidth * currImageDto.getIrisRadious() / currImageDto.getWidth());
        currImageDto.setWidth(irisMapWidth);
        currImageDto.setLength(irisMapHeight);

        return currImageDto;
    }

    /**
     * 이미지 뷰 롱 클릭 / 터치 이벤트 처리하는 클래스
     * @return
     */
    private AnalysisUserActionListener getUserActionListener() {
        return new AnalysisUserActionListener() {
            @Override
            public boolean onLongClikImageView(View view) {
                Bitmap irisBitmap = null;
                Bitmap irisMapBitmap = null;

                if (TextUtils.equals(mViewModel.getCurrentIrisImage().getSide(), "left")) {
                    irisBitmap = Image.leftIrisBitmap;
                    if (Image.leftCurrIrisMapColorBitmap == null) {
                        irisMapBitmap = Image.leftIrisMapColorBitmap;
                    } else {
                        irisMapBitmap = Image.leftCurrIrisMapColorBitmap;
                    }
                } else {
                    irisBitmap = Image.rightIrisBitmap;
                    if (Image.rightCurrIrisMapColorBitmap == null) {
                        irisMapBitmap = Image.rightIrisMapColorBitmap;
                    } else {
                        irisMapBitmap = Image.rightCurrIrisMapColorBitmap;
                    }
                }

                if (view.getId() == binding.imgCopy.getId()) {
                    ImageScaleDialog imageScaleDialog = new ImageScaleDialog(getActivity());
                    imageScaleDialog.callFunction(
                            irisBitmap,
                            mViewModel.getCurrentIrisBitmap().getValue().getWidth(),
                            mViewModel.getCurrentIrisBitmap().getValue().getHeight());
                } else if (view.getId() == binding.imgIrisMap.getId()) {
                    ImageScaleDialog imageScaleDialog = new ImageScaleDialog(getActivity());
                    imageScaleDialog.callFunction(
                            irisBitmap,
                            irisMapBitmap,
                            mViewModel.getCurrentIrisBitmap().getValue().getWidth(),
                            mViewModel.getCurrentIrisBitmap().getValue().getHeight());
                }

                return true;
            }

            @Override
            public ImageView.OnTouchListener onTouchImageView() {
                return new ImageViewOnTouchListener();
            }
        };
    }

    /**
     * 장기 부위 터치 이벤트 구현하는 클래스
     */
    private class ImageViewOnTouchListener implements ImageView.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                Image currImageDto = mViewModel.getCurrentIrisImage();
                mViewModel.organCheckOfTouch(x, y, currImageDto);
            }
            return false;
        }
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (mFragmentList.get(i) instanceof CholesterolRingFragment) {
                mViewModel.getAnalysisTitle().setValue(titleList.get(0));
            }
            return mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

    }
}