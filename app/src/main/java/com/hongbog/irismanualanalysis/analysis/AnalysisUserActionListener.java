package com.hongbog.irismanualanalysis.analysis;

import android.view.View;
import android.view.View.OnTouchListener;

public interface AnalysisUserActionListener {
    boolean onLongClikImageView(View view);
    OnTouchListener onTouchImageView();
}
