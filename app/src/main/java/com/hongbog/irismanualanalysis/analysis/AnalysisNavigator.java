package com.hongbog.irismanualanalysis.analysis;

// Analysis 화면에 Navigator interface
public interface AnalysisNavigator {
    void moveNextFragment();
}
