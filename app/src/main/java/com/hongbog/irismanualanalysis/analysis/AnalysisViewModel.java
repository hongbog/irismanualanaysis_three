package com.hongbog.irismanualanalysis.analysis;

import android.app.Application;
import android.graphics.Bitmap;
import android.widget.RadioGroup;

import com.hongbog.irismanualanalysis.data.entity.Image;
import com.hongbog.irismanualanalysis.data.entity.IrisAnalysis;
import com.hongbog.irismanualanalysis.data.entity.UserHistory;
import com.hongbog.irismanualanalysis.data.repository.ManualIrisAnalysisRepository;
import com.hongbog.irismanualanalysis.data.vo.OrganPointVO;
import com.hongbog.irismanualanalysis.userinfo.UserInfoActivity;
import com.hongbog.irismanualanalysis.util.Event;
import com.orhanobut.logger.Logger;

import java.util.List;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import lombok.Getter;

/**
 * AnalysisActivity 및 속해 있는 Fragment 를 공유하는 ViewModel
 *
 * @author taein
 * @date 2019-03-21 오후 1:43
 **/
public class AnalysisViewModel extends AndroidViewModel {
    private ManualIrisAnalysisRepository mRepository;
    private UserHistory mUserHistory;
    private IrisAnalysis mLeftIrisAnalysisByOrgan;
    private IrisAnalysis mRightIrisAnalysisByOrgan;
    private MutableLiveData<Boolean> activateBtn;
    private MutableLiveData<Event<Object>> mNextPage = new MutableLiveData<>(); // AnalysisFragment rightBtn
    private MutableLiveData<Event<Object>> mBeforePage = new MutableLiveData<>(); // AnalysisFragment leftBtn
    private MutableLiveData<Integer> mSelectStressRing = new MutableLiveData<>();
    private MutableLiveData<Integer> mSelectCholesterolRing = new MutableLiveData<>();
    private MutableLiveData<Boolean> mRightPage = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLeftPage = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mCurrentIrisBitmap = new MutableLiveData<>();
    private MutableLiveData<Bitmap> mCurrentIrisMapBitmap = new MutableLiveData<>();
    private MutableLiveData<String> mAnalysisTitle = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIrisMapVisible = new MutableLiveData<>();
    private MutableLiveData<Boolean> checkedNormalPatternCheckBox = new MutableLiveData<>();
    @Getter
    private MutableLiveData<Boolean> checkedLaPatternCheckBox = new MutableLiveData<>();
    @Getter
    private MutableLiveData<Boolean> checkedSpPatternCheckBox = new MutableLiveData<>();
    @Getter
    private MutableLiveData<Boolean> checkedPsPatternCheckBox = new MutableLiveData<>();
    @Getter
    private MutableLiveData<Boolean> checkedDfPatternCheckBox = new MutableLiveData<>();
    private MutableLiveData<Boolean> mEnableAbnormalPattern = new MutableLiveData<>();
    private MutableLiveData<Integer> titleColor = new MutableLiveData<>();
    private MutableLiveData<Boolean> loadingBol = new MutableLiveData<>();

    private int fragLoadCnt;

    private List<OrganPointVO> leftOrganPointList;
    private List<OrganPointVO> rightOrganPointList;

    private boolean mStressRingChecked = false;
    private boolean mCholesterolRingChecked = false;

    public AnalysisViewModel(@NonNull Application app) {
        super(app);
        mRepository = ManualIrisAnalysisRepository.getInstance(app);
    }

    public void start(UserHistory userHistory) {
        if (userHistory.getRightImage() == null) return;
        if (userHistory.getLeftImage() == null) return;
        fragLoadCnt = 1;
        mUserHistory = userHistory;
        mLeftIrisAnalysisByOrgan = IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
        mRightIrisAnalysisByOrgan = IrisAnalysis.newInstanceAsDefaultOrganAnalysis();
        enableAbnormalPattern();
        setLeftIrisPage();
    }

    public void initAnalysisInformation(Image currImageDto, String side) {
        List<OrganPointVO> organPointList = mRepository.initAnalysisInformation(currImageDto, side);

        if (side.equals("left")) leftOrganPointList = organPointList;
        else rightOrganPointList = organPointList;
    }

    public void organCheckOfTouch(int x, int y, Image currImageDto) {
        List<OrganPointVO> organPointList;

        if (isRightIrisPage()) {
            organPointList = rightOrganPointList;
            setCurrRightIrisMapImageView();
        } else {
            organPointList = leftOrganPointList;
            setCurrLeftIrisMapImageView();
        }

        String organCode = mRepository.organCheckOfTouch(x, y, currImageDto, organPointList);
        IrisAnalysis irisAnalysis;

        if (isRightIrisPage()) {
            irisAnalysis = mUserHistory.getRightImage().getIrisAnalysisBy(organCode);
        } else {
            irisAnalysis = mUserHistory.getLeftImage().getIrisAnalysisBy(organCode);
        }

        checkSelectedPattern(irisAnalysis);

        setIrisOrganPattern(organCode);

        setTitleColorBy(organCode);
    }

    private void setTitleColorBy(String organCode) {
        switch (organCode) {
            case IrisAnalysis.Organ.BRAIN_ORGAN:
                setTitleColor(Image.colorBrain);
                break;
            case IrisAnalysis.Organ.KIDENY_ORGAN:
                setTitleColor(Image.colorKidney);
                break;
            case IrisAnalysis.Organ.LIVER_ORGAN:
                setTitleColor(Image.colorLiver);
                break;
            case IrisAnalysis.Organ.LUNGS_ORGAN:
                setTitleColor(Image.colorLungs);
                break;
            case IrisAnalysis.Organ.PANCREAS_ORGAN:
                setTitleColor(Image.colorPancreas);
                break;
        }
    }

    public void setTitleColor(int color) {
        titleColor.setValue(color);
    }

    private void checkSelectedPattern(IrisAnalysis irisAnalysis) {
        checkedNormalPatternCheckBox.setValue(irisAnalysis.getOrganAnalysis().isNormalPattern());
        checkedLaPatternCheckBox.setValue(irisAnalysis.getOrganAnalysis().isLacunaPattern());
        checkedSpPatternCheckBox.setValue(irisAnalysis.getOrganAnalysis().isSpokePattern());
        checkedPsPatternCheckBox.setValue(irisAnalysis.getOrganAnalysis().isPigmentSpotPattern());
        checkedDfPatternCheckBox.setValue(irisAnalysis.getOrganAnalysis().isDefectPattern());
    }

    public MutableLiveData<Boolean> getLoadingBol() {
        return loadingBol;
    }

    public void enableLoadingDialog() {
        this.loadingBol.setValue(true);
    }

    public void disableLoadingDialog() {
        this.loadingBol.setValue(false);
    }

    public int getFragLoadCnt() {
        return fragLoadCnt;
    }

    public void setFragLoadCnt(int fragLoadCnt) {
        this.fragLoadCnt = fragLoadCnt;
    }

    public MutableLiveData<Integer> getTitleColor() {
        return titleColor;
    }

    public LiveData<Boolean> getEnableAbnormalPattern() {
        return mEnableAbnormalPattern;
    }

    public boolean getStressRingChecked() {
        return mStressRingChecked;
    }

    public boolean getCholesterolRingChecked() {
        return mCholesterolRingChecked;
    }

    public MutableLiveData<Boolean> getCheckedNormalPatternCheckBox() {
        return checkedNormalPatternCheckBox;
    }


    public LiveData<Integer> getSelectStressRing() {
        return mSelectStressRing;
    }

    public LiveData<Integer> getSelectCholesterolRing() {
        return mSelectCholesterolRing;
    }

    public MutableLiveData<String> getAnalysisTitle() {
        return mAnalysisTitle;
    }

    public LiveData<Bitmap> getCurrentIrisMapBitmap() {
        return mCurrentIrisMapBitmap;
    }

    public LiveData<Bitmap> getCurrentIrisBitmap() {
        return mCurrentIrisBitmap;
    }

    public IrisAnalysis getLeftIrisAnalysisByOrgan() {
        return mLeftIrisAnalysisByOrgan;
    }

    public IrisAnalysis getRightIrisAnalysisByOrgan() {
        return mRightIrisAnalysisByOrgan;
    }

    public LiveData<Boolean> getActivateBtn() {
        if (activateBtn == null) {
            activateBtn = new MutableLiveData<>();
            activateBtn.setValue(false);
        }
        return activateBtn;
    }

    public LiveData<Event<Object>> getNextPage() {
        return mNextPage;
    }

    public LiveData<Event<Object>> getBeforePage() {
        return mBeforePage;
    }

    // 다음 페이지로 이동하는 onClick listener 메소드
    public void startNextFragment() {
        mNextPage.setValue(new Event<>(new Object()));
    }

    // 이전 페이지로 이동하는 onClick listener 메소드
    public void startBeforeFragment() {
        mBeforePage.setValue(new Event<>(new Object()));
    }

    public boolean isEmptyIrisAnalysis() {
        Image image = getCurrentIrisImage();
        return image.isEmpty();
    }

    // onCheckedChanged listener in RadioGroup 메소드(physicalConstitution)
    public void setPhysicalConstitution(RadioGroup radioGroup, @IdRes int id) {
        mSelectCholesterolRing.setValue(id);
        mCholesterolRingChecked = true;
        enableBtn();
    }

    // onCheckedChanged listener in RadioGroup 메소드(tissueDensity)
    public void setTissueDensity(RadioGroup radioGroup, @IdRes int id) {
        mSelectStressRing.setValue(id);
        mStressRingChecked = true;
        enableBtn();
    }

    public void enableBtn() {
        activateBtn.setValue(true);
    }

    public void disableBtn() {
        activateBtn.setValue(false);
    }

    public UserHistory getUserHistory() {
        return mUserHistory;
    }

    public LiveData<Boolean> getRightIrisPage() {
        return mRightPage;
    }

    public LiveData<Boolean> getLeftIrisPage() {
        return mLeftPage;
    }

    public void setLeftIrisPage() {
        mLeftPage.setValue(true);
        setLeftIrisImageView();
    }

    public void setRightIrisPage() {
        mRightPage.setValue(true);
        setRightIrisImageView();
    }

    public void setRefresh() {
        mStressRingChecked = false;
        mCholesterolRingChecked = false;
        disableBtn();
    }

    public boolean isRightIrisPage() {
        if (mRightPage.getValue() == null || !mRightPage.getValue()) {
            return false;
        }
        return true;
    }

    public void setLeftIrisMapImageView() {
        if (mUserHistory != null) {
            mCurrentIrisMapBitmap.setValue(Image.leftIrisMapColorBitmap);
        }
    }

    public void setCurrLeftIrisMapImageView() {
        if (mUserHistory != null) {
            mCurrentIrisMapBitmap.setValue(Image.leftCurrIrisMapColorBitmap);
        }
    }

    public void setRightIrisMapImageView() {
        if (mUserHistory != null) {
            mCurrentIrisMapBitmap.setValue(Image.rightIrisMapColorBitmap);
        }
    }

    public void setCurrRightIrisMapImageView() {
        if (mUserHistory != null) {
            mCurrentIrisMapBitmap.setValue(Image.rightCurrIrisMapColorBitmap);
        }
    }

    public void setLeftIrisImageView() {
        if (mUserHistory != null) {
            mCurrentIrisBitmap.setValue(Image.leftIrisBitmap);
        }
    }

    public void setRightIrisImageView() {
        if (mUserHistory != null) {
            mCurrentIrisBitmap.setValue(Image.rightIrisBitmap);
        }
    }

    public void setIrisOrganPattern(String organ) {
        try {
            if (isRightIrisPage()) {
                setRightIrisAnalysisByOrgan(organ);
            } else {
                setLeftIrisAnalysisByOrgan(organ);
            }
        } catch (CloneNotSupportedException e) {
            Logger.e(e, "raise CloneNotSupportedException");
        }
    }

    private void setLeftIrisAnalysisByOrgan(String organ) throws CloneNotSupportedException {
        mLeftIrisAnalysisByOrgan = mLeftIrisAnalysisByOrgan.clone();
        mLeftIrisAnalysisByOrgan.setOrganCode(organ);
        mLeftIrisAnalysisByOrgan.getOrganAnalysis().setOrganCode(organ);
        mLeftIrisAnalysisByOrgan.setSide(UserInfoActivity.IRIS_LEFT);
        mLeftIrisAnalysisByOrgan.getOrganAnalysis().setSide(UserInfoActivity.IRIS_LEFT);
        mUserHistory.getLeftImage().setOrganAnalysisBy(mLeftIrisAnalysisByOrgan);
    }

    private void setRightIrisAnalysisByOrgan(String organ) throws CloneNotSupportedException {
        mRightIrisAnalysisByOrgan = mRightIrisAnalysisByOrgan.clone();
        mRightIrisAnalysisByOrgan.setOrganCode(organ);
        mRightIrisAnalysisByOrgan.getOrganAnalysis().setOrganCode(organ);
        mRightIrisAnalysisByOrgan.setSide(UserInfoActivity.IRIS_RIGHT);
        mRightIrisAnalysisByOrgan.getOrganAnalysis().setSide(UserInfoActivity.IRIS_RIGHT);
        mUserHistory.getRightImage().setOrganAnalysisBy(mRightIrisAnalysisByOrgan);
    }

    public void unCheckAbNormalPattern() {
        checkedLaPatternCheckBox.setValue(false);
        checkedSpPatternCheckBox.setValue(false);
        checkedPsPatternCheckBox.setValue(false);
        checkedDfPatternCheckBox.setValue(false);
    }

    @Nullable
    public Image getCurrentIrisImage() {
        if (isRightIrisPage()) {
            return mUserHistory.getRightImage();
        } else {
            return mUserHistory.getLeftImage();
        }
    }

    public LiveData<Boolean> getIrisMapVisible() {
        return mIrisMapVisible;
    }

    public void enableIrisMap() {
        mIrisMapVisible.setValue(true);
    }

    public void disableIrisMap() {
        mIrisMapVisible.setValue(false);
    }

    public void disableAbnormalPattern() {
        mEnableAbnormalPattern.setValue(false);
    }

    public void enableAbnormalPattern() {
        mEnableAbnormalPattern.setValue(true);
    }
}