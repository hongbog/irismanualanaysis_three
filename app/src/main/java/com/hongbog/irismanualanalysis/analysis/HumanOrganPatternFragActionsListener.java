package com.hongbog.irismanualanalysis.analysis;

import android.widget.CompoundButton;

public interface HumanOrganPatternFragActionsListener {
    void setHumanOrganPattern(CompoundButton buttonView, boolean isChecked);
}
