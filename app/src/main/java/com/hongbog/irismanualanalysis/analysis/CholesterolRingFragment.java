package com.hongbog.irismanualanalysis.analysis;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.databinding.FragmentCholesterolringBinding;
import com.orhanobut.logger.Logger;

public class CholesterolRingFragment extends Fragment{

    public static final String TAG = CholesterolRingFragment.class.getSimpleName();
    private AnalysisViewModel mViewModel;
    private boolean isCholesterolRing = false;

    public CholesterolRingFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentCholesterolringBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_cholesterolring, container,false);

        mViewModel = AnalysisActivity.obtainViewModel(getActivity());
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(getActivity());

        setUpObserver();

        return binding.getRoot();
    }

    private void setUpObserver() {
        mViewModel.getSelectCholesterolRing().observe(this, radioBtnId -> {

            String cholesterolRingCode = "-1";

            switch (radioBtnId) {
                case R.id.radio_ct1:
                    cholesterolRingCode = "CT1";
                    break;
                case R.id.radio_ct2:
                    cholesterolRingCode = "CT2";
                    break;

            }

            if(mViewModel.isRightIrisPage()) {
                mViewModel.getRightIrisAnalysisByOrgan().setCholesterolRingCode(cholesterolRingCode);
            } else {
                mViewModel.getLeftIrisAnalysisByOrgan().setCholesterolRingCode(cholesterolRingCode);
            }

            if (cholesterolRingCode.equals("-1")) {
                isCholesterolRing = true;
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            //실제로 사용자 눈에 보이는경우
            if (getActivity() != null) {
                ImageView nextPage = (ImageView) getActivity().findViewById(R.id.right_image_btn);
                if (mViewModel != null) {
                    nextPage.setEnabled(mViewModel.getCholesterolRingChecked());
                    return; }
                nextPage.setEnabled(false);
            }
        }
    }
}
