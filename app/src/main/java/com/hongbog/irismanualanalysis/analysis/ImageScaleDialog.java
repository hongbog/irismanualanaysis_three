package com.hongbog.irismanualanalysis.analysis;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;

import com.github.chrisbanes.photoview.PhotoView;
import com.hongbog.irismanualanalysis.R;

public class ImageScaleDialog {
    private Context context;
    private PhotoView photoView;

    public ImageScaleDialog(Context context) {
        this.context = context;
    }

    private Bitmap createSynthesisBitmap(Bitmap irisBitmap, Bitmap irisMapBitmap) {
        Bitmap resultBitmap  = Bitmap.createBitmap(irisBitmap.getWidth(), irisBitmap.getHeight(), irisBitmap.getConfig());

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(irisBitmap, new Matrix(), null);
        canvas.drawBitmap(irisMapBitmap, new Matrix(), null);

        return resultBitmap;
    }

    public void callFunction(final Bitmap bitmap, final int width, final int height) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  // 타이틀바 제거
        dialog.setContentView(R.layout.imagescale_dialog);
        dialog.getWindow().setGravity(Gravity.TOP);
        dialog.show();

        photoView = dialog.findViewById(R.id.photo_view);

        ViewTreeObserver viewTreeObserver = photoView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) photoView.getLayoutParams();
                linearParams.width = width;
                linearParams.height = height;

                photoView.setLayoutParams(linearParams);
                photoView.setImageBitmap(bitmap);
                photoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public void callFunction(final Bitmap bitmap1, final Bitmap bitmap2,  final int width, final int height) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  // 타이틀바 제거
        dialog.setContentView(R.layout.imagescale_dialog);
        dialog.getWindow().setGravity(Gravity.TOP);
        dialog.show();

        photoView = dialog.findViewById(R.id.photo_view);

        ViewTreeObserver viewTreeObserver = photoView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) photoView.getLayoutParams();
                linearParams.width = width;
                linearParams.height = height;

                photoView.setLayoutParams(linearParams);

                Bitmap resizeBitmap1 = Bitmap.createScaledBitmap(bitmap1, width, height, true);
                Bitmap resizeBitmap2 = Bitmap.createScaledBitmap(bitmap2, width, height, true);

                Bitmap synthesisBitmap = createSynthesisBitmap(resizeBitmap1, resizeBitmap2);
                photoView.setImageBitmap(synthesisBitmap);
                photoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }
}
