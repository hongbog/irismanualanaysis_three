package com.hongbog.irismanualanalysis.analysis;

import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hongbog.irismanualanalysis.R;
import com.hongbog.irismanualanalysis.data.entity.OrganAnalysis;
import com.hongbog.irismanualanalysis.databinding.FragmentHumanOrganPatternBinding;


public class HumanOrganPatternFragment extends Fragment {

    public static final String TAG = HumanOrganPatternFragment.class.getSimpleName();
    private AnalysisViewModel mViewModel;

    public HumanOrganPatternFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentHumanOrganPatternBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_human_organ_pattern, container, false);

        mViewModel = AnalysisActivity.obtainViewModel(getActivity());
        HumanOrganPatternFragActionsListener listener = getHumanOrganPatternFragActionsListener();
        binding.setListener(listener);
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    /**
     * 장기 패턴 선택시 콜백 메서드
     * @return
     * @author taein
     */
    private HumanOrganPatternFragActionsListener getHumanOrganPatternFragActionsListener() {
        return (buttonView, isChecked) -> {
            OrganAnalysis organAnalysis;
            int radioResourceId = buttonView.getId();

            if (mViewModel.isRightIrisPage()) {
                organAnalysis = mViewModel.getRightIrisAnalysisByOrgan().getOrganAnalysis();
            } else {
                organAnalysis = mViewModel.getLeftIrisAnalysisByOrgan().getOrganAnalysis();
            }

            if (isChecked) {
                checkOrganPattern(organAnalysis, radioResourceId);
            } else {
                unCheckOrganPattern(organAnalysis, radioResourceId);
            }
        };
    }

    /**
     * 장기 패턴 CheckBox 선택 해제시
     * OrganAnalysis 에 패턴을 'N'로 설정 한다.
     * @param organAnalysis
     * @param radioResourceId
     * @author taein
     */
    private void unCheckOrganPattern(OrganAnalysis organAnalysis, @IdRes int radioResourceId) {
        switch (radioResourceId) {
            case R.id.check_normal:
                mViewModel.enableAbnormalPattern();
                organAnalysis.nonExistNormal();
                break;
            case R.id.check_la:
                organAnalysis.nonExistLacuna();
                break;
            case R.id.check_sp:
                organAnalysis.nonExistSpoke();
                break;
            case R.id.check_ps:
                organAnalysis.nonExistPigmentSpot();
                break;
            case R.id.check_df:
                organAnalysis.nonExistDefect();
                break;
        }
    }

    /**
     * 장기 패턴 CheckBox 선택시
     * OrganAnalysis 에 패턴을 'Y'로 설정 한다.
     * @param organAnalysis
     * @param radioResourceId
     * @author taein
     */
    private void checkOrganPattern(OrganAnalysis organAnalysis, int radioResourceId) {
        switch (radioResourceId) {
            case R.id.check_normal:
                organAnalysis.setAllPatternsToNoExist();
                mViewModel.unCheckAbNormalPattern();
                mViewModel.disableAbnormalPattern();
                organAnalysis.existNormal();
                break;
            case R.id.check_la:
                organAnalysis.existLacuna();
                break;
            case R.id.check_sp:
                organAnalysis.existSpoke();
                break;
            case R.id.check_ps:
                organAnalysis.existPigmentSpot();
                break;
            case R.id.check_df:
                organAnalysis.existDefect();
                break;
        }
    }
}
