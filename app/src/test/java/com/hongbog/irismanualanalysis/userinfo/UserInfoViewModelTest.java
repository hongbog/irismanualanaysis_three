package com.hongbog.irismanualanalysis.userinfo;

import android.app.Application;
import android.content.res.Resources;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserInfoViewModelTest {

    // Executes each task synchronously using Architecture Components.
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    /*@Mock
    private TasksRepository mTasksRepository;*/
    @Mock
    private Application context;
    /*@Captor
    private ArgumentCaptor<TasksDataSource.GetTaskCallback> mGetTaskCallbackCaptor;*/
    private UserInfoViewModel userInfoViewModel;
    private final String SEX = "남자";
    private final String AGE = "29";
    private final String HEIGHT = "172";
    private final String WEIGHT = "69";
//    private Patient patient;

    @Before
    public void setupPatientInfoViewModel() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);
        setupContext();
//        this.patient = new Patient(AGE, HEIGHT, WEIGHT, SEX);
        // Get a reference to the class under test
//        this.userInfoViewModel = new UserInfoViewModel();
    }

    private void setupContext() {
        when(this.context.getApplicationContext()).thenReturn(this.context);
//        when(mContext.getString(R.string.no_data)).thenReturn(NO_DATA_STRING);
//        when(mContext.getString(R.string.no_data_description)).thenReturn(NO_DATA_DESC_STRING);
        when(this.context.getResources()).thenReturn(mock(Resources.class));
    }

    /*private void setupViewModelRepositoryCallback() {
        // Given an initialized ViewModel with an active task
        mTaskDetailViewModel.start(mTask.getId());

        // Use a captor to get a reference for the callback.
        verify(mTasksRepository).getTask(eq(mTask.getId()), mGetTaskCallbackCaptor.capture());

        mGetTaskCallbackCaptor.getValue().onTaskLoaded(mTask); // Trigger callback
    }*/

    @Test
    public void PatientInfoViewModel에Patient가유효한지검사() {
//        setupViewModelRepositoryCallback();
//        this.userInfoViewModel.start(new Patient(AGE, HEIGHT, WEIGHT, SEX));
//
//        assertEquals(this.userInfoViewModel.getPatient().getAge(), this.patient.getAge());
//        assertEquals(this.userInfoViewModel.getPatient().getHeight(), this.patient.getHeight());
//        assertEquals(this.userInfoViewModel.getPatient().getWeight(), this.patient.getWeight());
//        assertEquals(this.userInfoViewModel.getPatient().getSex(), this.patient.getSex());
    }
}